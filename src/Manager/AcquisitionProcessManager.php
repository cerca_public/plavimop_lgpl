<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\AcquisitionProcess;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AcquisitionProcessManager.
 */
class AcquisitionProcessManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PageManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param AcquisitionProcess $acquisitionProcess
     */
    public function save(AcquisitionProcess $acquisitionProcess)
    {
        $this->entityManager->persist($acquisitionProcess);
        $this->entityManager->flush();
    }

    /**
     * @param AcquisitionProcess $acquisitionProcess
     *
     * @return bool
     */
    public function remove(AcquisitionProcess $acquisitionProcess)
    {
        if (0 === $acquisitionProcess->getAcquisitions()->count()) {
            $this->entityManager->remove($acquisitionProcess);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\Motion;
use App\Entity\Tag;
use App\Repository\MotionRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PageManager.
 */
class MotionManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var MotionRepository */
    private $motionRepository;

    /**
     * PageManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param MotionRepository       $motionRepository
     */
    public function __construct(EntityManagerInterface $em, MotionRepository $motionRepository)
    {
        $this->em = $em;
        $this->motionRepository = $motionRepository;
    }

    public function createMotion($tags)
    {
        if (!$this->checkIfNewTagsExist($tags)) {
            $motion = $this->motionRepository->getMotionWithTags($tags);
            if ($motion) {
                return $motion;
            }
        }

        $motion = new Motion();
        $labels = [];

        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $motion->addTag($tag);
            $labels[] = $tag->getLabel();
        }
        $motion->setTitle(implode(' ', $labels));
        $this->em->persist($motion);
        $this->em->flush();

        return $motion;
    }

    private function checkIfNewTagsExist($tags)
    {
        /** @var Tag $tag */
        foreach ($tags as $tag) {
            if (null === $tag->getId()) {
                return true;
            }
        }

        return false;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\Tag;
use App\Entity\TagType;

/**
 * Class PageManager.
 */
class TagManager
{
    public function createTag(TagType $type, $label)
    {
        $tag = new Tag();
        $tag->setType($type)
            ->setLabel($label);

        return $tag;
    }
}

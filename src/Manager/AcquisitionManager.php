<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\Acquisition;
use Doctrine\ORM\EntityManagerInterface;

class AcquisitionManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    private $appDirAcquisition;

    public function __construct(EntityManagerInterface $entityManager, $appDirAcquisition)
    {
        $this->entityManager = $entityManager;
        $this->appDirAcquisition = $appDirAcquisition;
    }

    /**
     * @param Acquisition $acquisition
     *
     * @return string
     */
    public function download(Acquisition $acquisition)
    {
        $acquisition->incrementeCounter();
        $this->entityManager->flush();

        return $acquisition->getFileFile();
    }

    public function save(Acquisition $acquisition)
    {
        $this->entityManager->persist($acquisition);
        $this->entityManager->flush();
    }

    public function remove(Acquisition $acquisition)
    {
        $this->entityManager->remove($acquisition);
        $this->entityManager->flush();
    }
}

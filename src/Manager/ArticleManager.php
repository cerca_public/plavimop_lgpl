<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\Article;
use App\Entity\User;
use App\Service\Purifier;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserManager.
 */
class ArticleManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Purifier
     */
    private $purifier;

    /**
     * PageManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param Purifier               $purifier
     */
    public function __construct(EntityManagerInterface $em, Purifier $purifier)
    {
        $this->em = $em;
        $this->purifier = $purifier;
    }

    /**
     * @param Article   $article
     * @param User|null $user
     */
    public function save(Article $article, User $user = null)
    {
        if ($user) {
            $article->setUser($user);
        }

        $article->setValid(false);

        $article->setContent($this->purifier->purify($article->getContent()));

        $this->em->persist($article);
        $this->em->flush();
    }

    /**
     * @param Article $article
     */
    public function remove(Article $article)
    {
        $this->em->remove($article);
        $this->em->flush();
    }
}

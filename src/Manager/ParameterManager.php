<?php

namespace App\Manager;

use App\Entity\Parameter;
use App\Repository\ParameterRepository;

class ParameterManager
{
    /** @var Parameter */
    private $parameter;

    /**
     * ParameterManager constructor.
     *
     * @param ParameterRepository $parameterRepository
     */
    public function __construct(
        ParameterRepository $parameterRepository
    ) {
        $this->parameter = $parameterRepository->findOneBy([]) ?? new Parameter();
    }

    /**
     * @return Parameter
     */
    public function getParameters()
    {
        return $this->parameter;
    }
}

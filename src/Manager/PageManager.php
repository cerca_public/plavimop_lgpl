<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/11/2017
 * Time: 23:00.
 */

namespace App\Manager;

use App\Entity\Page;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PageManager.
 */
class PageManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var PageRepository */
    private $pageRepository;

    /**
     * PageManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param PageRepository         $pageRepository
     */
    public function __construct(EntityManagerInterface $em, PageRepository $pageRepository)
    {
        $this->em = $em;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param $slug
     * @param string $title
     *
     * @return Page
     */
    public function getOrCreatePage($slug, $title = 'TO DEFINE')
    {
        $page = $this->pageRepository->findOneBy(['slug' => $slug]);
        if (null === $page) {
            $page = new Page();
            $page->setTitle($title);
            $page->setDescription('TO DEFINE');
            $page->setSlug($slug);
            $page->setContent('TO DEFINE');
            $this->em->persist($page);
            $this->em->flush();
        }

        return $page;
    }
}

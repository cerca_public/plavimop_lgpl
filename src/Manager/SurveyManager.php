<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 16/12/2017
 * Time: 02:02.
 */

namespace App\Manager;

use App\Entity\Motion;
use App\Entity\Recognition;
use App\Entity\Survey;
use App\Repository\AcquisitionRepository;
use App\Repository\SurveyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SurveyManager.
 */
class SurveyManager
{
    const SESSION_ACQUISITIONS_IDS = 'current_acquisitions_ids';

    /**
     * @var SurveyRepository
     */
    private $surveyRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var AcquisitionRepository
     */
    private $acquisitionRepository;

    /**
     * SurveyManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param AcquisitionRepository  $acquisitionRepository
     * @param SurveyRepository       $surveyRepository
     * @param SessionInterface       $session
     */
    public function __construct(
        EntityManagerInterface $em,
        AcquisitionRepository $acquisitionRepository,
        SurveyRepository $surveyRepository,
        SessionInterface $session
    ) {
        $this->surveyRepository = $surveyRepository;
        $this->em = $em;
        $this->session = $session;
        $this->acquisitionRepository = $acquisitionRepository;
    }

    /**
     * @param bool $forceNewMotion
     *
     * @return Survey
     */
    public function initSurvey(bool $forceNewMotion = false): Survey
    {
        $survey = $this->surveyRepository->getCurrentSurvey();

        $acquisitions = $this->getCurrentMotion($survey, $forceNewMotion);

        /* @var Motion $motion */
        foreach ($acquisitions as $acquisition) {
                $recognition = new Recognition();
                $recognition->setAcquisition($acquisition);
                $survey->addRecognition($recognition);
        }
        return $survey;
    }

    private function getCurrentMotion(Survey $survey, $forceNewMotions)
    {
        $ids = ($forceNewMotions) ? null : $this->session->get(self::SESSION_ACQUISITIONS_IDS, null);

        list($ids, $motions) = $this->acquisitionRepository->getSurveyAcquisitions($survey->getNbRecognitions(), $ids);

        $this->session->set(self::SESSION_ACQUISITIONS_IDS, $ids);

        return $motions;
    }

    /**
     * @param Survey $survey
     *
     * @return Survey
     */
    public function saveSurvey(Survey $survey): Survey
    {
        foreach ($survey->getRecognitions() as $recognition) {
            $recognition->setRespondent($survey->getRespondent());
            $this->em->persist($recognition);
        }
        $this->em->flush();

        return $survey;
    }
}

<?php

namespace App\Security\Voter;

use App\Entity\AcquisitionProcess;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AcquisitionProcessVoter extends Voter
{
    const SELECT = 'select';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const DOWNLOAD_DESCRIPTION = 'download_description';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::SELECT, self::EDIT, self::DELETE, self::DOWNLOAD_DESCRIPTION], true)
            && $subject instanceof AcquisitionProcess;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::SELECT:
                return $this->canSelect($subject, $user);
                break;
            case self::EDIT:
                return $this->canEdit($subject, $user);
                break;
            case self::DELETE:
                return $this->canDelete($subject, $user);
                break;
            case self::DOWNLOAD_DESCRIPTION:
                return $this->canDownloadDescription($token);
                break;
        }

        return false;
    }

    private function canSelect(AcquisitionProcess $acquisition, UserInterface $user)
    {
        return $user === $acquisition->getUser();
    }

    private function canEdit(AcquisitionProcess $acquisition, UserInterface $user)
    {
        return $user === $acquisition->getUser();
    }

    private function canDelete(AcquisitionProcess $acquisition, UserInterface $user)
    {
        return $user === $acquisition->getUser();
    }

    private function canDownloadDescription(TokenInterface $token)
    {
        return $this->decisionManager->decide($token, [User::ROLE_USER]);
    }
}

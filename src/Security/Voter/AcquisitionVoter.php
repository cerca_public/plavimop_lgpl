<?php

namespace App\Security\Voter;

use App\Entity\Acquisition;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AcquisitionVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';
    const DOWNLOAD = 'download';
    const DOWNLOAD_CSV = 'download_csv';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::EDIT, self::DELETE, self::DOWNLOAD, self::DOWNLOAD_CSV], true)
            && $subject instanceof Acquisition;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($subject, $user);
                break;
            case self::DELETE:
                return $this->canDelete($subject, $user);
                break;
            case self::DOWNLOAD:
                return $this->canDownload($token);
                break;
            case self::DOWNLOAD_CSV:
                return $this->canDownloadCSV($token);
                break;
        }

        return false;
    }

    private function canEdit(Acquisition $acquisition, UserInterface $user)
    {
        return $user === $acquisition->getUser();
    }

    private function canDelete(Acquisition $acquisition, UserInterface $user)
    {
        return $user === $acquisition->getUser();
    }

    private function canDownload(TokenInterface $token)
    {
        return $this->decisionManager->decide($token, [User::ROLE_USER]);
    }

    private function canDownloadCSV(TokenInterface $token)
    {
        return $this->decisionManager->decide($token, [User::ROLE_CONTRIBUTOR]);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 19/10/2017
 * Time: 01:09.
 */

namespace App\Model;

use App\Entity\TagType;
use App\Repository\TagTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class MotionFilter.
 */
class MotionFilter
{
    /**
     * @var TagType[]
     */
    private $types;

    /**
     * @var ArrayCollection
     */
    private $selectedTags;

    /**
     * MotionFilter constructor.
     *
     * @param TagTypeRepository $tagTypeRepository
     */
    public function __construct(TagTypeRepository $tagTypeRepository
    ) {
        $this->types = $tagTypeRepository->findAllWithTags(true);

        $this->selectedTags = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getSelectedTags()
    {
        return $this->selectedTags;
    }

    /**
     * @param $tags
     */
    public function setSelectedTags($tags)
    {
        $this->selectedTags = $tags;
    }

    /**
     * @return array
     */
    public function getSelectedTagsIds()
    {
        $ids = [];
        foreach ($this->selectedTags as $tag) {
            $ids[] = $tag->getId();
        }

        return $ids;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param mixed $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }
}

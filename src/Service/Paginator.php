<?php

namespace App\Service;

use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 25/11/2017
 * Time: 16:40.
 */
class Paginator
{
    public function create(Query $query, $page, $max = 10)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($max);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}

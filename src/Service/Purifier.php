<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 21/03/2018
 * Time: 01:06.
 */

namespace App\Service;

/**
 * Class Purifier : based on HTMLPurifier.org.
 *
 * @see http://htmlpurifier.org/live/configdoc/plain.html#HTML.SafeIframe
 */
class Purifier
{
    /**
     * @var \HTMLPurifier
     */
    private $purifier;

    const REGEX_IFRAME_ALLOWED = [
        'www\.youtube\.com',
        'player\.vimeo\.com',
        'www\.dailymotion\.com',
        'mikro\.ish-lyon\.cnrs\.fr',
    ];

    /**
     * Purifier constructor.
     */
    public function __construct()
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set(
            'URI.SafeIframeRegexp',
            '%^(https?:)?//('.implode(')|(', self::REGEX_IFRAME_ALLOWED).')%'
        );
        $this->purifier = new \HTMLPurifier($config);
    }

    /**
     * @param $html
     *
     * @return string
     */
    public function purify($html)
    {
        return $this->purifier->purify($html);
    }
}

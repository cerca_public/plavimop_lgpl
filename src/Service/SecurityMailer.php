<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 13/12/2017
 * Time: 02:27.
 */

namespace App\Service;

use App\Entity\User;
use Twig\Environment;

class SecurityMailer
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var string */
    private $appMailContact;

    private $twig;

    public function __construct($appMailContact, \Swift_Mailer $mailer, Environment $twig)
    {
        $this->appMailContact = $appMailContact;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @param User $user
     *
     * @return int
     *
     * @internal param Contact $contact
     * @internal param FormInterface $form
     */
    public function forgottenPasswordMail(User $user)
    {
        $message = new \Swift_Message();

        $body = $this->twig->render('emails/forgotten_password.html.twig', ['user' => $user]);

        $message->setSubject('PLAVIMOP - Forgotten password')
            ->setFrom($this->appMailContact)
            ->setTo($user->getEmail())
            ->setBody($body, 'text/html');

        return $this->mailer->send($message);
    }
}

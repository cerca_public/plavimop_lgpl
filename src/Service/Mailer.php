<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 13/12/2017
 * Time: 02:27.
 */

namespace App\Service;

use App\Entity\User;
use App\Model\Contact;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Twig\Environment;

class Mailer
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var string */
    private $appMailContact;

    private $twig;

    public function __construct($appMailContact, \Swift_Mailer $mailer, Environment $twig)
    {
        $this->appMailContact = $appMailContact;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @param Contact $contact
     *
     * @return int
     *
     * @internal param FormInterface $form
     */
    public function sendMail(Contact $contact)
    {
        $message = new \Swift_Message();

        $body = $this->twig->render('emails/contact.html.twig', ['contact' => $contact]);

        $message->setSubject('[PLAVIMOP - Contact] - '.$contact->getSubject())
            ->setFrom($contact->getEmail())
            ->setTo($this->appMailContact)
            ->setBody($body, 'text/html');

        /** @var UploadedFile $attachment */
        foreach ($contact->getAttachments() as $attachment) {
            $swiftAtt = \Swift_Attachment::fromPath($attachment->getRealPath())
                ->setFilename($attachment->getClientOriginalName());
            $message->attach($swiftAtt);
        }

        return $this->mailer->send($message);
    }

    public function sendContributorRequest(User $user)
    {
        $message = new \Swift_Message();

        $body = $this->twig->render('emails/contributor_request.html.twig', ['user' => $user]);

        $message->setSubject('[PLAVIMOP - Contributor request] '.$user->getUsername())
            ->setFrom($user->getEmail())
            ->setTo($this->appMailContact)
            ->setBody($body, 'text/html');

        return $this->mailer->send($message);
    }

    public function sendDownloadSoftwareRequest(User $user)
    {
        $message = new \Swift_Message();

        $body = $this->twig->render('emails/download_software_request.twig', ['user' => $user]);

        $message->setSubject('[PLAVIMOP - Download Software request] '.$user->getUsername())
            ->setFrom($user->getEmail())
            ->setTo($this->appMailContact)
            ->setBody($body, 'text/html');

        return $this->mailer->send($message);
    }
}

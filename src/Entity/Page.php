<?php

namespace App\Entity;

use App\Entity\Translation\TranslatableTrait;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="page")
 *
 * @method PageTranslation translate($locale = null, $fallbackToDefault = true)
 */
class Page
{
    use ORMBehaviors\Sluggable\Sluggable;
    use TranslatableTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var PageTranslation[]
     *
     * @Assert\Valid()
     */
    protected $translations;

    /**
     * @return string[]
     */
    public function getSluggableFields()
    {
        return ['title'];
    }

    /**
     * @return bool
     */
    public function getRegenerateSlugOnUpdate()
    {
        return false;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->proxyCurrentLocaleTranslation('getTitle');
    }

    public function setTitle($title)
    {
        $this->proxyCurrentLocaleTranslation('setTitle', [$title]);
        $this->mergeNewTranslations();
    }

    public function getDescription()
    {
        return $this->proxyCurrentLocaleTranslation('getDescription');
    }

    public function setDescription($description)
    {
        $this->proxyCurrentLocaleTranslation('setDescription', [$description]);
        $this->mergeNewTranslations();
    }

    public function getContent()
    {
        return $this->proxyCurrentLocaleTranslation('getContent');
    }

    public function setContent($content)
    {
        $this->proxyCurrentLocaleTranslation('setContent', [$content]);
        $this->mergeNewTranslations();
    }
}

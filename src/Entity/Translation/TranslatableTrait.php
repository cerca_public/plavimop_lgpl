<?php

namespace App\Entity\Translation;

use Knp\DoctrineBehaviors\Model\Translatable\Translatable;

trait TranslatableTrait
{
    use Translatable;
}

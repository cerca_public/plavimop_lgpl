<?php

namespace App\Entity;

use App\Entity\Translation\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 * @ORM\Table()
 *
 * @method TagTranslation translate($locale = null, $fallbackToDefault = true)
 */
class Tag
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TagType", inversedBy="tags", cascade={"persist"})
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Motion", mappedBy="tags", cascade={"persist"})
     */
    private $motions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->motions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLabel();
    }

    public function getLabel()
    {
        return $this->proxyCurrentLocaleTranslation('getLabel');
    }

    public function setLabel($label)
    {
        $this->proxyCurrentLocaleTranslation('setLabel', [ucfirst($label)]);
        $this->mergeNewTranslations();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return TagType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param TagType $type
     *
     * @return Tag
     */
    public function setType(TagType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Add motions.
     *
     * @param Motion $motion
     *
     * @return Tag
     */
    public function addMotion(Motion $motion)
    {
        $this->motions[] = $motion;
        if (!$motion->getTags()->contains($this)) {
            $motion->addTag($this);
        }

        return $this;
    }

    /**
     * Remove motions.
     *
     * @param Motion $motion
     */
    public function removeMotion(Motion $motion)
    {
        $this->motions->removeElement($motion);
        if ($motion->getTags()->contains($this)) {
            $motion->removeTag($this);
        }
    }

    /**
     * Get motions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMotions()
    {
        return $this->motions;
    }
}

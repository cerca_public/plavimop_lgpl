<?php

namespace App\Entity;

use App\Entity\Translation\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagTypeRepository")
 * @ORM\Table()
 *
 * @method TagTypeTranslation translate($locale = null, $fallbackToDefault = true)
 */
class TagType
{
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Tag", mappedBy="type", cascade={"persist"})
     */
    private $tags;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $isComplete = true;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->proxyCurrentLocaleTranslation('getLabel');
    }

    /**
     * Set label.
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->proxyCurrentLocaleTranslation('setLabel', [ucfirst($label)]);
        $this->mergeNewTranslations();
    }

    /**
     * Add tag.
     *
     * @param Tag $tag
     *
     * @return TagType
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag.
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags.
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getOrderedTags()
    {
        $iterator = $this->tags->getIterator();

        $iterator->uasort(
            function (Tag $first, Tag $second) {
                return $first->getLabel() > $second->getLabel() ? 1 : -1;
            }
        );

        return $iterator;
    }

    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return $this->isComplete;
    }

    /**
     * @param bool $isComplete
     */
    public function setIsComplete(bool $isComplete)
    {
        $this->isComplete = $isComplete;
    }
}

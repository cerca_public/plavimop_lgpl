<?php

namespace App\Entity;

use App\Repository\AcquisitionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AcquisitionRepository::class)
 * @ORM\Table(name="acquisition")
 * @Vich\Uploadable()
 * @HasLifecycleCallbacks()
 */
class Acquisition
{
    const STATE_NEED_REVIEW = 0;
    const STATE_IS_VALID = 1;
    const ENABLED_SURVEY = 1;

    const PREVIEW_DURATION = 2000;
    const PREVIEW_DURATION_MIN = 1000;
    const PREVIEW_DURATION_MAX = 6000;

    const PREVIEW_MARKER_SIZE = 2;
    const PREVIEW_MARKER_SIZE_MIN = 1;
    const PREVIEW_MARKER_SIZE_MAX = 10;

    const PREVIEW_AZIMUTH = 45;
    const PREVIEW_AZIMUTH_MIN = -180;
    const PREVIEW_AZIMUTH_MAX = 180;

    const PREVIEW_ELEVATION = 45;
    const PREVIEW_ELEVATION_MIN = -90;
    const PREVIEW_ELEVATION_MAX = 90;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank()
     *
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Motion", inversedBy="acquisitions", cascade={"persist"})
     * @Assert\NotNull()
     *
     * @var Motion
     */
    private $motion;

    /**
     * @var AcquisitionProcess
     * @ORM\ManyToOne(targetEntity="AcquisitionProcess", inversedBy="acquisitions")
     * @Assert\NotNull()
     */
    private $acquisitionProcess;

    /**
     * @var ArrayCollection|Recognition[]
     * @ORM\OneToMany(targetEntity="Recognition", mappedBy="acquisition", cascade={"remove"})
     */
    private $recognitions;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $acqDate;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $preview;

    /**
     * @Vich\UploadableField(mapping="acquisition_files", fileNameProperty="preview.name", size="preview.size", mimeType="preview.mimeType", originalName="preview.originalName", dimensions="preview.dimensions")
     * @Assert\File(mimeTypes={"video/mp4"})
     *
     * @var File
     */
    private $previewFile;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $downloadCounter = 0;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $file;

    /**
     * @Vich\UploadableField(mapping="acquisition_files", fileNameProperty="file.name", size="file.size", mimeType="file.mimeType", originalName="file.originalName", dimensions="file.dimensions")
     * @Assert\File(mimeTypes={"application/octet-stream"})
     * @Assert\Expression("this.isValidFileUploaded()", message="validator.valid_acquisition_extension")
     *
     * @var File|UploadedFile
     */
    private $fileFile;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=false, options={"default": Acquisition::STATE_IS_VALID})
     */
    private $state = self::STATE_IS_VALID;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="acquisitions")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var ?integer
     * @ORM\Column(type="smallint", options={"default": Acquisition::PREVIEW_DURATION}, nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min=Acquisition::PREVIEW_DURATION_MIN, max=Acquisition::PREVIEW_DURATION_MAX)
     */
    private $previewDuration = self::PREVIEW_DURATION;

    /**
     * @var ?integer
     * @ORM\Column(type="smallint", options={"default": Acquisition::PREVIEW_MARKER_SIZE}, nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min=Acquisition::PREVIEW_MARKER_SIZE_MIN, max=Acquisition::PREVIEW_MARKER_SIZE_MAX)
     */
    private $previewMarkerSize = self::PREVIEW_MARKER_SIZE;

    /**
     * @var ?integer
     * @ORM\Column(type="smallint", options={"default": Acquisition::PREVIEW_AZIMUTH}, nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min=Acquisition::PREVIEW_AZIMUTH_MIN, max=Acquisition::PREVIEW_AZIMUTH_MAX)
     */
    private $previewAzimuth = self::PREVIEW_AZIMUTH;

    /**
     * @var ?integer
     * @ORM\Column(type="smallint", options={"default": Acquisition::PREVIEW_ELEVATION}, nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min=Acquisition::PREVIEW_ELEVATION_MIN, max=Acquisition::PREVIEW_ELEVATION_MAX)
     */
    private $previewElevation = self::PREVIEW_ELEVATION;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": 0}, nullable=false)
     */
    private $needNewPreviewGeneration = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": Acquisition::ENABLED_SURVEY}, nullable=false)
     */
    private $enabledSurvey = self::ENABLED_SURVEY;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->file = new EmbeddedFile();
        $this->preview = new EmbeddedFile();
        $this->recognitions = new ArrayCollection();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return Acquisition
     */
    public function setUser(?User $user): Acquisition
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThis(): Acquisition
    {
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAcqDate(): ?DateTime
    {
        return $this->acqDate;
    }

    /**
     * @param DateTime|null $acqDate
     */
    public function setAcqDate(DateTime $acqDate = null)
    {
        $this->acqDate = $acqDate;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    public function getName(): string
    {
        return '#'.$this->id.' '.$this->motion->getTitle();
    }

    /**
     * @return EmbeddedFile|null
     */
    public function getFile(): ?EmbeddedFile
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     *
     * @return Acquisition
     */
    public function setFile($file): Acquisition
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return int
     */
    public function getDownloadCounter(): int
    {
        return $this->downloadCounter;
    }

    /**
     * @param int $downloadCounter
     *
     * @return Acquisition
     */
    public function setDownloadCounter(int $downloadCounter): self
    {
        $this->downloadCounter = $downloadCounter;

        return $this;
    }

    /**
     * Incremente acquisition's counter by one.
     *
     * @return Acquisition $this
     */
    public function incrementeCounter(): Acquisition
    {
        ++$this->downloadCounter;

        return $this;
    }

    public function getFileFile(): ?File
    {
        return $this->fileFile;
    }

    /**
     * @param File|null $file
     */
    public function setFileFile(?File $file = null)
    {
        $this->fileFile = $file;
        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPreviewFile(): ?File
    {
        return $this->previewFile;
    }

    /**
     * @param File|null $file
     */
    public function setPreviewFile(?File $file = null)
    {
        $this->previewFile = $file;

        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt): Acquisition
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return EmbeddedFile
     */
    public function getPreview(): EmbeddedFile
    {
        return $this->preview;
    }

    /**
     * @param EmbeddedFile $preview
     */
    public function setPreview(EmbeddedFile $preview)
    {
        $this->preview = $preview;
    }

    /**
     * @param mixed $recognition
     */
    public function addRecognition($recognition)
    {
        $this->recognitions->add($recognition);
    }

    /**
     * @param mixed $recognition
     */
    public function removeRecognition($recognition)
    {
        $this->recognitions->removeElement($recognition);
    }

    /**
     * @return string|null
     */
    public function getComment(): string
    {
        return $this->comment == null?"":$this->comment;
    }

    /**
     * @param string|null $comment
     *
     * @return $this
     */
    public function setComment(?string $comment): Acquisition
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state)
    {
        $this->state = $state;
    }

    /**
     * Check if uploaded extension is c3d (use in fileFile expression constraint).
     *
     * @return bool
     */
    public function isValidFileUploaded(): bool
    {
        if ($this->fileFile && $this->fileFile instanceof UploadedFile) {
            return 'c3d' === $this->fileFile->getClientOriginalExtension();
        }

        return true;
    }

    /**
     * @return AcquisitionProcess|null
     */
    public function getAcquisitionProcess(): ?AcquisitionProcess
    {
        return $this->acquisitionProcess;
    }

    /**
     * @param AcquisitionProcess $acquisitionProcess
     */
    public function setAcquisitionProcess(AcquisitionProcess $acquisitionProcess)
    {
        $this->acquisitionProcess = $acquisitionProcess;
    }

    public function isValid(): bool
    {
        return self::STATE_IS_VALID === $this->state;
    }

    /**
     * @return mixed
     */
    public function getPreviewDuration()
    {
        return $this->previewDuration;
    }

    /**
     * @param mixed $previewDuration
     *
     * @return Acquisition
     */
    public function setPreviewDuration($previewDuration): Acquisition
    {
        $this->previewDuration = $previewDuration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviewMarkerSize()
    {
        return $this->previewMarkerSize;
    }

    /**
     * @param mixed $previewMarkerSize
     *
     * @return Acquisition
     */
    public function setPreviewMarkerSize($previewMarkerSize): Acquisition
    {
        $this->previewMarkerSize = $previewMarkerSize;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviewAzimuth()
    {
        return $this->previewAzimuth;
    }

    /**
     * @param mixed $previewAzimuth
     *
     * @return Acquisition
     */
    public function setPreviewAzimuth($previewAzimuth): Acquisition
    {
        $this->previewAzimuth = $previewAzimuth;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviewElevation()
    {
        return $this->previewElevation;
    }

    /**
     * @param mixed $previewElevation
     *
     * @return Acquisition
     */
    public function setPreviewElevation($previewElevation): Acquisition
    {
        $this->previewElevation = $previewElevation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNeedNewPreviewGeneration(): bool
    {
        return $this->needNewPreviewGeneration;
    }

    /**
     * @param bool $needNewPreviewGeneration
     */
    public function setNeedNewPreviewGeneration(bool $needNewPreviewGeneration)
    {
        $this->needNewPreviewGeneration = $needNewPreviewGeneration;
    }

    /**
     * @return bool
     */
    public function isEnabledSurvey(): bool
    {
        return $this->enabledSurvey;
    }

    /**
     * @param bool $enabledSurvey
     */
    public function setEnabledSurvey(bool $enabledSurvey): void
    {
        $this->enabledSurvey = $enabledSurvey;
    }

    public function getRecognitionRate()
    {
        $allRates = 0;
        foreach ($this->recognitions as $recognition) {
            $allRates += $recognition->getRecognitionRate();
        }

        return ($this->recognitions->count()) ? $allRates / $this->recognitions->count() : 0;
    }

    public function getRecognitionsTags(): array
    {
        $typesArray = [];
        foreach ($this->recognitions as $recognition) {
            foreach ($recognition->getSelectedTags() as $tag) {
                if (!isset($typesArray[$tag->getType()->getLabel()])) {
                    $typesArray[$tag->getType()->getLabel()] = [];
                }

                if (isset($typesArray[$tag->getType()->getLabel()][$tag->getLabel()])) {
                    $typesArray[$tag->getType()->getLabel()][$tag->getLabel()] += 1;
                } else {
                    $typesArray[$tag->getType()->getLabel()][$tag->getLabel()] = 1;
                }
            }
        }

        return $typesArray;
    }

    public function getExportRecognitionsArray(): array
    {
        $data = [];
        foreach ($this->getRecognitions() as $key => $recognition) {
            $row = [];
            $row['Acquisition'] = $this->getName();
            $row['Participant'] = $recognition->getRespondent()->getId();
            $row['Age'] = $recognition->getRespondent()->getAge();
            $row['Sex'] = $recognition->getRespondent()->getGender();
            $row['Sport'] = $recognition->getRespondent()->getSensoryMotorExpertise();
            $row['Level'] = $recognition->getRespondent()->getMotorExpertiseLevel();
            foreach ($recognition->getSelectedTags() as $tag) {
                $row[$tag->getType()->getLabel().' recognition'] = ($this->getMotion()->getTags()->contains(
                    $tag
                )) ? 1 : $tag->getLabel();
            }
            $data[] = $row;
        }

        return $data;
    }

    /**
     * @return Recognition[]|ArrayCollection
     */
    public function getRecognitions()
    {
        return $this->recognitions;
    }

    /**
     * @return Motion
     */
    public function getMotion(): Motion
    {
        return $this->motion;
    }

    /**
     * @param Motion $motion
     *
     * @return $this
     */
    public function setMotion(Motion $motion): Acquisition
    {
        $this->motion = $motion;

        return $this;
    }

    /**
     * /**
     * @ORM\PreUpdate()
     *
     * @param PreUpdateEventArgs $event
     */
    public function checkNeedNewPreviewGeneration(PreUpdateEventArgs $event)
    {
        $needNewPreviewGenerationField = ['previewAzimuth', 'previewDuration', 'previewMarkerSize', 'previewElevation'];

        foreach ($event->getEntityChangeSet() as $attr => [$old, $new]) {
            if (in_array($attr, $needNewPreviewGenerationField, true)) {
                $this->needNewPreviewGeneration = true;
            }
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 24/06/2018
 * Time: 00:38.
 */

namespace App\Entity;

use App\Entity\Translation\TranslationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class MotionTranslation
{
    use TranslationTrait;

    /**
     * @var ?string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function __toString()
    {
        return $this->title;
    }


}

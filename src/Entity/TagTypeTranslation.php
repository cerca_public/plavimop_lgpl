<?php

namespace App\Entity;

use App\Entity\Translation\TranslationTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class TagTypeTranslation
{
    use TranslationTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $label;

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function __toString()
    {
        return 'Label '.$this->locale.' | '.$this->label;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParameterRepository")
 * @Vich\Uploadable()
 */
class Parameter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $databaseSrcManual;
    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="databaseSrcManual")
     *
     * @var File|null
     */
    private $databaseSrcManualFile;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $databasePdfManual;
    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="databasePdfManual")
     * @Assert\File(mimeTypes={"application/pdf"})
     *
     * @var File|null
     */
    private $databasePdfManualFile;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $softwareSrcManual;
    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="softwareSrcManual")
     *
     * @var File|null
     */
    private $softwareSrcManualFile;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var null|string
     */
    private $softwarePdfManual;
    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="softwarePdfManual")
     * @Assert\File(mimeTypes={"application/pdf"})
     *
     * @var File|null
     */
    private $softwarePdfManualFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var null|string
     */
    private $softwareSrcManualNew;

    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="softwareSrcManualNew")
     *
     * @var File|null
     */
    private $softwareSrcManualFileNew;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var null|string
     */
    private $softwarePdfManualNew;

    /**
     * @Vich\UploadableField(mapping="parameter_files", fileNameProperty="softwarePdfManualNew")
     * @Assert\File(mimeTypes={"application/pdf"})
     *
     * @var File|null
     */
    private $softwarePdfManualFileNew;


    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getDatabaseSrcManual(): ?string
    {
        return $this->databaseSrcManual;
    }

    /**
     * @param null|string $databaseSrcManual
     */
    public function setDatabaseSrcManual(?string $databaseSrcManual)
    {
        $this->databaseSrcManual = $databaseSrcManual;
    }

    /**
     * @return null|File
     */
    public function getDatabaseSrcManualFile(): ?File
    {
        return $this->databaseSrcManualFile;
    }

    /**
     * @param null|File $databaseSrcManualFile
     */
    public function setDatabaseSrcManualFile(?File $databaseSrcManualFile)
    {
        $this->databaseSrcManualFile = $databaseSrcManualFile;
        $this->changeUpdatedAtIfFileUpload($databaseSrcManualFile);
    }

    private function changeUpdatedAtIfFileUpload($file)
    {
        if ($file instanceof UploadedFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    /**
     * @return null|string
     */
    public function getDatabasePdfManual(): ?string
    {
        return $this->databasePdfManual;
    }

    /**
     * @param null|string $databasePdfManual
     */
    public function setDatabasePdfManual(?string $databasePdfManual)
    {
        $this->databasePdfManual = $databasePdfManual;
    }

    /**
     * @return null|File
     */
    public function getDatabasePdfManualFile(): ?File
    {
        return $this->databasePdfManualFile;
    }

    /**
     * @param null|File $databasePdfManualFile
     */
    public function setDatabasePdfManualFile(?File $databasePdfManualFile)
    {
        $this->databasePdfManualFile = $databasePdfManualFile;
        $this->changeUpdatedAtIfFileUpload($databasePdfManualFile);
    }

    /**
     * @return null|string
     */
    public function getSoftwareSrcManual(): ?string
    {
        return $this->softwareSrcManual;
    }

    /**
     * @param null|string $softwareSrcManual
     */
    public function setSoftwareSrcManual(?string $softwareSrcManual)
    {
            $this->softwareSrcManual = $softwareSrcManual;
    }

    /**
     * @return null|File
     */
    public function getSoftwareSrcManualFile(): ?File
    {
        return $this->softwareSrcManualFile;
    }

    /**
     * @param null|File $softwareSrcManualFile
     */
    public function setSoftwareSrcManualFile(?File $softwareSrcManualFile)
    {
        $this->softwareSrcManualFile = $softwareSrcManualFile;
        $this->changeUpdatedAtIfFileUpload($softwareSrcManualFile);
    }

    /**
     * @return null|string
     */
    public function getSoftwarePdfManual(): ?string
    {
        return $this->softwarePdfManual;
    }

    /**
     * @param null|string $softwarePdfManual
     */
    public function setSoftwarePdfManual(?string $softwarePdfManual)
    {
       $this->softwarePdfManual = $softwarePdfManual;
    }

    /**
     * @return null|File
     */
    public function getSoftwarePdfManualFile(): ?File
    {
        return $this->softwarePdfManualFile;
    }

    /**
     * @param null|File $softwarePdfManualFile
     */
    public function setSoftwarePdfManualFile(?File $softwarePdfManualFile)
    {
        $this->softwarePdfManualFile = $softwarePdfManualFile;
        $this->changeUpdatedAtIfFileUpload($softwarePdfManualFile);
    }

    /**
     * @param null|string $softwareSrcManualNew
     */
    public function setSoftwareSrcManualNew(?string $softwareSrcManualNew)
    {
        $this->softwareSrcManualNew = $softwareSrcManualNew;
    }

    /**
     * @return File|null
     */
    public function getSoftwareSrcManualFileNew(): ?File
    {
        return $this->softwareSrcManualFileNew;
    }

    /**
     * @param null|File $softwareSrcManualFileNew
     */
    public function setSoftwareSrcManualFileNew(?File $softwareSrcManualFileNew)
    {
        $this->softwareSrcManualFileNew = $softwareSrcManualFileNew;
    }

    /**
     * @return null|string
     */
    public function getSoftwarePdfManualNew(): ?string
    {
        return $this->softwarePdfManualNew;
    }

    /**
     * @param null|string $softwarePdfManualNew
     */
    public function setSoftwarePdfManualNew(?string $softwarePdfManualNew)
    {
        $this->softwarePdfManualNew = $softwarePdfManualNew;
    }

    /**
     * @return null|File
     */
    public function getSoftwarePdfManualFileNew(): ?File
    {
        return $this->softwarePdfManualFileNew;
    }

    /**
     * @param null|File $softwarePdfManualFileNew
     */
    public function setSoftwarePdfManualFileNew(?File $softwarePdfManualFileNew)
    {
        $this->softwarePdfManualFileNew = $softwarePdfManualFileNew;
        $this->changeUpdatedAtIfFileUpload($softwarePdfManualFileNew);
    }

    /**
     * @return null|string
     */
    public function getSoftwareSrcManualNew(): ?string
    {
        return $this->softwareSrcManualNew;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}

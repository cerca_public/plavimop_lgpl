<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User implements UserInterface, \Serializable
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_CONTRIBUTOR = 'ROLE_CONTRIBUTOR';

    const PASSWORD_MIN_LENGTH = 6;
    const TITLES = [
        'Professor',
        'Associate professor',
        'Assistant professor',
        'Ph.D. / Ph.D. Student',
        'Engineer',
        'Undergraduate student',
        'Other',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $institute;

    /**
     * @var ?string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var ?string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var ?string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var ?string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Country()
     */
    private $country;

    /**
     * @var ?string
     * @ORM\Column(type="text", nullable=true)
     */
    private $researchTopic;

    /**
     * @var ?string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $username;
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(type="string", unique=true)
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $resetPassword;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var AcquisitionProcess[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="AcquisitionProcess", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $acquisitionProcess;

    /**
     * @var Acquisition[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Acquisition", mappedBy="user")
     */
    private $acquisitions;

    /**
     * @var Article[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
     */
    private $articles;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = [self::ROLE_USER];
        $this->acquisitionProcess = new ArrayCollection();
        $this->acquisitions = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Institute|null $institute
     */
    public function setInstitute(Institute $institute = null)
    {
        $this->institute = $institute;
    }

    /**
     * @return ?Institute
     */
    public function getInstitute()
    {
        return $this->institute;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        if (empty($roles)) {
            $roles[] = self::ROLE_USER;
        }

        return array_unique($roles);
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
        $this->resetPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getResetPassword()
    {
        return $this->resetPassword;
    }

    /**
     * @param mixed $resetPassword
     */
    public function setResetPassword($resetPassword)
    {
        $this->resetPassword = $resetPassword;
    }

    /**
     * @return AcquisitionProcess[]|ArrayCollection
     */
    public function getAcquisitionProcess()
    {
        return $this->acquisitionProcess;
    }

    /**
     * @param AcquisitionProcess $acquisitionProces
     */
    public function addAcquisitionProces(AcquisitionProcess $acquisitionProces)
    {
        $this->acquisitionProcess->add($acquisitionProces);
        $acquisitionProces->setUser($this);
    }

    /**
     * @param AcquisitionProcess $acquisitionProces
     */
    public function removeAcquisitionProces(AcquisitionProcess $acquisitionProces)
    {
        $this->acquisitionProcess->removeElement($acquisitionProces);
    }

    public function generateResetPassword()
    {
        $this->resetPassword = base64_encode(random_bytes(20));
    }

    /**
     * @return Acquisition[]|ArrayCollection
     */
    public function getAcquisitions()
    {
        return $this->acquisitions;
    }

    /**
     * @param Acquisition $acquisition
     */
    public function addAcquisition(Acquisition $acquisition)
    {
        $this->acquisitions->add($acquisition);
    }

    /**
     * @param Acquisition $acquisition
     */
    public function removeAcquisition(Acquisition $acquisition)
    {
        $this->acquisitions->removeElement($acquisition);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function getCountryName()
    {
        return Intl::getRegionBundle()->getCountryName($this->country);
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getResearchTopic()
    {
        return $this->researchTopic;
    }

    /**
     * @param mixed $researchTopic
     */
    public function setResearchTopic($researchTopic)
    {
        $this->researchTopic = $researchTopic;
    }

    /**
     * @return Article[]|ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $this->articles->add($article);
        $article->setUser($this);
    }

    /**
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
        $article->setUser(null);
    }
}

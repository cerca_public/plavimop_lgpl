<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecognitionRepository")
 */
class Recognition
{
    /**
     * @ORM\ManyToOne(targetEntity="Acquisition", inversedBy="recognitions")
     *
     * @var Acquisition
     */
    protected $acquisition;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     *
     * @var Tag[]|ArrayCollection
     */
    private $selectedTags;

    /**
     * @var Respondent
     * @ORM\ManyToOne(targetEntity="Respondent", cascade={"persist"}, inversedBy="recognitions")
     */
    private $respondent;

    public function __construct()
    {
        $this->selectedTags = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAcquisition()
    {
        return $this->acquisition;
    }

    /**
     * @param mixed $acquisition
     *
     * @return Recognition
     */
    public function setAcquisition($acquisition)
    {
        $this->acquisition = $acquisition;

        return $this;
    }

    /**
     * @param Tag $tag
     */
    public function addSelectedTag(Tag $tag)
    {
        $this->selectedTags->add($tag);
    }

    /**
     * @param Tag $tag
     */
    public function removeSelectedTag(Tag $tag)
    {
        $this->selectedTags->removeElement($tag);
    }

    /**
     * @return Tag[]|ArrayCollection
     */
    public function getSelectedTags()
    {
        return $this->selectedTags;
    }

    /**
     * @return Respondent
     */
    public function getRespondent(): Respondent
    {
        return $this->respondent;
    }

    /**
     * @param Respondent $respondent
     */
    public function setRespondent(Respondent $respondent)
    {
        $this->respondent = $respondent;
    }

    public function getRecognitionRate()
    {
        $total = 0;
        $goodAnswer = 0;
        foreach ($this->getSelectedTags() as $tag) {
            ++$total;
            if ($this->acquisition->getMotion()->getTags()->contains($tag)) {
                ++$goodAnswer;
            }
        }

        return ($total) ? $goodAnswer / $total * 100 : 0;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RespondentRepository")
 */
class Respondent
{
    const GENDERS = ['Male', 'Female'];
    const MIN_AGE = 1;
    const MAX_AGE = 120;

    const MOTOR_EXPERTISE_LEVELS = ['Beginner', 'Intermediate', 'Expert'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $gender;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(min=3)
     */
    private $sensoryMotorExpertise;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(choices=Respondent::MOTOR_EXPERTISE_LEVELS)
     */
    private $motorExpertiseLevel;

    /**
     * @var Recognition[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Recognition", mappedBy="respondent")
     */
    private $recognitions;

    public function __construct()
    {
        $this->recognitions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     *
     * @return $this
     */
    public function setAge(int $age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Recognition[]|ArrayCollection
     */
    public function getRecognitions()
    {
        return $this->recognitions;
    }

    /**
     * @param Recognition $recognition
     */
    public function addRecognition(Recognition $recognition)
    {
        $this->recognitions->add($recognition);
    }

    /**
     * @param Recognition $recognition
     */
    public function removeRecognition(Recognition $recognition)
    {
        $this->recognitions->removeElement($recognition);
    }

    /**
     * @return string
     */
    public function getSensoryMotorExpertise(): ?string
    {
        return $this->sensoryMotorExpertise;
    }

    /**
     * @param string $sensoryMotorExpertise
     */
    public function setSensoryMotorExpertise(?string $sensoryMotorExpertise)
    {
        $this->sensoryMotorExpertise = $sensoryMotorExpertise;
    }

    /**
     * @return string
     */
    public function getMotorExpertiseLevel(): ?string
    {
        return $this->motorExpertiseLevel;
    }

    /**
     * @param string $motorExpertiseLevel
     */
    public function setMotorExpertiseLevel(?string $motorExpertiseLevel)
    {
        $this->motorExpertiseLevel = $motorExpertiseLevel;
    }

    public function getRecognitionRate()
    {
        $allRates = 0;
        foreach ($this->recognitions as $recognition) {
            $allRates += $recognition->getRecognitionRate();
        }

        return ($this->recognitions->count()) ? $allRates / $this->recognitions->count() : 0;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleAttachmentRepository")
 * @Vich\Uploadable()
 */
class ArticleAttachment
{
    const APP_EXPORT_ATTRIBUTES = [
        'file' => ['name', 'originalName', 'mimeType', 'size', 'dimensions'],
        'updatedAt',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Article|null
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="attachments")
     */
    private $article;

    /**
     * @Vich\UploadableField(mapping="articleAttachment_file", fileNameProperty="file.name", size="file.size", mimeType="file.mimeType", originalName="file.originalName", dimensions="file.dimensions")
     *
     * @var File
     */
    private $fileFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $file;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->file = new EmbeddedFile();
        $this->updatedAt = new \DateTime();
    }

    public function getFileFile(): ?File
    {
        return $this->fileFile;
    }

    /**
     * @param File|UploadedFile $file
     */
    public function setFileFile(?File $file = null)
    {
        $this->fileFile = $file;

        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Article|null
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Article|null $article
     *
     * @return $this
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    public function __toString()
    {
        return $this->getFile()->getOriginalName();
    }

    public function getFile(): ?EmbeddedFile
    {
        return $this->file;
    }

    public function setFile(EmbeddedFile $file)
    {
        $this->file = $file;
    }

    public function initFromExport($data)
    {
        $this->file->setName($data['file']['name']);
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param ?\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}

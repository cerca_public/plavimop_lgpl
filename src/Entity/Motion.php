<?php

namespace App\Entity;

use App\Entity\Translation\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MotionRepository")
 *
 * @method MotionTranslation translate($locale = null, $fallbackToDefault = true)
 * @ORM\Table(name="motion")
 */
class Motion
{
    use TranslatableTrait;
    const PAGINATION_LIST_LIMIT = 8;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="motions", cascade={"persist"})
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="Acquisition", mappedBy="motion", cascade={"persist"})
     */
    private $acquisitions;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->acquisitions = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->proxyCurrentLocaleTranslation('getTitle');
    }

    public function setTitle($title)
    {
        $this->proxyCurrentLocaleTranslation('setTitle', [$title]);
        $this->mergeNewTranslations();
    }

    /**
     * @param Tag[] $tags
     *
     * @return $this
     */
    public function setTags(array $tags): Motion
    {
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }

        return $this;
    }

    /**
     * Add tag.
     *
     * @param Tag $tag
     *
     * @return Motion
     */
    public function addTag(Tag $tag): Motion
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag.
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags.
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add acquisition.
     *
     * @param Acquisition $acquisition
     *
     * @return Motion
     */
    public function addAcquisition(Acquisition $acquisition): Motion
    {
        $this->acquisitions[] = $acquisition;
        $acquisition->setMotion($this);

        return $this;
    }

    /**
     * Remove acquisition.
     *
     * @param Acquisition $acquisition
     */
    public function removeAcquisition(Acquisition $acquisition)
    {
        $this->acquisitions->removeElement($acquisition);
    }

    /**
     * Get acquisitions.
     *
     * @return Collection
     */
    public function getAcquisitions()
    {
        return $this->acquisitions;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}

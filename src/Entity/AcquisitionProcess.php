<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AcquisitionProcessRepository")
 * @Vich\Uploadable()
 */
class AcquisitionProcess
{
    const CAPTURES_SYSTEMS_NAMES = [
        'Kinect',
        'Leap Motion',
        'Motion Analisys',
        'Optitrack',
        'Qualisys',
        'Vicon-Nexus',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="acquisitionProcess")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var Acquisition[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Acquisition", mappedBy="acquisitionProcess")
     */
    private $acquisitions;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Range(min="1")
     */
    private $frameRate;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Range(min="0")
     */
    private $numberOfMarkers;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $captureSystemName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $numberAndTypeOfCameraSensors;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank()
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $document;

    /**
     * @Vich\UploadableField(mapping="acquisitionProcess_document", fileNameProperty="document.name", size="document.size", mimeType="document.mimeType", originalName="document.originalName", dimensions="document.dimensions")
     * @Assert\File(mimeTypes={"application/pdf","image/*"})
     *
     * @var File
     */
    private $documentFile;

    /**
     * AcquisitionProcess constructor.
     */
    public function __construct()
    {
        $this->document = new EmbeddedFile();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return AcquisitionProcess
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getFrameRate()
    {
        return $this->frameRate;
    }

    /**
     * @param int $frameRate
     *
     * @return AcquisitionProcess
     */
    public function setFrameRate(int $frameRate)
    {
        $this->frameRate = $frameRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfMarkers(): ?int
    {
        return $this->numberOfMarkers;
    }

    /**
     * @param int $numberOfMarkers
     *
     * @return AcquisitionProcess
     */
    public function setNumberOfMarkers(int $numberOfMarkers): self
    {
        $this->numberOfMarkers = $numberOfMarkers;

        return $this;
    }

    /**
     * @return string
     */
    public function getCaptureSystemName(): ?string
    {
        return $this->captureSystemName;
    }

    /**
     * @param string $captureSystemName
     *
     * @return AcquisitionProcess
     */
    public function setCaptureSystemName(string $captureSystemName): self
    {
        $this->captureSystemName = $captureSystemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumberAndTypeOfCameraSensors(): ?string
    {
        return $this->numberAndTypeOfCameraSensors;
    }

    /**
     * @param string $numberAndTypeOfCameraSensors
     *
     * @return AcquisitionProcess
     */
    public function setNumberAndTypeOfCameraSensors(string $numberAndTypeOfCameraSensors): self
    {
        $this->numberAndTypeOfCameraSensors = $numberAndTypeOfCameraSensors;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Acquisition[]|ArrayCollection
     */
    public function getAcquisitions()
    {
        return $this->acquisitions;
    }

    /**
     * @param Acquisition $acquisition
     */
    public function addAcquisition(Acquisition $acquisition)
    {
        $this->acquisitions->add($acquisition);
    }

    /**
     * @param Acquisition $acquisition
     */
    public function removeAcquisition(Acquisition $acquisition)
    {
        $this->acquisitions->removeElement($acquisition);
    }

    /**
     * @return EmbeddedFile|null
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param $document
     *
     * @return AcquisitionProcess
     */
    public function setDocument($document)
    {
        if ($document) {
            $this->document = $document;
        } else {
            $this->document = new EmbeddedFile();
        }

        return $this;
    }

    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    /**
     * @param File|UploadedFile $documentFile
     */
    public function setDocumentFile(?File $documentFile = null)
    {
        $this->documentFile = $documentFile;
        if (null !== $documentFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SurveyRepository")
 */
class Survey
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $nbRecognitions = 0;

    /**
     * @var ArrayCollection|TagType[]
     * @ORM\ManyToMany(targetEntity="TagType")
     */
    private $tagTypes;

    /** @var Respondent */
    private $respondent;

    /** @var Recognition[] */
    private $recognitions;

    /**
     * Survey constructor.
     */
    public function __construct()
    {
        $this->tagTypes = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|iterable
     */
    public function getTagTypes()
    {
        return $this->tagTypes;
    }

    /**
     * @param iterable|ArrayCollection $tagTypes
     *
     * @return $this
     */
    public function setTagTypes(iterable $tagTypes): Survey
    {
        $this->tagTypes = $tagTypes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbRecognitions(): int
    {
        return $this->nbRecognitions;
    }

    /**
     * @param mixed $nbRecognitions
     */
    public function setNbRecognitions($nbRecognitions)
    {
        $this->nbRecognitions = $nbRecognitions;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Recognition $recognition
     */
    public function addRecognition(Recognition $recognition)
    {
        $this->recognitions[] = $recognition;
    }

    /**
     * @return mixed
     */
    public function getRespondent()
    {
        return $this->respondent;
    }

    /**
     * @param mixed $respondent
     *
     * @return Survey
     */
    public function setRespondent($respondent): Survey
    {
        $this->respondent = $respondent;

        return $this;
    }

    /**
     * @return Recognition[]
     */
    public function getRecognitions(): array
    {
        return $this->recognitions;
    }

    /**
     * @param Recognition $recognition
     */
    public function removeRecognition(Recognition $recognition)
    {
        if (false !== $key = array_search($recognition, $this->recognitions, true)) {
            array_splice($this->recognitions, $key, 1);
        }
    }
}

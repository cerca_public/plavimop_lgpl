<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @Vich\Uploadable()
 */
class EditorUpload
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="editorUpload_file", fileNameProperty="file.name", size="file.size", mimeType="file.mimeType", originalName="file.originalName", dimensions="file.dimensions")
     *
     * @var File
     */
    private $fileFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $file;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->file = new EmbeddedFile();
        $this->updatedAt = new \DateTime();
    }

    public function getFileFile(): ?File
    {
        return $this->fileFile;
    }

    /**
     * @param File|UploadedFile $file
     */
    public function setFileFile(?File $file = null)
    {
        $this->fileFile = $file;

        if (null !== $file) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFile(): ?EmbeddedFile
    {
        return $this->file;
    }

    public function setFile(EmbeddedFile $file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}

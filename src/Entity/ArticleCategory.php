<?php

namespace App\Entity;

use App\Entity\Translation\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleCategoryRepository")
 *
 * @method ArticleCategoryTranslation translate($locale = null, $fallbackToDefault = true)
 */
class ArticleCategory
{
    use ORMBehaviors\Sluggable\Sluggable;
    use TranslatableTrait;

    /**
     * @var ArticleCategoryTranslation[]
     *
     * @Assert\Valid()
     */
    protected $translations;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var Article[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     */
    private $articles;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Article[]|ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $this->articles->add($article);
        $article->setCategory($this);
    }

    /**
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getName()
    {
        return $this->proxyCurrentLocaleTranslation('getName');
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string[]
     */
    public function getSluggableFields()
    {
        return ['name'];
    }

    public function setName($name)
    {
        $this->proxyCurrentLocaleTranslation('setName', [$name]);
        $this->mergeNewTranslations();
    }

    public function getDescription()
    {
        return $this->proxyCurrentLocaleTranslation('getDescription');
    }

    public function setDescription($description)
    {
        $this->proxyCurrentLocaleTranslation('setDescription', [$description]);
        $this->mergeNewTranslations();
    }
}

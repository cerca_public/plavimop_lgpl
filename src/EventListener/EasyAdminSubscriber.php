<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 26/02/2018
 * Time: 23:21.
 */

namespace App\EventListener;

use App\Entity\User;
use App\Manager\UserManager;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            EasyAdminEvents::PRE_PERSIST => ['updateUserPassword'],
            EasyAdminEvents::PRE_UPDATE => ['updateUserPassword'],
        ];
    }

    public function updateUserPassword(GenericEvent $event)
    {
        $entity = $event->getSubject();

        if (!($entity instanceof User && $entity->getPlainPassword())) {
            return;
        }

        $this->userManager->encodePassword($entity, $entity->getPlainPassword());

        //$event['entity'] = $entity;
    }
}

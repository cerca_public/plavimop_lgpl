<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180624213145 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE motion_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_465ADDF02C2AC5D3 (translatable_id), UNIQUE INDEX motion_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE motion_translation ADD CONSTRAINT FK_465ADDF02C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES motion (id) ON DELETE CASCADE'
        );

        $this->addSql(
            'INSERT INTO motion_translation (translatable_id, title, locale) SELECT motion.id, motion.title, \'en\' FROM motion'
        );
        $this->addSql('ALTER TABLE motion DROP title');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE motion ADD title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');

        $this->addSql(
            'UPDATE motion LEFT JOIN motion_translation ON motion.id  = motion_translation.translatable_id AND motion_translation.locale = \'en\' SET motion.title = motion_translation.title'
        );
        $this->addSql('DROP TABLE motion_translation');
    }
}

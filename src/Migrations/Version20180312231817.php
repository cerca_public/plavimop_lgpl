<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180312231817 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE acquisition ADD preview_duration SMALLINT DEFAULT 2000 NOT NULL, ADD preview_marker_size SMALLINT DEFAULT 6 NOT NULL, ADD preview_azimuth SMALLINT DEFAULT 30 NOT NULL, ADD preview_elevation SMALLINT DEFAULT 30 NOT NULL'
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE acquisition DROP preview_duration, DROP preview_marker_size, DROP preview_azimuth, DROP preview_elevation'
        );
    }
}

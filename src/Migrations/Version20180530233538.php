<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180530233538 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE tag_type_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, `label` VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_AB2FE54B2C2AC5D3 (translatable_id), UNIQUE INDEX tag_type_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE tag_type_translation ADD CONSTRAINT FK_AB2FE54B2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tag_type (id) ON DELETE CASCADE'
        );

        $this->addSql(
            'INSERT INTO tag_type_translation (translatable_id, label, locale) SELECT tag_type.id, tag_type.label, \'en\' FROM tag_type'
        );
        $this->addSql('ALTER TABLE tag_type DROP `label`');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE tag_type ADD `label` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');

        $this->addSql(

            'UPDATE tag_type LEFT JOIN tag_type_translation ON tag_type.id  = tag_type_translation.translatable_id AND tag_type_translation.locale = \'en\' SET tag_type.label = tag_type_translation.label'
        );

        $this->addSql('DROP TABLE tag_type_translation');
    }
}

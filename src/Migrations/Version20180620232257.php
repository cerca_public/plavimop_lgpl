<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180620232257 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE article_category_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_7FD892502C2AC5D3 (translatable_id), UNIQUE INDEX article_category_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE article_category_translation ADD CONSTRAINT FK_7FD892502C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES article_category (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'INSERT INTO article_category_translation (translatable_id, name,description, locale) SELECT article_category.id, article_category.name, article_category.description,  \'en\' FROM article_category'
        );
        $this->addSql(
            'ALTER TABLE article_category DROP name, DROP description, CHANGE slug slug VARCHAR(255) DEFAULT NULL'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE article_category ADD name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD description VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci'
        );

        $this->addSql(

            'UPDATE article_category LEFT JOIN article_category_translation ON article_category.id  = article_category_translation.translatable_id AND article_category_translation.locale = \'en\' SET article_category.name = article_category_translation.name, article_category.description = article_category_translation.description'
        );

        $this->addSql('DROP TABLE article_category_translation');
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180213010011 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE recognition DROP FOREIGN KEY FK_95DB6FB2C86048C6');
        $this->addSql('DROP TABLE acquisition_video');
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB9033697B0F4C');
        $this->addSql('DROP INDEX IDX_2FEB9033697B0F4C ON acquisition');
        $this->addSql(
            'ALTER TABLE acquisition ADD updated_at DATETIME NOT NULL, ADD preview_name VARCHAR(255) DEFAULT NULL, ADD preview_original_name VARCHAR(255) DEFAULT NULL, ADD preview_mime_type VARCHAR(255) DEFAULT NULL, ADD preview_size INT DEFAULT NULL, ADD preview_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', ADD file_name VARCHAR(255) DEFAULT NULL, ADD file_original_name VARCHAR(255) DEFAULT NULL, ADD file_mime_type VARCHAR(255) DEFAULT NULL, ADD file_size INT DEFAULT NULL, ADD file_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', DROP file, DROP format, CHANGE institute_id user_id INT DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB9033A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)'
        );
        $this->addSql('CREATE INDEX IDX_2FEB9033A76ED395 ON acquisition (user_id)');
        $this->addSql('DROP INDEX IDX_95DB6FB2C86048C6 ON recognition');
        $this->addSql('ALTER TABLE recognition CHANGE acquisition_video_id acquisition_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE recognition ADD CONSTRAINT FK_95DB6FB26F52F3C FOREIGN KEY (acquisition_id) REFERENCES acquisition (id)'
        );
        $this->addSql('CREATE INDEX IDX_95DB6FB26F52F3C ON recognition (acquisition_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE acquisition_video (id INT AUTO_INCREMENT NOT NULL, acquisition_id INT DEFAULT NULL, file VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, format VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, view VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_9778A3476F52F3C (acquisition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE acquisition_video ADD CONSTRAINT FK_9778A3476F52F3C FOREIGN KEY (acquisition_id) REFERENCES acquisition (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB9033A76ED395');
        $this->addSql('DROP INDEX IDX_2FEB9033A76ED395 ON acquisition');
        $this->addSql(
            'ALTER TABLE acquisition ADD institute_id INT DEFAULT NULL, ADD file VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD format VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP user_id, DROP updated_at, DROP preview_name, DROP preview_original_name, DROP preview_mime_type, DROP preview_size, DROP preview_dimensions, DROP file_name, DROP file_original_name, DROP file_mime_type, DROP file_size, DROP file_dimensions'
        );
        $this->addSql(
            'ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB9033697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)'
        );
        $this->addSql('CREATE INDEX IDX_2FEB9033697B0F4C ON acquisition (institute_id)');
        $this->addSql('ALTER TABLE recognition DROP FOREIGN KEY FK_95DB6FB26F52F3C');
        $this->addSql('DROP INDEX IDX_95DB6FB26F52F3C ON recognition');
        $this->addSql('ALTER TABLE recognition CHANGE acquisition_id acquisition_video_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE recognition ADD CONSTRAINT FK_95DB6FB2C86048C6 FOREIGN KEY (acquisition_video_id) REFERENCES acquisition_video (id)'
        );
        $this->addSql('CREATE INDEX IDX_95DB6FB2C86048C6 ON recognition (acquisition_video_id)');
    }
}

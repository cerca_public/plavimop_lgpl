<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180308003401 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE user ADD institute_id INT DEFAULT NULL, ADD title VARCHAR(255) NOT NULL, ADD first_name VARCHAR(255) NOT NULL, ADD country VARCHAR(255) NOT NULL, ADD research_topic LONGTEXT DEFAULT NULL, CHANGE full_name last_name VARCHAR(255) NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE user ADD CONSTRAINT FK_8D93D649697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)'
        );
        $this->addSql('CREATE INDEX IDX_8D93D649697B0F4C ON user (institute_id)');
        $this->addSql(
            'ALTER TABLE institute CHANGE address address LONGTEXT DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE url url VARCHAR(255) DEFAULT NULL'
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE institute CHANGE address address LONGTEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE email email VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE url url VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci'
        );
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649697B0F4C');
        $this->addSql('DROP INDEX IDX_8D93D649697B0F4C ON user');
        $this->addSql(
            'ALTER TABLE user ADD full_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP institute_id, DROP last_name, DROP title, DROP first_name, DROP country, DROP research_topic'
        );
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180129004628 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql(
            'CREATE TABLE publication_attachment (id INT AUTO_INCREMENT NOT NULL, publication_id INT DEFAULT NULL, updated_at DATETIME NOT NULL, file_name VARCHAR(255) DEFAULT NULL, file_original_name VARCHAR(255) DEFAULT NULL, file_mime_type VARCHAR(255) DEFAULT NULL, file_size INT DEFAULT NULL, file_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', INDEX IDX_17289C5A38B217A7 (publication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE publication_attachment ADD CONSTRAINT FK_17289C5A38B217A7 FOREIGN KEY (publication_id) REFERENCES publication (id)'
        );
        $this->addSql('DROP TABLE publication_acquisition');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE publication_acquisition (publication_id INT NOT NULL, acquisition_id INT NOT NULL, INDEX IDX_F888359838B217A7 (publication_id), INDEX IDX_F88835986F52F3C (acquisition_id), PRIMARY KEY(publication_id, acquisition_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE publication_acquisition ADD CONSTRAINT FK_F888359838B217A7 FOREIGN KEY (publication_id) REFERENCES publication (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE publication_acquisition ADD CONSTRAINT FK_F88835986F52F3C FOREIGN KEY (acquisition_id) REFERENCES acquisition (id) ON DELETE CASCADE'
        );
        $this->addSql('DROP TABLE publication_attachment');
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180125154611 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE survey (id INT AUTO_INCREMENT NOT NULL, nb_recognitions INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE survey_tag_type (survey_id INT NOT NULL, tag_type_id INT NOT NULL, INDEX IDX_3E26AEB8B3FE509D (survey_id), INDEX IDX_3E26AEB8F77DA139 (tag_type_id), PRIMARY KEY(survey_id, tag_type_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE survey_tag_type ADD CONSTRAINT FK_3E26AEB8B3FE509D FOREIGN KEY (survey_id) REFERENCES survey (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE survey_tag_type ADD CONSTRAINT FK_3E26AEB8F77DA139 FOREIGN KEY (tag_type_id) REFERENCES tag_type (id) ON DELETE CASCADE'
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE survey_tag_type DROP FOREIGN KEY FK_3E26AEB8B3FE509D');
        $this->addSql('DROP TABLE survey');
        $this->addSql('DROP TABLE survey_tag_type');
    }
}

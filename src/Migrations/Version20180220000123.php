<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180220000123 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE acquisition_process (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, frame_rate SMALLINT NOT NULL, number_of_frame INT NOT NULL, number_of_markers SMALLINT NOT NULL, capture_system_name VARCHAR(255) NOT NULL, number_and_type_of_camera_sensors VARCHAR(255) NOT NULL, INDEX IDX_29CD88DDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE acquisition_process ADD CONSTRAINT FK_29CD88DDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)'
        );
        $this->addSql('ALTER TABLE acquisition ADD acquisition_process_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB9033F59FCF34 FOREIGN KEY (acquisition_process_id) REFERENCES acquisition_process (id)'
        );
        $this->addSql('CREATE INDEX IDX_2FEB9033F59FCF34 ON acquisition (acquisition_process_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB9033F59FCF34');
        $this->addSql('DROP TABLE acquisition_process');
        $this->addSql('DROP INDEX IDX_2FEB9033F59FCF34 ON acquisition');
        $this->addSql('ALTER TABLE acquisition DROP acquisition_process_id');
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180303002947 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE publication_attachment DROP FOREIGN KEY FK_17289C5A38B217A7');
        $this->addSql(
            'CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE article_attachment (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, updated_at DATETIME NOT NULL, file_name VARCHAR(255) DEFAULT NULL, file_original_name VARCHAR(255) DEFAULT NULL, file_mime_type VARCHAR(255) DEFAULT NULL, file_size INT DEFAULT NULL, file_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', INDEX IDX_4586083A7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE article_attachment ADD CONSTRAINT FK_4586083A7294869C FOREIGN KEY (article_id) REFERENCES article (id)'
        );
        $this->addSql('DROP TABLE publication');
        $this->addSql('DROP TABLE publication_attachment');
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB90332CA9166E');
        $this->addSql(
            'ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB90332CA9166E FOREIGN KEY (motion_id) REFERENCES motion (id)'
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE article_attachment DROP FOREIGN KEY FK_4586083A7294869C');
        $this->addSql(
            'CREATE TABLE publication (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, content LONGTEXT NOT NULL COLLATE utf8_unicode_ci, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE publication_attachment (id INT AUTO_INCREMENT NOT NULL, publication_id INT DEFAULT NULL, updated_at DATETIME NOT NULL, file_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, file_original_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, file_mime_type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, file_size INT DEFAULT NULL, file_dimensions LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:simple_array)\', INDEX IDX_17289C5A38B217A7 (publication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE publication_attachment ADD CONSTRAINT FK_17289C5A38B217A7 FOREIGN KEY (publication_id) REFERENCES publication (id)'
        );
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_attachment');
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB90332CA9166E');
        $this->addSql(
            'ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB90332CA9166E FOREIGN KEY (motion_id) REFERENCES motion (id) ON DELETE CASCADE'
        );
    }
}

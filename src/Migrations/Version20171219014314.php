<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219014314 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE recognition (id INT AUTO_INCREMENT NOT NULL, acquisition_video_id INT DEFAULT NULL, respondent_id INT DEFAULT NULL, INDEX IDX_95DB6FB2C86048C6 (acquisition_video_id), INDEX IDX_95DB6FB2CE80CD19 (respondent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recognition_tag (recognition_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_58BBDF6A53FF93EF (recognition_id), INDEX IDX_58BBDF6ABAD26311 (tag_id), PRIMARY KEY(recognition_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE respondent (id INT AUTO_INCREMENT NOT NULL, age SMALLINT NOT NULL, gender VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recognition ADD CONSTRAINT FK_95DB6FB2C86048C6 FOREIGN KEY (acquisition_video_id) REFERENCES acquisition_video (id)');
        $this->addSql('ALTER TABLE recognition ADD CONSTRAINT FK_95DB6FB2CE80CD19 FOREIGN KEY (respondent_id) REFERENCES respondent (id)');
        $this->addSql('ALTER TABLE recognition_tag ADD CONSTRAINT FK_58BBDF6A53FF93EF FOREIGN KEY (recognition_id) REFERENCES recognition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recognition_tag ADD CONSTRAINT FK_58BBDF6ABAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recognition_tag DROP FOREIGN KEY FK_58BBDF6A53FF93EF');
        $this->addSql('ALTER TABLE recognition DROP FOREIGN KEY FK_95DB6FB2CE80CD19');
        $this->addSql('DROP TABLE recognition');
        $this->addSql('DROP TABLE recognition_tag');
        $this->addSql('DROP TABLE respondent');
    }
}

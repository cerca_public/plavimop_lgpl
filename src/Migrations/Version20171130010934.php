<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171130010934 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acquisition (id INT AUTO_INCREMENT NOT NULL, motion_id INT DEFAULT NULL, institute_id INT DEFAULT NULL, acq_date DATE DEFAULT NULL, file VARCHAR(255) NOT NULL, download_counter INT DEFAULT 0 NOT NULL, format VARCHAR(255) NOT NULL, INDEX IDX_2FEB90332CA9166E (motion_id), INDEX IDX_2FEB9033697B0F4C (institute_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acquisition_video (id INT AUTO_INCREMENT NOT NULL, acquisition_id INT DEFAULT NULL, file VARCHAR(255) NOT NULL, format VARCHAR(255) NOT NULL, view VARCHAR(255) NOT NULL, INDEX IDX_9778A3476F52F3C (acquisition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address LONGTEXT NOT NULL, email VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE motion (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE motion_tag (motion_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_C27B3A592CA9166E (motion_id), INDEX IDX_C27B3A59BAD26311 (tag_id), PRIMARY KEY(motion_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, `label` VARCHAR(255) NOT NULL, INDEX IDX_389B783C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_type (id INT AUTO_INCREMENT NOT NULL, `label` VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB90332CA9166E FOREIGN KEY (motion_id) REFERENCES motion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acquisition ADD CONSTRAINT FK_2FEB9033697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE acquisition_video ADD CONSTRAINT FK_9778A3476F52F3C FOREIGN KEY (acquisition_id) REFERENCES acquisition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE motion_tag ADD CONSTRAINT FK_C27B3A592CA9166E FOREIGN KEY (motion_id) REFERENCES motion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE motion_tag ADD CONSTRAINT FK_C27B3A59BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783C54C8C93 FOREIGN KEY (type_id) REFERENCES tag_type (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acquisition_video DROP FOREIGN KEY FK_9778A3476F52F3C');
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB9033697B0F4C');
        $this->addSql('ALTER TABLE acquisition DROP FOREIGN KEY FK_2FEB90332CA9166E');
        $this->addSql('ALTER TABLE motion_tag DROP FOREIGN KEY FK_C27B3A592CA9166E');
        $this->addSql('ALTER TABLE motion_tag DROP FOREIGN KEY FK_C27B3A59BAD26311');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783C54C8C93');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE acquisition');
        $this->addSql('DROP TABLE acquisition_video');
        $this->addSql('DROP TABLE institute');
        $this->addSql('DROP TABLE motion');
        $this->addSql('DROP TABLE motion_tag');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tag_type');
        $this->addSql('DROP TABLE user');
    }
}

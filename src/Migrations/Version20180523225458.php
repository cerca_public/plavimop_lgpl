<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523225458 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE tag_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, `label` VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A8A03F8F2C2AC5D3 (translatable_id), UNIQUE INDEX tag_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE tag_translation ADD CONSTRAINT FK_A8A03F8F2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES tag (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'INSERT INTO tag_translation (translatable_id, label, locale) SELECT tag.id, tag.label, \'en\' FROM tag'
        );
        $this->addSql('ALTER TABLE tag DROP `label`');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE tag ADD `label` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci'
        );
        $this->addSql(

            'UPDATE tag LEFT JOIN tag_translation ON tag.id  = tag_translation.translatable_id AND tag_translation.locale = \'en\' SET tag.label = tag_translation.label'
        );
        $this->addSql(
            'DROP TABLE tag_translation'
        );
    }
}

<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findAllQuery(ArticleCategory $category = null): \Doctrine\ORM\Query
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.valid = 1');

        if ($category) {
            $qb->andWhere('a.category = :category')
                ->setParameter(':category', $category);
        }
        $qb->orderBy('a.date', 'desc');

        return $qb->getQuery();
    }
}

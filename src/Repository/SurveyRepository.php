<?php

namespace App\Repository;

use App\Entity\Survey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class SurveyRepository.
 */
class SurveyRepository extends ServiceEntityRepository
{
    /**
     * SurveyRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Survey::class);
    }

    /**
     * @return Survey|null
     */
    public function getCurrentSurvey(): ?Survey
    {
        $qb = $this->createQueryBuilder('s')
            ->addSelect('s,types, types_translations,tags, tags_translations')
            ->innerJoin('s.tagTypes', 'types')
            ->innerJoin('types.translations', 'types_translations')
            ->innerJoin('types.tags', 'tags')
            ->innerJoin('tags.translations', 'tags_translations')
            ->setMaxResults(1)
            ->orderBy('s.id', 'desc');

        $paginator = new Paginator($qb->getQuery());

        return $paginator->getIterator()->current();
    }
}

<?php

namespace App\Repository;

use App\Entity\Institute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class InstituteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Institute::class);
    }

    public function getInstitutesNamesArray()
    {
        $res = $this->createQueryBuilder('i')
            ->select('i.name')
            ->getQuery()
            ->getScalarResult();
        array_walk(
            $res,
            function (&$elt) {
                $elt = $elt['name'];
            }
        );

        return $res;
    }
}

<?php

namespace App\Repository;

use App\Entity\Tag;
use App\Entity\TagType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function getFilterTags(TagType $type)
    {
        return $this->createQueryBuilder('t')
            ->addSelect('type')
            ->where('t.type = :type')
            ->setParameter('type', $type)
            ->innerJoin('t.type', 'type');
    }

    public function loadAll()
    {
        $tags = $this->findAll();
        $tagsArray = [];
        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $tagsArray[$tag->getType()->getLabel().'-'.$tag->getLabel()] = $tag;
        }

        return $tagsArray;
    }

    public function getOneByLabel($label)
    {
        return $this->createQueryBuilder('t')
            ->where('translations.label = :label')
            ->setParameter('label', $label)
            ->innerJoin('t.translations', 'translations')
            ->getQuery()
            ->getOneOrNullResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\Acquisition;
use App\Entity\Motion;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;

/**
 * Class MotionRepository.
 */
class MotionRepository extends ServiceEntityRepository
{
    /**
     * @var string
     */
    private $currentLocale;

    /**
     * MotionRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry, $defaultLocale)
    {
        parent::__construct($registry, Motion::class);
        $this->currentLocale = $defaultLocale;
    }

    public function setCurrentLocale($locale)
    {
        $this->currentLocale = $locale;
    }

    public function getMotionShow($id)
    {
        return $this->createQueryBuilder('m')
            ->select('m,tags,types,types_translations,acquisitions,tags_translations,recognitions,recognitionsTags')
            ->innerJoin('m.tags', 'tags')
            ->innerJoin('tags.translations', 'tags_translations')
            ->innerJoin('tags.type', 'types')
            ->innerJoin('types.translations', 'types_translations')
            ->innerJoin('m.acquisitions', 'acquisitions')
            ->leftJoin('acquisitions.recognitions', 'recognitions')
            ->leftJoin('recognitions.selectedTags', 'recognitionsTags')
            ->andWhere('m.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param array $ids
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getMotionsByIdsQueryBuilder($ids = [])
    {
        return $this->getBaseJoinQueryBuilderWithValidAcq()
            ->addSelect('acquisitions, tags, types, tags_translations, motion_translations')
            ->andWhere('m.id IN (:ids)')
            ->setParameter('ids', $ids);
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getBaseJoinQueryBuilder()
    {
        return $this->createQueryBuilder('m')
            ->innerJoin('m.translations', 'motion_translations')
            ->innerJoin('m.acquisitions', 'acquisitions')
            ->innerJoin('m.tags', 'tags')
            ->innerJoin('tags.translations', 'tags_translations')
            ->innerJoin('tags.type', 'types');
    }

    private function getBaseJoinQueryBuilderWithValidAcq()
    {
        return $this->getBaseJoinQueryBuilder()
            ->andWhere('acquisitions.state = :state')
            ->setParameter('state', Acquisition::STATE_IS_VALID);
    }

    /**
     * @param iterable|Tag[] $tags
     *
     * @return mixed
     */
    public function getMotionsFormFilterQuery(iterable $tags)
    {
        if (count($tags)) {
            $queryBuilder = $this->getMotionsByIdsQueryBuilder(
                $this->findMotionIdsFormTags($tags)
            );
        } else {
            $queryBuilder = $this->getBaseJoinQueryBuilderWithValidAcq()
                ->addSelect('acquisitions, tags, types, tags_translations');
        }
        $queryBuilder->andWhere('motion_translations.locale = :locale')
            ->setParameter(':locale', $this->getCurrentLocale());

        return $queryBuilder->orderBy('motion_translations.title', 'asc')->getQuery();
    }

    /**
     * @param iterable|Tag[] $tags
     *
     * @return Motion[]|ArrayCollection
     */
    public function findMotionIdsFormTags(iterable $tags)
    {
        $typeIds = [];
        foreach ($tags as $tag) {
            $typeIds[$tag->getType()->getId()] = true;
        }

        $qb = $this->getBaseJoinQueryBuilder()
            ->select('m.id,COUNT(DISTINCT types) AS HIDDEN tagsTypeCpt,COUNT(DISTINCT tags) AS HIDDEN tagsCpt')
            ->where('tags IN (:tags)')
            ->groupBy('m.id')
            ->having('tagsTypeCpt = :nbType')
            ->setParameter('nbType', count($typeIds))
            ->orderBy('tagsTypeCpt', 'desc')
            ->setParameter(':tags', $tags);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param Tag[] $tags
     *
     * @return Motion|bool
     */
    public function getMotionWithTags($tags)
    {
        $query = $this->createQueryBuilder('m')
            ->select('m, COUNT(tags) AS HIDDEN tagsCpt')
            ->where('tags IN (:tags)')
            ->setParameter('tags', $tags)
            ->innerJoin('m.tags', 'tags')
            ->groupBy('m.id')
            ->having('tagsCpt = :tagsCpt')
            ->setParameter('tagsCpt', count($tags))
            ->getQuery();

        try {
            $return = $query->getSingleResult();
        } catch (NoResultException $e) {
            $return = false;
        }

        return $return;
    }

    /**
     * @return string
     */
    public function getCurrentLocale(): string
    {
        return $this->currentLocale;
    }
}

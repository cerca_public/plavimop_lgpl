<?php

namespace App\Repository;

use App\Entity\Respondent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RespondentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Respondent::class);
    }

    public function findForResult($id)
    {
        return $this->createQueryBuilder('r')
            ->select('r,recognitions, acquisitions, motion, motionTags, recognitionsTags, tagsTypes')
            ->innerJoin('r.recognitions', 'recognitions')
            ->innerJoin('recognitions.acquisition', 'acquisitions')
            ->innerJoin('acquisitions.motion', 'motion')
            ->innerJoin('motion.tags', 'motionTags')
            ->innerJoin('motionTags.type', 'tagsTypes')
            ->innerJoin('recognitions.selectedTags', 'recognitionsTags')
            ->andWhere('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }
}

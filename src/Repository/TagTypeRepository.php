<?php

namespace App\Repository;

use App\Entity\TagType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * TagTyRepository.
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class TagTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TagType::class);
    }

    public function findAllWithTags($with_motion = false)
    {
        $qb = $this->createQueryBuilder('type')
            ->addSelect('type_translations, tag, tag_translations')
            ->innerJoin('type.translations', 'type_translations')
            ->innerJoin('type.tags', 'tag')
            ->innerJoin('tag.translations', 'tag_translations');
        if ($with_motion) {
            $qb->innerJoin('tag.motions', 'motions')
                ->innerJoin('motions.acquisitions', 'acquisitions');
        }

        return $qb->getQuery()
            ->execute();
    }

    public function findAll()
    {
        return $this->getBaseQueryBuilder()
            ->addSelect('translations')
            ->getQuery()
            ->execute();
    }

    private function getBaseQueryBuilder()
    {
        return $this->createQueryBuilder('type')
            ->innerJoin('type.translations', 'translations');
    }

    public function findOneByLabel($label)
    {
        return $this->getBaseQueryBuilder()
            ->where('translations.label = :label')
            ->setParameter('label', $label)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByLabels(iterable $labels)
    {
        return $this->getBaseQueryBuilder()
            ->where('translations.label IN (:labels)')
            ->setParameter('labels', $labels)
            ->getQuery()
            ->execute();
    }
}

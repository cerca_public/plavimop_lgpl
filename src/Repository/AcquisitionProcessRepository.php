<?php

namespace App\Repository;

use App\Entity\AcquisitionProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AcquisitionProcessRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AcquisitionProcess::class);
    }

    public function getByUserQueryBuilder(User $user)
    {
        return $this->createQueryBuilder('ap')
            ->where('ap.user = :user')
            ->setParameter('user', $user);
    }
}

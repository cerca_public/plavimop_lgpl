<?php

namespace App\Repository;

use App\Entity\Acquisition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class AcquisitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Acquisition::class);
    }

    public function getSurveyAcquisitions(int $limit, $formIds = null): array
    {
        if ($formIds) {
            $ids = $formIds;
        } else {
            $ids = [];
            $res_ids = $this->createQueryBuilder('a')
                ->select('GROUP_CONCAT(a.id)')
                ->andWhere('a.state = :state')
                ->andWhere('a.enabledSurvey = :survey')
                ->setParameter('state', Acquisition::STATE_IS_VALID)
                ->setParameter('survey', Acquisition::ENABLED_SURVEY)
                ->groupBy('a.motion')
                ->setMaxResults($limit)
                ->orderBy('RAND()')
                ->getQuery()
                ->getScalarResult();
            foreach ($res_ids as $id) {
                $arr_ids = explode(',', reset($id));
                shuffle($arr_ids);
                $ids[] = (int)reset($arr_ids);
            }
        }
        $qb = $this->getByIdsQuery($ids);

        return [$ids, $qb->execute()];
    }

    /**
     * @param array $ids
     *
     * @return Query
     */
    private function getByIdsQuery(array $ids = []): Query
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery();
    }

    public function getAcquisitionExportRecognitions($id)
    {
        $qb = $this->createQueryBuilder('a')
            ->select(
                'a, motion, motion_tags, motion_tags_type, recognitions, recognitions_respondents,recognitions_tags'
            )
            ->innerJoin('a.motion', 'motion')
            ->innerJoin('motion.tags', 'motion_tags')
            ->innerJoin('motion_tags.type', 'motion_tags_type')
            ->leftJoin('a.recognitions', 'recognitions')
            ->leftJoin('recognitions.respondent', 'recognitions_respondents')
            ->leftJoin('recognitions.selectedTags', 'recognitions_tags')
            ->where('a.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @return Acquisition[]
     */
    public function getExportNeedPreview(): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.needNewPreviewGeneration = 1')
            ->orWhere('a.preview.name IS NULL');

        return $qb->getQuery()->getResult();
    }
}

<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Manager\PageManager;
use App\Service\Mailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_index")
     *
     * @param Request     $request
     * @param PageManager $pageManager
     * @param Mailer      $mailer
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contact(Request $request, PageManager $pageManager, Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Captcha validation passed
            $message = 'CAPTCHA validation passed, human visitor confirmed!';
            if ($mailer->sendMail($form->getData()) >= 1) {
                $this->addFlash('success', 'flash.contact.message_send');
            }

            return $this->redirectToRoute('contact_index');
        }

        return $this->render('home/contact.html.twig', [
            'form' => $form->createView(),
            'page' => $pageManager->getOrCreatePage('contact', 'Contact us !'),
        ]);
    }
}

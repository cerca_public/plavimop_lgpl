<?php

namespace App\Controller;

use App\Entity\Motion;
use App\Form\MotionFilterType;
use App\Manager\PageManager;
use App\Manager\ParameterManager;
use App\Model\MotionFilter;
use App\Repository\MotionRepository;
use App\Service\Paginator;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MotionController extends AbstractController
{
    /**
     * @Route("/motions", defaults={"page": "1"}, name="motion_index")
     * @Route("/motions/{page}", requirements={"page": "[1-9]\d*"}, name="motion_index_paginated")
     * @Method({"GET","POST"})
     *
     * @param                  $page
     * @param Request          $request
     * @param MotionFilter     $motionFilter
     * @param MotionRepository $motionRepository
     * @param Paginator        $paginator
     * @param SessionInterface $session
     * @param PageManager      $pageManager
     * @param ParameterManager $parameterManager
     *
     * @return Response
     *
     * @internal param EntityManagerInterface $em
     * @internal param MotionRepository $motionRepository
     * @internal param EntityManagerInterface $em
     */
    public function index(
        $page,
        Request $request,
        MotionFilter $motionFilter,
        MotionRepository $motionRepository,
        Paginator $paginator,
        SessionInterface $session,
        PageManager $pageManager,
        ParameterManager $parameterManager
    ) {
        $motionFilter = $session->get('motionFilter') ?? $motionFilter;
        $motionRepository->setCurrentLocale($request->getLocale());
        $form = $this->createForm(MotionFilterType::class, $motionFilter);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $session->set('motionFilter', $motionFilter);
        }
        /** @var Pagerfanta $paginator */
        $paginator = $paginator->create(
            $motionRepository->getMotionsFormFilterQuery($motionFilter->getSelectedTags()),
            $page,
            Motion::PAGINATION_LIST_LIMIT
        );

        return $this->render(
            'motion/index.html.twig',
            [
                'motionFilter' => $motionFilter,
                'motions' => $paginator,
                'form' => $form->createView(),
                'page' => $pageManager->getOrCreatePage('database', 'Database'),
                'parameters' => $parameterManager->getParameters(),
            ]
        );
    }

    /**
     * @Route("motions/clean", name="motion_filter_resetting")
     * @Method({"GET"})
     *
     * @param SessionInterface $session
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetSearch(SessionInterface $session)
    {
        $session->remove('motionFilter');
        $this->addFlash('success', 'flash.motion_filter.resetting');

        return $this->redirectToRoute('motion_index');
    }

    /**
     * @Route("motion/{id}", name="motion_show")
     * @Entity("motion", expr="repository.getMotionShow(id)")
     *
     * @param Motion $motion
     *
     * @return Response
     */
    public function show(Motion $motion)
    {
        return $this->render(
            'motion/show.html.twig',
            [
                'motion' => $motion,
            ]
        );
    }
}

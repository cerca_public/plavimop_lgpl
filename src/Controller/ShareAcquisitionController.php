<?php

namespace App\Controller;

use App\Entity\Acquisition;
use App\Entity\AcquisitionProcess;
use App\Entity\Motion;
use App\Form\AcquisitionProcessSelectorType;
use App\Form\AcquisitionProcessType;
use App\Form\AcquisitionType;
use App\Form\MotionCreatorType;
use App\Form\MotionSelectorType;
use App\Manager\MotionManager;
use App\Manager\PageManager;
use App\Repository\TagTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Security("has_role('ROLE_CONTRIBUTOR')")
 * @Route(path="/share-acquisition")
 */
class ShareAcquisitionController extends Controller
{
    /**
     * @Route("/",name="share_acquisition_index")
     */
    public function index(PageManager $pageManager)
    {
        return $this->render(
            'shareAcquisition/index.html.twig',
            [
                'page' => $pageManager->getOrCreatePage('share-acquisition', 'How to share acquisition'),
            ]
        );
    }

    /**
     * @Route("/select-process",  name="share_acquisition_select_process")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param AcquisitionProcessManager $acquisitionProcessManager
     */
    public function selectProcess(Request $request, EntityManagerInterface $entityManager)
    {
        $processSelector = $this->createForm(AcquisitionProcessSelectorType::class, null, ['user' => $this->getUser()]);
        $processSelector->handleRequest($request);

        if ($processSelector->isSubmitted() && $processSelector->isValid()) {
            /** @var AcquisitionProcess $acquisitionProcess */
            $acquisitionProcess = $processSelector->getData()['acquisitionProcess'];

            /** @var SubmitButton $saveButton */
            $saveButton = $processSelector->get('save');
            if ($saveButton->isClicked()) {
                return $this->redirectToRoute(
                    'share_acquisition_select_motion',
                    [
                        'processSlug' => $acquisitionProcess->getSlug(),
                    ]
                );
            }
            $acquisitionProcess = clone $acquisitionProcess;
            $acquisitionProcess->setName(null);
            $acquisitionProcess->setDocument(null);
        }
        $acquisitionProcess = $acquisitionProcess ?? new AcquisitionProcess();
        $acquisitionProcess->setUser($this->getUser());

        $processCreator = $this->createForm(AcquisitionProcessType::class, $acquisitionProcess);
        $processCreator->handleRequest($request);

        if ($processCreator->isSubmitted() && $processCreator->isValid()) {
            $entityManager->persist($acquisitionProcess);
            $entityManager->flush();

            return $this->redirectToRoute(
                'share_acquisition_select_motion',
                [
                    'processSlug' => $acquisitionProcess->getSlug(),
                ]
            );
        }

        return $this->render(
            'shareAcquisition/selectProcess.html.twig',
            [
                'processSelector' => $processSelector->createView(),
                'processCreator' => $processCreator->createView(),
            ]
        );
    }

    /**
     * @Route("/{processSlug}",  name="share_acquisition_select_motion")
     * @ParamConverter("acquisitionProcess", options={"mapping" : {"processSlug" : "slug"}})
     * @Security("is_granted('select',acquisitionProcess)")
     *
     * @param AcquisitionProcess $acquisitionProcess
     * @param TagTypeRepository  $tagTypeRepository
     * @param Request            $request
     * @param MotionManager      $motionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function selectMotion(
        AcquisitionProcess $acquisitionProcess,
        TagTypeRepository $tagTypeRepository,
        Request $request,
        MotionManager $motionManager
    ) {

        $motionSelector = $this->createForm(MotionSelectorType::class);
        $motionSelector->handleRequest($request);

        if ($motionSelector->isSubmitted() && $motionSelector->isValid()) {
            /** @var Motion $motion */
            $motion = $motionSelector->getData()['motion'];
            //var_dump($motion);
            return $this->redirectToRoute(
                'share_acquisition_send_acquisition',
                [
                    'motion_id' => $motion->getId(),
                    'processSlug' => $acquisitionProcess->getSlug(),
                ]
            );
        }

        $motionCreator = $this->createForm(
            MotionCreatorType::class,
            ['tags' => []],
            ['typesSelected' => $tagTypeRepository->findAllWithTags()]
        );
        $motionCreator->handleRequest($request);

        if ($motionCreator->isSubmitted() && $motionCreator->isValid()) {
            $tags = $motionCreator->getData()['tags'];
            $motion = $motionManager->createMotion($tags);

            return $this->redirectToRoute(
                'share_acquisition_send_acquisition',
                [
                    'motion_id' => $motion->getId(),
                    'processSlug' => $acquisitionProcess->getSlug(),
                ]
            );
        }

        $motionCreator->handleRequest($request);

        return $this->render(
            'shareAcquisition/selectMotion.html.twig',
            [
                'motionSelector' => $motionSelector->createView(),
                'motionCreator' => $motionCreator->createView(),
                'acquisitionProcess' => $acquisitionProcess,
            ]
        );
    }

    /**
     * @Route("/send-acquisition/{processSlug}/{motion_id}",  name="share_acquisition_send_acquisition")
     * @ParamConverter("motion", options={"id" = "motion_id"})
     * @ParamConverter("acquisitionProcess", options={"mapping" : {"processSlug" : "slug"}})
     * @Security("is_granted('select',acquisitionProcess)")
     *
     * @param AcquisitionProcess     $acquisitionProcess
     * @param Motion                 $motion
     * @param Request                $request
     * @param UserInterface          $user
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function shareAcquisition(
        AcquisitionProcess $acquisitionProcess,
        Motion $motion,
        Request $request,
        UserInterface $user,
        EntityManagerInterface $em
    ) {
        $acquisition = new Acquisition();
        $acquisition->setMotion($motion);
        $acquisition->setAcquisitionProcess($acquisitionProcess);
        $acquisition->setUser($user);
        $acquisition->setState(Acquisition::STATE_NEED_REVIEW);

        $form = $this->createForm(AcquisitionType::class, $acquisition);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($acquisition);
            $em->flush();
            $this->addFlash('success', 'flash.acquisition.create');

            return $this->redirectToRoute(
                'share_acquisition_select_motion',
                [
                    'processSlug' => $acquisitionProcess->getSlug(),
                ]
            );
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($acquisition);
            $em->flush();
            $this->addFlash('success', 'flash.acquisition.create');

            return $this->redirectToRoute(
                'share_acquisition_select_motion',
                [
                    'processSlug' => $acquisitionProcess->getSlug(),
                ]
            );
        }

        return $this->render(
            'shareAcquisition/sendAcquisition.html.twig',
            [
                'form' => $form->createView(),
                'motion' => $motion,
                'acquisitionProcess' => $acquisitionProcess,
            ]
        );
    }
}

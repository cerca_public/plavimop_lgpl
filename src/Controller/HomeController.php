<?php

namespace App\Controller;

use App\Manager\PageManager;
use App\Manager\ParameterManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/" , name="homepage")
     *
     * @param PageManager $pageManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PageManager $pageManager)
    {
        return $this->render('page/layout.html.twig', [
            'page' => $pageManager->getOrCreatePage('what-is-plavimop', 'What is PLaviMop ?'),
        ]);
    }

    /**
     * @Route("/software", name="page_software")
     *
     * @param PageManager      $pageManager
     * @param ParameterManager $parameterManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function software(PageManager $pageManager, ParameterManager $parameterManager)
    {
        return $this->render(
            'home/software.html.twig',
            [
            'page' => $pageManager->getOrCreatePage('software', 'Software'),
                'parameters' => $parameterManager->getParameters(),
        ]);
    }

    /**
     * @Route("/community", name="page_community")
     *
     * @param PageManager $pageManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function community(PageManager $pageManager)
    {
        return $this->render('page/layout.html.twig', [
            'page' => $pageManager->getOrCreatePage('community', 'Community'),
        ]);
    }

    /**
     * @Route("/terms-and-conditions", name="page_terms_and_conditions")
     *
     * @param PageManager $pageManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function termsAndCondition(PageManager $pageManager)
    {
        return $this->render(
            'page/layout.html.twig',
            [
                'page' => $pageManager->getOrCreatePage('terms-and-conditions', 'Terms and conditions'),
            ]
        );
    }

    /**
     * @Route("/legal-notices", name="page_legal_notices")
     *
     * @param PageManager $pageManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function legalNotices(PageManager $pageManager)
    {
        return $this->render(
            'page/layout.html.twig',
            [
                'page' => $pageManager->getOrCreatePage('legal-notices', 'Legal notices'),
            ]
        );
    }
}

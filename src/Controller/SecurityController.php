<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 20/11/2017
 * Time: 12:32.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgottenPasswordType;
use App\Form\LoginType;
use App\Form\ResetPasswordType;
use App\Form\UserRegisterType;
use App\Repository\UserRepository;
use App\Service\SecurityMailer;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     *
     * @param AuthenticationUtils $helper
     *
     * @return Response
     */
    public function login(AuthenticationUtils $helper)
    {
        $form = $this->createForm(
            LoginType::class,
            [
                '_username' => $helper->getLastUsername(),
            ]
        );

        $error = $helper->getLastAuthenticationError();
        if ($error) {
            $form->addError(new FormError($error->getMessage()));
        }

        return $this->render(
            'security/login.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/register", name="security_register")
     *
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return RedirectResponse|Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(UserRegisterType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'flash.security.register_success');

            return $this->redirectToRoute('security_login');
        }

        return $this->render(
            'security/register.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/security/forgotten-password", name="security_forgotten_password")
     *
     * @param Request        $request
     * @param UserRepository $userRepository
     * @param SecurityMailer $securityMailer
     *
     * @return Response
     */
    public function forgottenPassword(
        Request $request,
        UserRepository $userRepository,
        SecurityMailer $securityMailer,
        EntityManagerInterface $entityManager
    ) {
        $form = $this->createForm(ForgottenPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $userRepository->findOneByEmail($form->getData()['email']);
            if ($user) {
                $user->generateResetPassword();
                $securityMailer->forgottenPasswordMail($user);
                $entityManager->flush();
            }
            $this->addFlash('success', 'flash.security.forgotten_password_instruction');
        }

        return $this->render(
            'security/forgotten_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
    /**
     * @Route("/security/reset-password/{resetPassword}", name="security_reset_password")
     *
     * @param Request                      $request
     * @param User                         $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface       $entityManager
     */
    public function resetPassword(
        Request $request,
        User $user,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager
    ) {
        $form = $this->createForm(ResetPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $password = $passwordEncoder->encodePassword($user, $data['plainPassword']);
            $user->setPassword($password);
            $user->eraseCredentials();
            $entityManager->flush();
            $this->addFlash('success', 'flash.security.password_changed');

            return $this->redirectToRoute('security_login');
        }

        return $this->render(
            'security/reset_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}

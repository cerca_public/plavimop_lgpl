<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Form\ArticleType;
use App\Manager\ArticleManager;
use App\Manager\PageManager;
use App\Repository\ArticleCategoryRepository;
use App\Repository\ArticleRepository;
use App\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    /**
     * @Route("/news", name="article_index", defaults={"page": "1"})
     * @Route("/news/{page}", requirements={"page": "[1-9]\d*"}, name="article_index_paginated")
     *
     * @param int                       $page
     * @param Paginator                 $paginator
     * @param PageManager               $pageManager
     * @param ArticleRepository         $articleRepository
     * @param ArticleCategoryRepository $articleCategoryRepository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        $page,
        Paginator $paginator,
        PageManager $pageManager,
        ArticleRepository $articleRepository,
        ArticleCategoryRepository $articleCategoryRepository
    ) {
        $paginator = $paginator->create(
            $articleRepository->findAllQuery(),
            $page,
            Article::PAGINATION_LIST_LIMIT
        );

        $categories = $articleCategoryRepository->findAll();

        return $this->render(
            'article/index.html.twig',
            [
                'articles' => $paginator,
                'categories' => $categories,
                'page' => $pageManager->getOrCreatePage('news', 'News'),
            ]
        );
    }

    /**
     * @Route("/news-category/{slug}", name="article_category_index", defaults={"page": "1"})
     * @Route("/news-category/{slug}/{page}", requirements={"page": "[1-9]\d*"}, name="article_category_index_paginated")
     *
     * @param ArticleCategory   $category
     * @param PageManager       $pageManager
     * @param int               $page
     * @param Paginator         $paginator
     * @param ArticleRepository $articleRepository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexCategory(
        ArticleCategory $category,
        PageManager $pageManager,
        $page,
        Paginator $paginator,
        ArticleRepository $articleRepository
    ) {
        $paginator = $paginator->create(
            $articleRepository->findAllQuery($category),
            $page,
            Article::PAGINATION_LIST_LIMIT
        );

        return $this->render(
            'article/index.html.twig',
            [
                'articles' => $paginator,
                'category' => $category,
                'page' => $pageManager->getOrCreatePage('news', 'News'),
            ]
        );
    }

    /**
     * @param Article $article
     * @Route("/news/{slug}", name="article_show")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Article $article)
    {
        return $this->render(
            'article/show.html.twig',
            [
                'page' => $article,
            ]
        );
    }

    /**
     * @Route("/submit-news", name="article_submit")
     * @Security("has_role('ROLE_CONTRIBUTOR')")
     *
     * @param Request        $request
     * @param PageManager    $pageManager
     * @param ArticleManager $articleManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function submit(Request $request, PageManager $pageManager, ArticleManager $articleManager)
    {
        $form = $this->createForm(ArticleType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->save($form->getData(), $this->getUser());
            $this->addFlash('success', 'flash.article.created');

            return $this->redirectToRoute('article_submit');
        }

        return $this->render(
            'article/submit.html.twig',
            [
                'page' => $pageManager->getOrCreatePage('submit-article', 'Submit a news'),
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("news/{slug}/edit", name="article_edit")
     * @Security("is_granted('edit',article)")
     *
     * @param Request        $request
     * @param Article        $article
     * @param ArticleManager $articleManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function edit(Request $request, Article $article, ArticleManager $articleManager)
    {
        $form = $this->createForm(
            ArticleType::class,
            $article,
            [
                'action' => $request->getRequestUri(),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->save($article);
            $this->addFlash('success', 'flash.article.updated');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'article/edit.html.twig',
            [
                'form' => $form->createView(),
                'article' => $article,
            ]
        );
    }

    /**
     * @Route("news/{slug}/delete", name="article_delete")
     * @Security("is_granted('delete',article)")
     *
     * @param Request        $request
     * @param Article        $article
     * @param ArticleManager $articleManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function delete(Request $request, Article $article, ArticleManager $articleManager)
    {
        $form = $this->createFormBuilder()
            ->setAction($request->getRequestUri())
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->remove($article);
            $this->addFlash('warning', 'flash.article.deleted');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'article/delete.html.twig',
            [
                'form' => $form->createView(),
                'article' => $article,
            ]
        );
    }
}

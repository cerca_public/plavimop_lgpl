<?php

namespace App\Controller;

use App\Entity\AcquisitionProcess;
use App\Form\AcquisitionProcessType;
use App\Manager\AcquisitionProcessManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AcquisitionProcessController extends Controller
{
    /**
     * @Route("acquisition-process/{slug}/edit", name="acquisition_process_edit")
     * @Security("is_granted('edit',acquisitionProcess)")
     *
     * @param Request                   $request
     * @param AcquisitionProcess        $acquisitionProcess
     * @param AcquisitionProcessManager $acquisitionProcessManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function edit(
        Request $request,
        AcquisitionProcess $acquisitionProcess,
        AcquisitionProcessManager $acquisitionProcessManager
    ) {
        $form = $this->createForm(
            AcquisitionProcessType::class,
            $acquisitionProcess,
            [
                'action' => $request->getRequestUri(),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acquisitionProcessManager->save($acquisitionProcess);
            $this->addFlash('success', 'flash.acquisition_process.updated');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'acquisition_process/edit.html.twig',
            [
                'form' => $form->createView(),
                'acquisitionProcess' => $acquisitionProcess,
            ]
        );
    }

    /**
     * @Route("acquisition-process/{slug}/delete", name="acquisition_process_delete")
     * @Security("is_granted('delete',acquisitionProcess)")
     *
     * @param Request                   $request
     * @param AcquisitionProcess        $acquisitionProcess
     * @param AcquisitionProcessManager $acquisitionProcessManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function delete(
        Request $request,
        AcquisitionProcess $acquisitionProcess,
        AcquisitionProcessManager $acquisitionProcessManager
    ) {
        $form = $this->createFormBuilder()
            ->setAction($request->getRequestUri())
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($acquisitionProcessManager->remove($acquisitionProcess)) {
                $this->addFlash('warning', 'flash.acquisition_process.deleted');
            } else {
                $this->addFlash('danger', 'flash.acquisition_process.can_t_be_deleted');
            }

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'acquisition_process/delete.html.twig',
            [
                'form' => $form->createView(),
                'acquisitionProcess' => $acquisitionProcess,
            ]
        );
    }
}

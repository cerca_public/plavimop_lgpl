<?php

namespace App\Controller\Admin;

use App\Entity\EditorUpload;
use App\Form\EditorUploadType;
use App\Repository\EditorUploadRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class EditorController extends AbstractController
{
    /**
     * @Route("/editor/delete/{id}", name="admin_editor_delete")
     *
     * @param EditorUpload           $upload
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function delete(EditorUpload $upload, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($upload);
        $entityManager->flush();

        return new JsonResponse(true, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/editor/browse", name="admin_editor_browse")
     *
     * @param Request                $request
     * @param EditorUploadRepository $uploadRepository
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function browse(
        Request $request,
        EditorUploadRepository $uploadRepository,
        EntityManagerInterface $entityManager
    ) {
        $funcNum = $request->query->get('CKEditorFuncNum');

        $form = $this->createForm(EditorUploadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editorUpload = $form->getData();

            $entityManager->persist($editorUpload);
            $entityManager->flush();

            return $this->redirectToRoute('admin_editor_browse', $request->query->all());
        }

        return $this->render(
            'admin/editor/browse.html.twig',
            [
                'files' => $uploadRepository->findAll(),
                'funcNum' => $funcNum,
                'form' => $form->createView(),
            ]
        );
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 27/08/2018
 * Time: 12:16
 */

namespace App\Controller\Admin;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AcquisitionController extends BaseAdminController
{
    protected function createSearchQueryBuilder(
        $entityClass,
        $searchQuery,
        array $searchableFields,
        $sortField = null,
        $sortDirection = null,
        $dqlFilter = null
    ) {
        /* @var EntityManager $em */
        $em = $this->getDoctrine()->getManagerForClass($this->entity['class']);
        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = $em->createQueryBuilder()
            ->select('entity')
            ->from($this->entity['class'], 'entity')
            ->innerjoin('entity.motion', 'motion')
            ->innerjoin('motion.translations', 'translations')
            ->orWhere('LOWER(translations.title) LIKE :query')
            ->orWhere('entity.id = :id')
            ->setParameter('query', '%'.strtolower($searchQuery).'%')
            ->setParameter('id', $searchQuery);
        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }
        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.'.$sortField, $sortDirection ?: 'DESC');
        }

        return $queryBuilder;
    }
}
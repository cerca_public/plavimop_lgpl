<?php

namespace App\Controller\Admin;

use App\Repository\AcquisitionRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends BaseAdminController
{
    const GENERATOR_EXE = 'createMP4.exe';

    /**
     * @Route("/export-need-preview", name="admin_export_need_preview")
     *
     * @param AcquisitionRepository $acquisitionRepository
     * @param Filesystem $filesystem
     * @return BinaryFileResponse
     */
    public function exportNeedPreview(AcquisitionRepository $acquisitionRepository, Filesystem $filesystem): BinaryFileResponse
    {
        $archivePath = $filesystem->tempnam(sys_get_temp_dir(), 'PLV');
        $batPath = $filesystem->tempnam(sys_get_temp_dir(), 'PLV');

        $archive = new \ZipArchive();
        $archive->open($archivePath, \ZipArchive::CREATE);
        $acquisitions = $acquisitionRepository->getExportNeedPreview();
        $filesystem->appendToFile($batPath, "Echo off\r\n");
        foreach ($acquisitions as $acq) {
            $command = sprintf(
                "%s %s %s %d %d %d %d\r\n",
                self::GENERATOR_EXE,
                $acq->getId().'.'.$acq->getFileFile()->getExtension(),
                $acq->getId().'.mp4',
                $acq->getPreviewDuration(),
                $acq->getPreviewMarkerSize(),
                $acq->getPreviewAzimuth(),
                $acq->getPreviewElevation()
            );
            $filesystem->appendToFile($batPath, $command);
            $archive->addFile(
                $acq->getFileFile()->getRealPath(),
                $acq->getId().'.'.$acq->getFileFile()->getExtension()
            );
        }
        $filesystem->appendToFile($batPath, "pause\r\n");
        $archive->addFile($batPath, 'generateMP4.bat');
        $archive->close();

        $filesystem->remove($batPath);

        $file = new File($archivePath);

        return $this->file($file, time().'_need_preview.zip');
    }

    public function editParameterAction()
    {
        $response = parent::editAction();
        if ($response instanceof RedirectResponse) {
            return $this->redirectToRoute('easyadmin', ['entity' => 'Parameter', 'action' => 'edit', 'id' => 1]);
        }

        return $response;
    }
}

<?php

namespace App\Controller;

use App\Entity\Acquisition;
use App\Form\AcquisitionType;
use App\Manager\AcquisitionManager;
use Gedmo\Sluggable\Util\Urlizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class AcquisitionController extends AbstractController
{
    /**
     * @Route("acquisition/{id}/download", name="acquisition_download")
     * @Security("is_granted('download',acquisition)")
     *
     * @param Acquisition        $acquisition
     * @param AcquisitionManager $acquisitionManager
     *
     * @return BinaryFileResponse
     */
    public function download(Acquisition $acquisition, AcquisitionManager $acquisitionManager)
    {
        return $this->file($acquisitionManager->download($acquisition), $acquisition->getFile()->getOriginalName());
    }

    /**
     * @Route("acquisition-video/{id}/download", name="acquisition_video_download")
     *
     * @param Acquisition $acquisition
     *
     * @return BinaryFileResponse
     *
     * @internal param Acquisition $acquisition
     * @internal param AcquisitionManager $acquisitionManager
     */
    public function downloadVideo(Acquisition $acquisition)
    {
        return $this->file($acquisition->getPreviewFile(), $acquisition->getPreview()->getOriginalName());
    }

    /**
     * @Route("acquisition/{id}/export-recognitions", name="acquisition_export_recognitions")
     * @Entity("acquisition", expr="repository.getAcquisitionExportRecognitions(id)")
     * @Security("is_granted('download_csv',acquisition)")
     *
     * @param Acquisition         $acquisition
     * @param SerializerInterface $serializer
     *
     * @return Response
     */
    public function exportRecognitions(Acquisition $acquisition, SerializerInterface $serializer)
    {
        $csv = $serializer->serialize(
            $acquisition->getExportRecognitionsArray(),
            'csv',
            [CsvEncoder::DELIMITER_KEY => ';']
        );
        $response = new Response($csv);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set(
            'Content-Disposition',
            'attachment; filename="'.Urlizer::urlize($acquisition->getName()).'.csv"'
        );

        return $response;
    }

    /**
     * @Route("acquisition/{id}/edit", name="acquisition_edit")
     * @Security("is_granted('edit',acquisition)")
     *
     * @param Request            $request
     * @param Acquisition        $acquisition
     * @param AcquisitionManager $acquisitionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function edit(Request $request, Acquisition $acquisition, AcquisitionManager $acquisitionManager)
    {
        $form = $this->createForm(
            AcquisitionType::class,
            $acquisition,
            [
                'action' => $request->getRequestUri(),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acquisitionManager->save($acquisition);
            $this->addFlash('success', 'flash.acquisition.updated');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'acquisition/edit.html.twig',
            [
                'form' => $form->createView(),
                'acquisition' => $acquisition,
            ]
        );
    }

    /**
     * @Route("acquisition/{id}/delete", name="acquisition_delete")
     * @Security("is_granted('delete',acquisition)")
     *
     * @param Request            $request
     * @param Acquisition        $acquisition
     * @param AcquisitionManager $acquisitionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @internal param EntityManagerInterface $entityManager
     */
    public function delete(Request $request, Acquisition $acquisition, AcquisitionManager $acquisitionManager)
    {
        $form = $this->createFormBuilder()
            ->setAction($request->getRequestUri())
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $acquisitionManager->remove($acquisition);
            $this->addFlash('warning', 'flash.acquisition.deleted');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'acquisition/delete.html.twig',
            [
                'form' => $form->createView(),
                'acquisition' => $acquisition,
            ]
        );
    }
}

<?php

namespace App\Controller;

use App\Entity\Respondent;
use App\Form\SurveyType;
use App\Manager\PageManager;
use App\Manager\SurveyManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecognitionController extends Controller
{
    /**
     * @Route("/recognition", name="recognition_index")
     *
     * @param SurveyManager $surveyManager
     * @param Request $request
     * @param PageManager $pageManager
     * @return Response
     *
     * @internal param Survey $survey
     */
    public function index(SurveyManager $surveyManager, Request $request, PageManager $pageManager): Response
    {
        $form = $this->createForm(
            SurveyType::class,
            $surveyManager->initSurvey($request->isMethod(Request::METHOD_GET))
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $survey = $surveyManager->saveSurvey($form->getData());
            $this->addFlash('success', 'flash.recognition.results_saved');

            return $this->redirectToRoute('recognition_result', ['id' => $survey->getRespondent()->getId()]);
        }

        return $this->render(
            'recognition/index.html.twig',
            [
                'form' => $form->createView(),
                'page' => $pageManager->getOrCreatePage('recognition', 'Recognition of human movements'),
            ]
        );
    }

    /**
     * @Route("/recognition/result/{id}", name="recognition_result")
     * @Entity("respondent", expr="repository.findForResult(id)")
     *
     * @param Respondent $respondent
     *
     * @return Response
     */
    public function result(Respondent $respondent): Response
    {
        return $this->render(
            'recognition/show_result.html.twig',
            [
                'respondant' => $respondent,
            ]
        );
    }
}

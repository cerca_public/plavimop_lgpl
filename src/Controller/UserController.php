<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use App\Form\UserRequestContributorType;
use App\Form\UserRequestSoftwareType;
use App\Form\UserType;
use App\Manager\UserManager;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController.
 *
 * @Security("has_role('ROLE_USER')")
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/edit-profile", name="user_edit_profile")
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @internal param UserManager $userManager
     */
    public function editProfile(
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        $form = $this->createForm(
            UserType::class,
            $this->getUser(),
            [
                'action' => $request->getRequestUri(),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'flash.user.updated');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'user/edit_profile.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/change-password", name="user_change_password")
     * @Method({"GET", "POST"})
     *
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function changePassword(Request $request, UserManager $userManager)
    {
        $form = $this->createForm(
            ChangePasswordType::class,
            null,
            [
                'action' => $request->getRequestUri(),
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $userManager->changePassword($this->getUser(), $data['plainPassword']);
            $this->addFlash('success', 'flash.user.password_changed');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'user/change_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/send-contributor-request", name="user_send_contributor_request")
     * @Method({"GET","POST"})
     *
     * @Security("not (has_role('ROLE_CONTRIBUTOR'))")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function sendContributorRequest(Request $request, Mailer $mailer)
    {
        $form = $this->createForm(
            UserRequestContributorType::class,
            $this->getUser(),
            [
                'action' => $request->getRequestUri(),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailer->sendContributorRequest($this->getUser());
            $this->addFlash('success', 'flash.user.contributor_request');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'user/send_contributor_request.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/send-download-software-request", name="user_send_download_software_request")
     * @Method({"GET","POST"})
     *
     * @param Request $request
     * @param Mailer  $mailer
     *
     * @return Response
     */
    public function sendDownloadSoftwareRequest(Request $request, Mailer $mailer)
    {
        $form = $this->createForm(
            UserRequestSoftwareType::class,
            $this->getUser(),
            [
                'action' => $request->getRequestUri(),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailer->sendDownloadSoftwareRequest($this->getUser());
            $this->addFlash('success', 'flash.user.software_request');

            return $this->redirectToRoute('user_dashboard');
        }

        return $this->render(
            'user/send_download_software_request.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/dashboard", name="user_dashboard")
     * @Method({"GET"})
     *
     * @param UserRepository $userRepository
     *
     * @return Response
     */
    public function dashboard(UserRepository $userRepository)
    {
        $user = $userRepository->getDashboardInfo($this->getUser()->getId());

        return $this->render(
            'user/dashboard.html.twig',
            [
                'user' => $user,
            ]
        );
    }
}

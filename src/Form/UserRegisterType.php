<?php

namespace App\Form;

use App\Entity\User;
use App\Form\DataTransformer\InstituteToStringTransformer;
use App\Repository\InstituteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class UserRegisterType extends AbstractType
{
    /**
     * @var InstituteToStringTransformer
     */
    private $transformer;
    /**
     * @var InstituteRepository
     */
    private $instituteRepository;

    public function __construct(InstituteToStringTransformer $transformer, InstituteRepository $instituteRepository)
    {
        $this->transformer = $transformer;
        $this->instituteRepository = $instituteRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'country',
                CountryType::class,
                [
                    'label' => 'label.country',
                    'attr' => ['class' => 'select2'],
                ]
            )
            ->add(
                'researchTopic',
                TextareaType::class,
                [
                    'label' => 'label.research_topic',
                    'required' => true,
                ]
            )
            ->add(
                'title',
                ChoiceType::class,
                [
                    'label' => 'label.title',
                    'choices' => array_combine(User::TITLES, User::TITLES),
                    'choice_translation_domain' => false,
                ]
            )
            ->add('firstName', null, ['label' => 'label.first_name'])
            ->add('lastName', null, ['label' => 'label.last_name'])
            ->add('email', EmailType::class, ['label' => 'label.email'])
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'label.username',
                    'help' => 'Used to log in',
                ]
            )
            ->add(
                'institute',
                TextType::class,
                [
                    'label' => 'label.institute',
                    'datalist' => $this->instituteRepository->getInstitutesNamesArray(),
                ]
            )
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'label' => 'label.plain_password',
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'label.password',
                        'help' => User::PASSWORD_MIN_LENGTH.' characters min',
                    ],
                    'second_options' => ['label' => 'label.repeat_password'],
                ]
            )
            ->add(
                'acceptTerms',
                CheckboxType::class,
                [
                    'label' => 'label.accept_terms',
                    'mapped' => false,
                    'required' => true,
                    'constraints' => [
                        new IsTrue(),
                    ],
                ]
            );

        $builder->get('institute')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }
}

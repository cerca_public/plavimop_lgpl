<?php

namespace App\Form;

use App\Entity\Respondent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RespondentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'label' => 'label.gender',
                    'expanded' => true,
                    'choices' => array_combine(Respondent::GENDERS, Respondent::GENDERS),
                    'choice_translation_domain' => false,
                ]
            )
            ->add(
                'age',
                IntegerType::class,
                [
                    'label' => 'label.age',
                    'attr' => [
                        'min' => Respondent::MIN_AGE,
                        'max' => Respondent::MAX_AGE,
                    ],
                ]
            )
            ->add(
                'sensoryMotorExpertise',
                null,
                [
                    'label' => 'label.sensory_motor_expertise',
                    'attr' => [
                        'placeholder' => 'placeholder.sensory_motor_expertise',
                    ],
                ]
            )
            ->add(
                'motorExpertiseLevel',
                ChoiceType::class,
                [
                    'label' => 'label.motor_expertise_level',
                    'required' => true,
                    'expanded' => true,
                    'choices' => array_combine(Respondent::MOTOR_EXPERTISE_LEVELS, Respondent::MOTOR_EXPERTISE_LEVELS),
                    'choice_translation_domain' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Respondent::class,
            ]
        );
    }
}

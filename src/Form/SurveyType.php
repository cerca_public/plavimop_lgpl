<?php

namespace App\Form;

use App\Entity\Survey;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                $form
                    ->add(
                        'respondent',
                        RespondentType::class,
                        [
                            'label' => 'label.respondent',
                        ]
                    )
                    ->add(
                        'recognitions',
                        CollectionType::class,
                        [
                            'label' => 'label.recognitions',
                            'entry_type' => RecognitionType::class,
                            'entry_options' => ['typesSelected' => $event->getData()->getTagTypes()],
                        ]
                    );
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Survey::class,
            ]
        );
    }
}

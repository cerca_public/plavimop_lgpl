<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                '_username',
                TextType::class,
                [
                    'label' => 'label.username',
                    'help' => 'Email or Username',
            ])
            ->add(
                '_password',
                PasswordType::class,
                [
                    'label' => 'label.password',
                ]
            );
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'crsf_protection' => true,
                'csrf_field_name' => '_csrf_token',
                'csrf_token_id' => 'authenticate',
            ]
        );
    }
}

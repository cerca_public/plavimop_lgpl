<?php

namespace App\Form;

use App\Entity\Acquisition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AcquisitionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'acqDate',
                DateType::class,
                [
                    'label' => 'label.acquisition_date',
                    'widget' => 'single_text',
                ]
            )->add(
                'fileFile',
                FileType::class,
                [
                    'label' => false,
                    'attr' => ['style' => 'opacity:1;'],
                    'required' => true,
                ]
            )->add('comment', TextareaType::class, ['label' => 'label.comment', 'required' => false])
            ->add(
                'previewDuration',
                null,
                [
                    'label' => 'label.preview_duration',
                    'help' => 'Between '.Acquisition::PREVIEW_DURATION_MIN.' and '.Acquisition::PREVIEW_DURATION_MAX,
                    'attr' => ['min' => Acquisition::PREVIEW_DURATION_MIN, 'max' => Acquisition::PREVIEW_DURATION_MAX],
                ]
            )
            ->add(
                'previewMarkerSize',
                null,
                [
                    'label' => 'label.preview_marker_size',
                    'help' => 'Between '.Acquisition::PREVIEW_MARKER_SIZE_MIN.' and '.Acquisition::PREVIEW_MARKER_SIZE_MAX,
                    'attr' => [
                        'min' => Acquisition::PREVIEW_MARKER_SIZE_MIN,
                        'max' => Acquisition::PREVIEW_MARKER_SIZE_MAX,
                    ],
                ]
            )
            ->add(
                'previewAzimuth',
                null,
                [
                    'label' => 'label.preview_azimuth',
                    'help' => 'Between '.Acquisition::PREVIEW_AZIMUTH_MIN.' and '.Acquisition::PREVIEW_AZIMUTH_MAX,
                    'attr' => ['min' => Acquisition::PREVIEW_AZIMUTH_MIN, 'max' => Acquisition::PREVIEW_AZIMUTH_MAX],
                ]
            )
            ->add(
                'previewElevation',
                null,
                [
                    'label' => 'label.preview_elevation',
                    'help' => 'Between '.Acquisition::PREVIEW_ELEVATION_MIN.' and '.Acquisition::PREVIEW_ELEVATION_MAX,
                    'attr' => ['min' => Acquisition::PREVIEW_ELEVATION_MIN, 'max' => Acquisition::PREVIEW_ELEVATION_MAX],
                ]
            );

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var Acquisition $acquisition */
                $acquisition = $event->getData();
                $form = $event->getForm();

                if ($acquisition && $acquisition->getId()) {
                    $form->remove('fileFile');
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Acquisition::class,
            ]
        );
    }
}

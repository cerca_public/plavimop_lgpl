<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 05/12/2017
 * Time: 00:59.
 */

namespace App\Form;

use App\Entity\ArticleAttachment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ArticleAttachmentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'fileFile',
            VichFileType::class,
            [
                'required' => true,
                'allow_delete' => false,
                'label' => false,
                'attr' => ['style' => 'opacity:1;'],
                'download_label' => function (ArticleAttachment $aa) {
                    return $aa->getFile()->getOriginalName();
                },
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => ArticleAttachment::class,
            ]
        );
    }
}

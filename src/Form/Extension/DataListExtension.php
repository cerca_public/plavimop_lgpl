<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 10/02/2018
 * Time: 23:42.
 */

namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataListExtension extends AbstractTypeExtension
{
    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['datalist'])) {
            $dataListId = $view->vars['id'].'_datalist';
            $view->vars['attr']['list'] = $dataListId;
            $view->vars['attr']['autocomplete'] = 'off';
            $view->vars['datalist_id'] = $dataListId;
            $view->vars['datalist'] = $options['datalist'];
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['datalist']);
        $resolver->setAllowedTypes('datalist', 'iterable');
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return TextType::class;
    }
}

<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'oldPassword',
                PasswordType::class,
                [
                    'label' => 'label.old_password',
                    'constraints' => [
                        new UserPassword(),
                    ],
                ]
            )
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'label' => 'label.plain_password',
                    'type' => PasswordType::class,
                    'first_name' => 'newPassword',
                    'first_options' => ['label' => 'label.password'],
                    'second_name' => 'repeatedNewPassword',
                    'second_options' => ['label' => 'label.repeat_password'],
                    'constraints' => [
                        new NotBlank(),
                        new Length(
                            [
                                'min' => User::PASSWORD_MIN_LENGTH,
                            ]
                        ),
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 10/02/2018
 * Time: 02:28.
 */

namespace App\Form\DataTransformer;

namespace App\Form\DataTransformer;

use App\Entity\Tag;
use App\Entity\TagType;
use App\Manager\TagManager;
use App\Repository\TagRepository;
use Symfony\Component\Form\DataTransformerInterface;

class TagToStringTransformer implements DataTransformerInterface
{
    /**
     * @var TagManager
     */
    private $tagManager;
    /**
     * @var TagType
     */
    private $type;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    public function __construct(TagRepository $tagRepository, TagManager $tagManager)
    {
        $this->tagManager = $tagManager;
        $this->type = null;
        $this->tagRepository = $tagRepository;
    }

    public function setType(TagType $type)
    {
        $this->type = $type;
    }

    /**
     * Transforms an object (tag) to a string (number).
     *
     * @param Tag|null $institute
     *
     * @return string
     */
    public function transform($institute)
    {
        if (empty($institute)) {
            return '';
        }

        return $institute->getLabel();
    }

    /**
     * Transforms a string (number) to an object (tag).
     *
     * @param mixed $name
     *
     * @return Tag|null
     *
     * @internal param string $tagNumber
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $tag = $this->tagRepository->getOneByLabel($name);

        if (null === $tag) {
            $tag = $this->tagManager->createTag($this->type, $name);
        }

        return $tag;
    }
}

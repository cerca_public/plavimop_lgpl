<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 10/02/2018
 * Time: 02:28.
 */

namespace App\Form\DataTransformer;

namespace App\Form\DataTransformer;

use App\Entity\Institute;
use App\Repository\InstituteRepository;
use Symfony\Component\Form\DataTransformerInterface;

class InstituteToStringTransformer implements DataTransformerInterface
{
    /**
     * @var InstituteRepository
     */
    private $instituteRepository;

    public function __construct(InstituteRepository $instituteRepository)
    {
        $this->instituteRepository = $instituteRepository;
    }

    /**
     * Transforms an object (institute) to a string (number).
     *
     * @param Institute|null $institute
     *
     * @return string
     */
    public function transform($institute)
    {
        if (null === $institute) {
            return '';
        }

        return $institute->getName();
    }

    /**
     * Transforms a string (number) to an object (institute).
     *
     * @param mixed $name
     *
     * @return Institute|null
     *
     * @internal param string $tagNumber
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $institute = $this->instituteRepository->findOneBy(['name' => $name]);

        if (null === $institute) {
            $institute = new Institute();
            $institute->setName($name);
        }

        return $institute;
    }
}

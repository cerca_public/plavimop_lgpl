<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 16/12/2017
 * Time: 01:05.
 */

namespace App\Form\DataTransformer;

use App\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;

class SelectedTagsToTypesTagTransformer implements DataTransformerInterface
{
    private $typesSelected = [];

    public function __construct($types = [])
    {
        foreach ($types as $type) {
            $this->typesSelected[$type->getId()]['type'] = $type;
            $this->typesSelected[$type->getId()]['tagsSelected'] = [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function transform($tags)
    {
        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $tagType = $tag->getType();
            $this->typesSelected[$tagType->getId()]['type'] = $tag->getType();
            $this->typesSelected[$tagType->getId()]['tagsSelected'][] = $tag;
        }

        return ['types' => $this->typesSelected];
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($types)
    {
        $tagsSelected = new ArrayCollection();
        if (!$types) {
            return $tagsSelected;
        }

        foreach ($types['types'] as $type) {
            if ($type['tagsSelected'] instanceof Tag) {
                if (!$tagsSelected->contains($type['tagsSelected'])) {
                    $tagsSelected->add($type['tagsSelected']);
                }
            } else {
                foreach ($type['tagsSelected'] as $tag) {
                    if (!$tagsSelected->contains($tag)) {
                        $tagsSelected->add($tag);
                    }
                }
            }
        }

        return $tagsSelected;
    }
}

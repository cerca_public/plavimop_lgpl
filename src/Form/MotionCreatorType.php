<?php

namespace App\Form;

use App\Form\Type\TagsSelectedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MotionCreatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'tags',
            TagsSelectedType::class,
            [
                'label' => 'label.tags',
                'typesSelected' => $options['typesSelected'],
                'choices_required' => true,
                'choices_multiple' => false,
                'choices_expanded_limit' => 5,
                'choices_create_new' => true,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(
            [
                'typesSelected',
            ]
        );
    }
}

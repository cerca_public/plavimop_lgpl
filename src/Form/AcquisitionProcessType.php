<?php

namespace App\Form;

use App\Entity\AcquisitionProcess;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class AcquisitionProcessType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                null,
                ['help' => 'Acquisition process name (for later retrieve) ', 'label' => 'label.name']
            )
            ->add(
                'frameRate',
                null,
                ['label' => 'label.frame_rate_hz']
            )
            ->add('numberOfMarkers', null, ['label' => 'label.number_of_marker'])
            ->add(
                'captureSystemName',
                TextType::class,
                [
                    'help' => 'Ex : Vico-Nexus',
                    'label' => 'label.motion_capture_system_name',
                    'datalist' => AcquisitionProcess::CAPTURES_SYSTEMS_NAMES,
                ]
            )
            ->add(
                'numberAndTypeOfCameraSensors',
                TextType::class,
                ['help' => 'Ex : 20 MX-T40 cameras', 'label' => 'label.number_and_type_of_camera']
            )
            ->add(
                'documentFile',
                VichFileType::class,
                [
                    'help' => 'A pdf that contains a picture and a table describing the markers set (name and location on the subject)',
// TODO WAITING FOR BOOTSTRAP 4 THEME CORRECT TO REMOVE translator
                    'label' => $this->translator->trans('label.full_description_document'),
                    'allow_delete' => true,
                    'attr' => ['style' => 'opacity:1;'],
                    'required' => false,
                    'download_label' => function (AcquisitionProcess $ap) {
                        return $ap->getDocument()->getOriginalName();
                    },
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AcquisitionProcess::class,
            ]
        );
    }
}

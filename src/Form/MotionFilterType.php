<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 15/11/2017
 * Time: 14:48.
 */

namespace App\Form;

use App\Form\Type\TagsSelectedType;
use App\Model\MotionFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TagCollectionType.
 */
class MotionFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                $motion = $event->getData();
                $form->add(
                    'selectedTags',
                    TagsSelectedType::class,
                    [
                        'label' => 'label.selected_tags',
                        'typesSelected' => $motion->getTypes(),
                        'choices_required' => false,
                        'choices_multiple' => true,
                    ]
                );
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => MotionFilter::class,
            ]
        );
    }
}

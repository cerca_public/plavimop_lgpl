<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'label.title'])
            ->add('description', null, ['label' => 'label.description'])
            ->add('category', null, ['label' => 'label.category'])
            ->add('content', null, ['label' => 'label.content', 'attr' => ['class' => 'editor']])
            ->add(
                'attachments',
                CollectionType::class,
                [
                    'label' => 'label.attachments',
                    'entry_type' => ArticleAttachmentType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'attachment-box'],
                    ],
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Article::class,
            ]
        );
    }
}

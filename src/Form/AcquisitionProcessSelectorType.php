<?php

namespace App\Form;

use App\Entity\AcquisitionProcess;
use App\Entity\User;
use App\Repository\AcquisitionProcessRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AcquisitionProcessSelectorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'acquisitionProcess',
                EntityType::class,
                [
                    'class' => AcquisitionProcess::class,
                    'query_builder' => function (AcquisitionProcessRepository $er) use ($options) {
                        return $er->getByUserQueryBuilder($options['user']);
                    },
                    'label' => false,
                    'attr' => ['class' => 'select2'],
                    'required' => true,
                ]
            )
            ->add('save', SubmitType::class, ['label' => 'label.select_process'])
            ->add('load', SubmitType::class, ['label' => 'label.load_process_for_create_new']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['user']);
        $resolver->setAllowedTypes('user', User::class);
    }
}

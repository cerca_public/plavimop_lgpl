<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'label.username',
                    'disabled' => true,
                ]
            )
            ->add(
                'title',
                ChoiceType::class,
                [
                    'label' => 'label.title',
                    'choices' => array_combine(User::TITLES, User::TITLES),
                    'choice_translation_domain' => false,
                ]
            )
            ->add('firstName', null, ['label' => 'label.first_name'])
            ->add('lastName', null, ['label' => 'label.last_name'])
            ->add('email', null, ['label' => 'label.email'])
            ->add('institute', null, ['label' => 'label.institute'])
            ->add(
                'country',
                CountryType::class,
                [
                    'label' => 'label.country',
                    'attr' => ['class' => 'select2'],
                ]
            )
            ->add(
                'researchTopic',
                TextareaType::class,
                [
                    'label' => 'label.research_topic',
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }
}

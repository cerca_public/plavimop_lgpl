<?php

namespace App\Form;

use App\Entity\Recognition;
use App\Form\Type\TagsSelectedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecognitionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'selectedTags',
            TagsSelectedType::class,
            [
                'label' => 'label.selected_tags',
                'typesSelected' => $options['typesSelected'],
                'choices_expanded_limit' => 5,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Recognition::class,
            ]
        );
        $resolver->setRequired(
            [
                'typesSelected',
            ]
        );
    }
}

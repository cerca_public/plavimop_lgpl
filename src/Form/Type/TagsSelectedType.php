<?php

namespace App\Form\Type;

use App\Form\DataTransformer\SelectedTagsToTypesTagTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 16/12/2017
 * Time: 01:00.
 */
class TagsSelectedType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('types', CollectionType::class, [
            'label' => false,
            'entry_type' => TagsChoiceType::class,
            'translation_domain' => false,
            'entry_options' => [
                'expanded_limit' => $options['choices_expanded_limit'],
                'label' => false,
                'multiple' => $options['choices_multiple'],
                'required' => $options['choices_required'],
                'create_tag' => $options['choices_create_new'],
            ],
        ])->addModelTransformer(new SelectedTagsToTypesTagTransformer($options['typesSelected']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'typesSelected',
        ]);
        $resolver->setDefault('choices_multiple', false);
        $resolver->setDefault('choices_expanded_limit', TagsChoiceType::PERMUTE_TO_MULTIPLE_SELECT);
        $resolver->setDefault('choices_required', true);
        $resolver->setDefault('choices_create_new', false);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 15/11/2017
 * Time: 14:48.
 */

namespace App\Form\Type;

use App\Form\DataTransformer\TagToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TagCollectionType.
 */
class TagInputType extends AbstractType
{
    /**
     * @var TagToStringTransformer
     */
    private $tagToStringTransformer;

    public function __construct(TagToStringTransformer $tagToStringTransformer)
    {
        $this->tagToStringTransformer = $tagToStringTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tagToStringTransformer = clone $this->tagToStringTransformer;
        $tagToStringTransformer->setType($options['type']);
        $builder->addModelTransformer($tagToStringTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['type']);
    }

    public function getParent()
    {
        return TextType::class;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 15/11/2017
 * Time: 14:48.
 */

namespace App\Form\Type;

use App\Entity\TagType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TagCollectionType.
 */
class TagsChoiceType extends AbstractType
{
    const PERMUTE_TO_MULTIPLE_SELECT = 3;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $data = $event->getData();
            /** @var TagType $type */
            $type = $data['type'];
            $form = $event->getForm();

            if (!$options['create_tag'] || $type->isComplete()) {
                $form->add(
                    'tagsSelected',
                    ChoiceType::class,
                    [
                        'translation_domain' => false,
                        'label' => $type->getLabel(),
                        'choices' => $type->getOrderedTags(),
                        'choice_translation_domain' => false,
                        'placeholder' => '---',
                        'choice_label' => 'label',
                        'label_attr' => ['class' => $options['multiple'] ? 'checkbox-custom checkbox-inline' : 'radio-custom radio-inline'],
                        'required' => $options['required'],
                        'multiple' => $options['multiple'],
                        'expanded' => ($type->getTags()->count() > $options['expanded_limit']) ? false : true,
                        'attr' => [
                            'class' => ($type->getTags()->count() > $options['expanded_limit']) ? 'select2' : false,
                        ],
                    ]
                );
            } else {
                $form->add(
                    'tagsSelected',
                    TagInputType::class,
                    [
                        'label' => $type->getLabel(),
                        'translation_domain' => false,
                        'type' => $type,
                        'datalist' => $type->getOrderedTags(),
                    ]
                );
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'create_tag' => false,
            'required' => true,
            'multiple' => false,
            'expanded_limit' => self::PERMUTE_TO_MULTIPLE_SELECT,
        ]);
    }
}

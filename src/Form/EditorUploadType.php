<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 05/12/2017
 * Time: 00:59.
 */

namespace App\Form;

use App\Entity\EditorUpload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditorUploadType extends AbstractType
{
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'fileFile',
            VichImageType::class,
            [
                'label' => 'label.file',
                'required' => true,
                'attr' => ['style' => 'opacity:1;'],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EditorUpload::class,
        ]);
    }
}

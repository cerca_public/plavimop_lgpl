<?php

namespace App\Command;

use App\Entity\Acquisition;
use App\Repository\AcquisitionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AppPreviewUpdateCommand extends Command
{
    const FILENAME_PATTERN = '/\d+\.mp4$/i';
    protected static $defaultName = 'app:preview:update';
    /**
     * @var null|string
     */
    private $appDirAcquisitionPreview;
    /**
     * @var AcquisitionRepository
     */
    private $acquisitionRepository;

    /** @var SymfonyStyle */
    private $io;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        AcquisitionRepository $acquisitionRepository,
        $appDirAcquisitionPreview
    ) {
        parent::__construct();
        $this->appDirAcquisitionPreview = $appDirAcquisitionPreview;
        $this->acquisitionRepository = $acquisitionRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setName(self::$defaultName)
            ->setDescription('Update preview from mp4 in data/updatePreview folder');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $this->io->title('Reading update preview folder');

        $finder = new Finder();
        $finder->files()
            ->name(self::FILENAME_PATTERN)
            ->in($this->appDirAcquisitionPreview);

        $this->io->progressStart($finder->count());

        foreach ($finder as $file) {
            $acquisitionId = str_replace('.'.$file->getExtension(), '', $file->getFilename());
            /** @var Acquisition $acquisition */
            $acquisition = $this->acquisitionRepository->find($acquisitionId);
            if ($acquisition) {
                $preview = new UploadedFile(
                    $file->getRealPath(),
                    Urlizer::urlize($acquisition->getName().'.mp4'),
                    null,
                    null,
                    null,
                    true
                );
                $acquisition->setPreviewFile($preview);
                $acquisition->setNeedNewPreviewGeneration(false);
                $this->entityManager->flush();
            }

            $this->io->progressAdvance();
        }
        $this->io->progressFinish();
    }
}

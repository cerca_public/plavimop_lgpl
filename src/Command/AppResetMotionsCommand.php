<?php

namespace App\Command;

use App\Repository\AcquisitionRepository;
use App\Repository\MotionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppResetMotionsCommand extends Command
{
    protected static $defaultName = 'app:reset:motions';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MotionRepository
     */
    private $motionRepository;
    /**
     * @var AcquisitionRepository
     */
    private $acquisitionRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        MotionRepository $motionRepository,
        AcquisitionRepository $acquisitionRepository
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->motionRepository = $motionRepository;
        $this->acquisitionRepository = $acquisitionRepository;
    }

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Remove all motions form database')
            ->addOption('force', null, InputOption::VALUE_NONE, 'Confirmation for process');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('force')) {
            $acquisitions = $this->acquisitionRepository->findAll();
            foreach ($acquisitions as $acquisition) {
                $this->entityManager->remove($acquisition);
            }
            $this->entityManager->flush();
            $io->success('Remove all acquisitions form database');

            $motions = $this->motionRepository->findAll();
            foreach ($motions as $motion) {
                $this->entityManager->remove($motion);
            }
            $this->entityManager->flush();

            $io->success('Remove all motions form database');
        } else {
            $io->error('No remove : please add --force option');
        }
    }
}

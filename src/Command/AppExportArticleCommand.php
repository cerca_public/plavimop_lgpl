<?php

namespace App\Command;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AppExportArticleCommand.
 */
class AppExportArticleCommand extends Command
{
    protected static $defaultName = 'app:export:article';

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $appDirData;

    /** @var ArticleRepository */
    private $articleRepository;

    /**
     * @param ArticleRepository   $articleRepository
     * @param SerializerInterface $serializer
     * @param string              $appDirData directory that contain initialization data
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(ArticleRepository $articleRepository, SerializerInterface $serializer, $appDirData)
    {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName(self::$defaultName)
            ->setDescription('Export Article to yaml file');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Article[] $articles */
        $articles = $this->articleRepository->findAll();

        $this->io->title('Export '.count($articles).' articles from database');
        $yaml = ['Articles' => []];
        foreach ($articles as $article) {
            $array = $this->serializer->normalize($article, null, ['attributes' => Article::APP_EXPORT_ATTRIBUTES]);
            $yaml['Articles'][$article->getSlug()] = $array;
        }

        $fs = new Filesystem();
        $filename = $this->appDirData.'Articles.yaml';
        $fs->dumpFile(
            $filename,
            $this->serializer->encode($yaml, 'yaml', ['yaml_inline' => 3, 'yaml_indent' => 0, 'yaml_flags' => 0])
        );
        $this->io->text('Dump '.$filename);

        $this->io->success('Export complete !');
    }
}

<?php

namespace App\Command;

use App\Entity\AcquisitionProcess;
use App\Entity\Institute;
use App\Entity\Tag;
use App\Entity\TagType;
use App\Entity\User;
use App\Repository\TagRepository;
use App\Repository\TagTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AppInitCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $passwordEncoder;

    private $appDirData;
    /**
     * @var TagTypeRepository
     */
    private $tagTypeRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @param EntityManagerInterface       $em
     * @param SerializerInterface          $serializer
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TagTypeRepository            $tagTypeRepository
     * @param TagRepository                $tagRepository
     * @param                              $appDirData        string Directory that contain initialization data
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        UserPasswordEncoderInterface $passwordEncoder,
        TagTypeRepository $tagTypeRepository,
        TagRepository $tagRepository,
        $appDirData
    ) {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->tagTypeRepository = $tagTypeRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:init')
            ->setDescription('Load Tag & TagType in database');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadTags();

        $this->loadUser();

        $this->loadPage($output);

        $this->loadArticle($output);

        $this->io->title('Database flush ');
        $this->em->flush();
        $this->io->success('Ok');

        $this->loadDefaultSurvey($output);

        $this->io->success('Initialization complete !');
    }

    protected function loadTags()
    {
        $this->io->title('Create Tags ');
        $yml = $this->serializer->decode(file_get_contents($this->appDirData.'Tags.yaml'), 'yaml');

        foreach ($yml['TagType'] as $labelType => $typeData) {
            $type = $this->tagTypeRepository->findOneByLabel($labelType) ?? new TagType();
            $type->translate('en')->setLabel($labelType);
            $type->translate('fr')->setLabel($typeData['fr']);
            $type->mergeNewTranslations();
            $type->setIsComplete($typeData['complete']);
            foreach ($typeData['tags'] as $label) {
                if (!$this->tagRepository->getOneByLabel($label[0])) {
                    $tag = new Tag();
                    $tag->setType($type);
                    $tag->translate('en')->setLabel($label[0]);
                    $tag->translate('fr')->setLabel($label[1]);
                    $tag->mergeNewTranslations();
                    $this->em->persist($tag);
                }
            }
        }
        $this->io->success('Ok');
    }

    protected function loadUser()
    {
        $this->io->title('Create users');
        $this->io->text('Create admin user');

        $institute = $this->em->getRepository(Institute::class)->findOneBy(
                ['name' => 'P\' Institute']
            ) ?? new Institute();
        $institute->setName('P\' Institute');

        $admin = $this->em->getRepository(User::class)->findOneBy(['username' => 'admin']) ?? new User();
        $admin->setUsername('admin');
        $admin->setEmail('sebastien.contact@gmail.com');
        $admin->setTitle('Mrs');
        $admin->setFirstName('Sebastien');
        $admin->setLastName('Desbouchages');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setCountry('FR');
        $encodedPassword = $this->passwordEncoder->encodePassword($admin, 'admin');
        $admin->setPassword($encodedPassword);
        $this->em->persist($admin);

        $this->io->text('Create P\' Institute');
        $pprime = $this->em->getRepository(User::class)->findOneBy(['username' => 'pprime']) ?? new User();
        $pprime->setInstitute($institute);
        $pprime->setUsername('pprime');
        $pprime->setCountry('FR');
        $pprime->setEmail('arnaud.decatoire@univ-poitiers.fr ');
        $pprime->setTitle('Dr');
        $pprime->setFirstName('Arnaud');
        $pprime->setLastName('Decatoire');
        $pprime->setRoles(['ROLE_CONTRIBUTOR']);
        $encodedPassword = $this->passwordEncoder->encodePassword($pprime, 'pprime');
        $pprime->setPassword($encodedPassword);
        $this->em->persist($pprime);

        $acquisitionProcess = new AcquisitionProcess();
        $acquisitionProcess->setName('Acq 2016 Process');
        $acquisitionProcess->setFrameRate(100);
        $acquisitionProcess->setNumberOfMarkers(19);
        $acquisitionProcess->setCaptureSystemName('Vico-Nexus');
        $acquisitionProcess->setNumberAndTypeOfCameraSensors('20 MX-T40 cameras');

        $pprime->addAcquisitionProces($acquisitionProcess);

        $this->io->success('Users loaded !');
    }

    /**
     * @param OutputInterface $output
     */
    protected function loadPage(OutputInterface $output)
    {
        $command = $this->getApplication()->find('app:import:page');
        $arguments = [
            'command' => 'app:import:page',
        ];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function loadArticle(OutputInterface $output)
    {
        $command = $this->getApplication()->find('app:import:article');
        $arguments = [
            'command' => 'app:import:article',
        ];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function loadDefaultSurvey(OutputInterface $output)
    {
        $command = $this->getApplication()->find('app:import:defaultSurvey');
        $arguments = [
            'command' => 'app:import:defaultSurvey',
        ];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }
}

<?php

namespace App\Command;

use App\Entity\Acquisition;
use App\Entity\Motion;
use App\Entity\Tag;
use App\Entity\TagType;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AppCsvImportCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var string
     */
    private $appDirAcquisition;
    /**
     * @var
     */
    private $kernelCacheDir;

    /**
     * @var User[]
     */
    private $users;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Tag[]
     */
    private $tags;
    /**
     * @var TagType[]
     */
    private $tagsType;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * AppCsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     * @param UserRepository         $userRepository
     * @param Filesystem             $filesystem
     * @param                        $appDirAcquisition Acquisition directory define in services.yml
     * @param string                 $kernelCacheDir
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        UserRepository $userRepository,
        Filesystem $filesystem,
        $appDirAcquisition,
        $kernelCacheDir
    ) {
        parent::__construct();
        $this->serializer = $serializer;
        $this->em = $em;
        $this->appDirAcquisition = $appDirAcquisition;
        $this->kernelCacheDir = $kernelCacheDir;
        $this->users = [];
        $this->userRepository = $userRepository;
        $this->filesystem = $filesystem;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:csv:import')
            ->setDescription('Imports motions from folder in data/acquisition.')
            ->addArgument(
                'InputFolder',
                InputArgument::REQUIRED,
                'the path to the folder containing one csv, many c3d and many mp4'
            );
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->tags = $this->em->getRepository(Tag::class)->loadAll();
        $this->tagsType = $this->em->getRepository(TagType::class)->findAll();

        $this->io = new SymfonyStyle($input, $output);

        $datas = $this->loadCSVData($input->getArgument('InputFolder'));

        $this->io->progressStart(count($datas));

        foreach ($datas as $row) {
            if ($this->checkFilesExist($row, $input)) {
                $motion = $this->createOrGetMotion($row);

                $acquisition = $this->createAcquisition($row, $input);

                $motion->addAcquisition($acquisition);

                $this->em->persist($motion);
                $this->em->flush();
            }
            $this->io->progressAdvance();
        }
        $this->io->progressFinish();
        $this->io->success('import motions from CSV exited cleanly !');
    }

    private function loadCSVData($path)
    {
        $path = trim($path, '/');
        $this->io->title('Attempting import motions from folder');

        $completePath = $this->appDirAcquisition.'/'.$path.'/*.csv';

        $listCsv = glob($completePath);
        if (1 === count($listCsv)) {
            $this->io->success('Path CSV finded '.$listCsv[0]);

            return $this->serializer->decode(
                file_get_contents($listCsv[0]),
                'csv',
                [CsvEncoder::DELIMITER_KEY => ';']
            );
        }
        $this->io->error(
            count($listCsv).' file(s) found ! One and only one CSV File must be present in the directory '.$path
        );
        throw new FileNotFoundException('File not found exception : '.$completePath);
    }

    private function checkFilesExist($row, InputInterface $input)
    {
        $completePath = $this->appDirAcquisition.$input->getArgument('InputFolder').'/';

        if ($this->filesystem->exists(
            [
                $completePath.$row['filec3d'],
                $completePath.$row['filemp4'],
            ]
        )) {
            return true;
        }
        $this->io->error($row['Title'].' c3d or mp4 files not found in directory');

        return false;
    }

    /**
     * @param $row
     *
     * @return Motion
     */
    protected function createorGetMotion($row)
    {
        $motionTags = [];

        foreach ($this->tagsType as $tagType) {
            $motionTags[] = $this->createOrCreateTag($tagType, $row[$tagType->getLabel()]);
        }
        if ($motion = $this->em->getRepository(Motion::class)->getMotionWithTags($motionTags)) {
            return $motion;
        }
        $motion = new Motion();
        preg_match("#(?<title>(\D)*)#", $row['Title'], $matches);
        $motion->setTitle($matches['title']);
        $motion->setTags($motionTags);

        return $motion;
    }

    protected function createOrCreateTag(TagType $tagType, $tagLabel)
    {
        if (isset($this->tags[$tagType->getLabel().'-'.$tagLabel])) {
            return $this->tags[$tagType->getLabel().'-'.$tagLabel];
        }
        $tag = new Tag();
        $tag->setType($tagType);
        $tag->setLabel($tagLabel);
        $this->io->caution('New tag created : '.$tag->getLabel().' for TagType : '.$tagType->getLabel());
        $this->tags[$tagType->getLabel().'-'.$tagLabel] = $tag;

        $this->em->persist($tag);
        $this->em->flush();

        return $tag;
    }

    /**
     * @param InputInterface $input
     * @param array          $row
     *
     * @return Acquisition
     */
    protected function createAcquisition($row, InputInterface $input)
    {
        $completePath = $this->appDirAcquisition.$input->getArgument('InputFolder').'/';

        $user = $this->getUser($row['User']);

        $acquisition = new Acquisition();
        $acquisition->setUser($user);
        $acquisition->setAcquisitionProcess($user->getAcquisitionProcess()->last());
        $acquisition->setState(Acquisition::STATE_IS_VALID);
        if (preg_match("#(?<year>\d{4})(?<month>\d{2})(?<day>\d{2})#", $row['DateAcquisition'], $matches)) {
            $acquisition->setAcqDate(new \DateTime($matches['year'].'-'.$matches['month'].'-'.$matches['day']));
        }
        $acquisition->setComment($row['Comments']);

        if (preg_match(
            "#az(?<azimuth>\d+)_el(?<elevation>\d+)_Duration(?<duration>\d+)_MarkersSize(?<markersSize>\d+)#",
            $row['filemp4'],
            $matches
        )) {
            $acquisition->setPreviewElevation($matches['elevation']);
            $acquisition->setPreviewAzimuth($matches['azimuth']);
            $acquisition->setPreviewDuration($matches['duration']);
            $acquisition->setPreviewMarkerSize($matches['markersSize']);
        }
        $acquisition->setFileFile(
            $this->copyFileBeforeUpload($completePath, $row['filec3d'])
        );
        $acquisition->setPreviewFile(
            $this->copyFileBeforeUpload($completePath, $row['filemp4'])
        );

        return $acquisition;
    }

    private function getUser($username)
    {
        if (!isset($this->users[$username])) {
            $this->users[$username] = $this->userRepository->loadUserByUsername($username);
        }

        return $this->users[$username];
    }

    private function copyFileBeforeUpload(
        $path,
        $filename
    ) {
        $fileCompletePath = $path.$filename;
        $tmpCompletePath = $this->kernelCacheDir.'/'.$filename;
        $this->filesystem->copy($fileCompletePath, $tmpCompletePath);

        return new UploadedFile($tmpCompletePath, $filename, null, null, null, true);
    }
}

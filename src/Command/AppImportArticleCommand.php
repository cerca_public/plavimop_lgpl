<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\ArticleAttachment;
use App\Entity\ArticleCategory;
use App\Repository\ArticleCategoryRepository;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AppImportArticleCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $appDirData;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ArticleCategoryRepository
     */
    private $articleCategoryRepository;

    /**
     * @param EntityManagerInterface    $em
     * @param SerializerInterface       $serializer
     * @param string                    $appDirData Directory that contain initialization data
     * @param ArticleRepository         $articleRepository
     * @param UserRepository            $userRepository
     * @param ArticleCategoryRepository $articleCategoryRepository
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        $appDirData,
        ArticleRepository $articleRepository,
        UserRepository $userRepository,
        ArticleCategoryRepository $articleCategoryRepository
    ) {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->em = $em;
        $this->articleRepository = $articleRepository;
        $this->userRepository = $userRepository;
        $this->articleCategoryRepository = $articleCategoryRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:import:article')
            ->setDescription('Import Article form Articles.yaml to database');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create Article ');
        $yml = $this->serializer->decode(
            file_get_contents($this->appDirData.'Articles.yaml'),
            'yaml'
        );
        /** @var array $articleData */
        foreach ($yml['Articles'] as $articleData) {
            if (0 && $articleData['slug'] && $this->articleRepository->findOneBy(['slug' => $articleData['slug']])) {
                $this->io->comment($articleData['slug'].' already exist in database');
            } else {
                $article = new Article();
                $article->setTitle($articleData['title']);
                $article->setSlug($articleData['slug']);
                $article->setContent($articleData['content']);
                $article->setDescription($articleData['description']);
                $article->setDate(new \DateTime($articleData['date']));
                $article->setValid($articleData['valid']);

                $category = $this->articleCategoryRepository->findOneBy(['slug' => $articleData['category']['slug']]);
                if ($category) {
                    $category = new ArticleCategory();
                    $category->setName($articleData['category']['slug']);
                }
                $user = $this->userRepository->findOneBy(['username' => $articleData['user']['username']]);
                $article->setCategory($category);
                $article->setUser($user);

                foreach ($articleData['attachments'] as $attachmentData) {
                    $attachment = new ArticleAttachment();
                    $attachment->getFile()->setName($attachmentData['file']['name']);
                    $attachment->getFile()->setOriginalName($attachmentData['file']['originalName']);
                    $attachment->getFile()->setMimeType($attachmentData['file']['mimeType']);
                    $attachment->getFile()->setSize($attachmentData['file']['size']);
                    $attachment->getFile()->setDimensions($attachmentData['file']['dimensions']);
                    $attachment->setUpdatedAt(new \DateTime($attachmentData['updatedAt']));
                    $article->addAttachment($attachment);
                }
                $this->em->persist($article);
            }
        }
        $this->em->flush();
        $this->io->success('Ok');
    }
}

<?php

namespace App\Command;

use App\Entity\Survey;
use App\Repository\SurveyRepository;
use App\Repository\TagTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppImportDefaultSurveyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var SurveyRepository
     */
    private $surveyRepository;
    /**
     * @var TagTypeRepository
     */
    private $tagTypeRepository;

    /**
     * @param EntityManagerInterface $em
     * @param SurveyRepository       $surveyRepository
     * @param TagTypeRepository      $tagTypeRepository
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(
        EntityManagerInterface $em,
        SurveyRepository $surveyRepository,
        TagTypeRepository $tagTypeRepository
    ) {
        parent::__construct();
        $this->surveyRepository = $surveyRepository;
        $this->em = $em;
        $this->tagTypeRepository = $tagTypeRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:import:defaultSurvey')
            ->setDescription('Import Default survey if no existing');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadDefaultSurvey();

        $this->em->flush();
    }

    /**
     * Load default survey in database if no exist.
     */
    protected function loadDefaultSurvey()
    {
        $this->io->title('Load Default survey');
        $survey = $this->surveyRepository->getCurrentSurvey();

        if (!$survey) {
            $survey = new Survey();
            $survey->setNbRecognitions(5);
            $survey->setTagTypes(
                $this->tagTypeRepository->findByLabels(['Participant', 'Action', 'Object'])
            );
            $this->em->persist($survey);
            $this->io->success('Default survey loaded.');
        } else {
            $this->io->success('Default survey already exist.');
        }
    }
}

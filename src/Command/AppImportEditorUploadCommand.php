<?php

namespace App\Command;

use App\Entity\EditorUpload;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class AppImportEditorUploadCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $appDirData;

    /**
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     * @param string                 $appDirData Directory that contain initialization data
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer, $appDirData)
    {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:import:editorUpload')
            ->setDescription('Import EditorUpload form EditorUploads.yaml to database');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Create EditorUpload ');
        $yml = $this->serializer->decode(file_get_contents($this->appDirData.'EditorUploads.yaml'), 'yaml');
        /** @var array $editorUploadData */
        foreach ($yml['EditorUploads'] as $editorUploadData) {
            $editorUpload = $this->em->getRepository(EditorUpload::class)->findOneBy(
                    ['file.name' => $editorUploadData['name']]
                ) ?? new EditorUpload();

            $editorUpload->getFile()->setName($editorUploadData['name']);
            $editorUpload->getFile()->setOriginalName($editorUploadData['originalName']);
            $editorUpload->getFile()->setMimeType($editorUploadData['mimeType']);
            $editorUpload->getFile()->setDimensions($editorUploadData['dimensions']);
            $editorUpload->getFile()->setSize($editorUploadData['size']);
            $this->em->persist($editorUpload);
        }
        $this->em->flush();
        $this->io->success('Ok');
    }
}

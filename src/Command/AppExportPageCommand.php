<?php

namespace App\Command;

use App\Entity\Page;
use App\Repository\PageRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AppExportPageCommand.
 */
class AppExportPageCommand extends Command
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $appDirData;

    /** @var PageRepository */
    private $pageRepository;

    /**
     * @param PageRepository      $pageRepository
     * @param SerializerInterface $serializer
     * @param string              $appDirData directory that contain initialization data
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(PageRepository $pageRepository, SerializerInterface $serializer, $appDirData)
    {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->pageRepository = $pageRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:export:page')
            ->setDescription('Export Page to yaml file');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Page[] $pages */
        $pages = $this->pageRepository->findAll();

        $this->io->title('Export '.count($pages).' pages from database');
        $yaml = ['Pages' => []];
        foreach ($pages as $page) {
            $yaml['Pages'][$page->getSlug()] = [
                'title' => $page->getTitle(),
                'slug' => $page->getSlug(),
                'description' => $page->getDescription(),
                'content' => $page->getContent(),
            ];
        }

        $fs = new Filesystem();
        $filename = $this->appDirData.'Pages.yaml';
        $fs->dumpFile(
            $filename,
            $this->serializer->encode($yaml, 'yaml', ['yaml_inline' => 3, 'yaml_indent' => 0, 'yaml_flags' => 0])
        );
        $this->io->text('Dump '.$filename);

        $this->io->success('Export complete !');

        $command = $this->getApplication()->find('app:export:editorUpload');
        $arguments = [
            'command' => 'app:export:editorUpload',
        ];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }
}

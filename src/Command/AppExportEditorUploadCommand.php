<?php

namespace App\Command;

use App\Entity\EditorUpload;
use App\Repository\EditorUploadRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AppExportEditorUploadCommand.
 */
class AppExportEditorUploadCommand extends Command
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $appDirData;

    /** @var EditorUploadRepository */
    private $editorUploadRepository;

    /**
     * @param EditorUploadRepository $editorUploadRepository
     * @param SerializerInterface    $serializer
     * @param string                 $appDirData directory that contain initialization data
     *
     * @internal param SymfonyStyle $io
     */
    public function __construct(
        EditorUploadRepository $editorUploadRepository,
        SerializerInterface $serializer,
        $appDirData
    ) {
        parent::__construct();
        $this->appDirData = $appDirData;
        $this->serializer = $serializer;
        $this->editorUploadRepository = $editorUploadRepository;
    }

    /**
     * Configure.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('app:export:editorUpload')
            ->setDescription('Export editorUpload to yaml file');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface         $input
     * @param Output|OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EditorUpload[] $editorUploads */
        $editorUploads = $this->editorUploadRepository->findAll();

        $this->io->title('Export '.count($editorUploads).' editorUploads from database');
        $yaml = ['EditorUploads' => []];
        foreach ($editorUploads as $editorUpload) {
            $yaml['EditorUploads'][$editorUpload->getFile()->getName()] = [
                'name' => $editorUpload->getFile()->getName(),
                'originalName' => $editorUpload->getFile()->getOriginalName(),
                'mimeType' => $editorUpload->getFile()->getMimeType(),
                'dimensions' => $editorUpload->getFile()->getDimensions(),
                'size' => $editorUpload->getFile()->getSize(),
            ];
        }

        $fs = new Filesystem();
        $filename = $this->appDirData.'EditorUploads.yaml';
        $fs->dumpFile(
            $filename,
            $this->serializer->encode($yaml, 'yaml', ['yaml_inline' => 3, 'yaml_indent' => 0, 'yaml_flags' => 0])
        );
        $this->io->text('Dump '.$filename);

        $this->io->success('Export complete !');
    }
}

import icons from "trumbowyg/dist/ui/icons.svg"
import "trumbowyg/dist/ui/trumbowyg.min.css"
import "trumbowyg/dist/trumbowyg.min.js"

$.trumbowyg.svgPath = icons;
$('.editor').trumbowyg();
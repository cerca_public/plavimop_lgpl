$(function() {
    $('.js-change-preview').on('mouseenter',function(){
        let video = document.querySelector('video[data-preview-id="'+this.dataset.previewId +'"]');
        let source = video.getElementsByTagName('source')[0];
        if (source.src !== this.href) {
            video.pause();
            source.setAttribute('src',this.href );
            video.load();
            video.play();
        }
    });
});
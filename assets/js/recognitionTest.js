$(function () {
    let currentStep = 0;
    const nextBtn = document.getElementById("nextBtn");
    nextBtn.addEventListener('click', () => {
        nextPrev(1)
    });
    const submitBtn = document.getElementById("submitBtn");
    submitBtn.style.display = "none";

    function showStep(n) {
        const x = document.getElementsByClassName("recognitionTest__Step");
        x[n].style.display = "block";
        if (n === (x.length - 1)) {
            nextBtn.style.display = "none";
            submitBtn.style.display = "inline";
        } else {
            nextBtn.style.display = "inline";
            submitBtn.style.display = "none";
        }
    }

    let checkIValidity = (elem) => {
        let valid = true;
        if (!elem.checkValidity()) {
            $(elem).parents('.form-group:first').addClass('text-danger');
            valid = false;
        } else {
            $(elem).parents('.form-group:first').removeClass('text-danger');
        }
        return valid;
    };

    $('#regForm').find('select').on('change', function () {
        checkIValidity(this)
    });

    $("#survey_respondent_motorExpertiseLevel input").removeAttr("required");
    $("#survey_respondent_motorExpertiseLevel").parent().hide();
    document.querySelector("#survey_respondent_sensoryMotorExpertise").addEventListener("keyup", function (e) {
        if (e.target.checkValidity() && e.target.value !== "") {
            $("#survey_respondent_motorExpertiseLevel").parent().slideDown();
            $("#survey_respondent_motorExpertiseLevel input").attr("required", "");
        } else {
            $("#survey_respondent_motorExpertiseLevel").parent().slideUp();
            $("#survey_respondent_motorExpertiseLevel input").removeAttr("required");
        }
    });


    showStep(currentStep);

    function nextPrev(n) {
        const x = document.getElementsByClassName("recognitionTest__Step");
        if (n === 1 && !validateForm()) return false;
        x[currentStep].style.display = "none";
        currentStep = currentStep + n;
        if (currentStep >= x.length) {
            document.getElementById("regForm").submit();
            return false;
        }
        showStep(currentStep);
    }

    function validateForm() {
        let x, y, i, valid = true;

        x = document.getElementsByClassName("recognitionTest__Step");
        y = x[currentStep].getElementsByTagName("input");
        for (i = 0; i < y.length; i++) {
            valid = checkIValidity(y[i]) && valid;
        }
        y = x[currentStep].getElementsByTagName("select");
        for (i = 0; i < y.length; i++) {
            valid = checkIValidity(y[i]) && valid;
        }
        return valid;
    }
});
import 'bootstrap';
import './select2.min.js';

function matchCustom(params, data) {
    if ($.trim(params.term) === '') {
        return data;
    }
    if (typeof data.text === 'undefined') {
        return null;
    }
    let words = params.term.toUpperCase().split(" ");

    for (let i = 0; i < words.length; i++) {
        if (data.text.toUpperCase().indexOf(words[i]) < 0) {
            return null;
        }
    }
    return data;
}

$(function () {
    $('select.select2').select2({
        width: '100%',
        matcher: matchCustom
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('video').on('mouseenter', event => {
        $('video').each((index, video) => {
            video.pause()
        });
            event.target.play();
        }
    );
    $('video').on('click', event => {
            if (event.target.paused) {
                event.target.play();
            } else {
                event.target.pause();
            }
        }
    );


    $('#plavimopModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let modal = $(this);
        modal.find('.modal-title').text(modal.data('defaultTitle'));
        modal.find('.modal-footer').html(modal.data('defaultFooter'));
        modal.find('.modal-body').html(modal.data('defaultContent'));

        $.ajax(button.data("remote"))
            .done(function (data) {
                if (button.data("autoload")) {
                    modal.find('.modal-title').text($(data).find('title').text());
                    modal.find('.modal-body').html($(data).find('.plavimopBody'));
                } else {
                    modal.html(data);
                    if ($.trumbowyg !== undefined) {
                        $('.editor').trumbowyg();
                    }
                }
            })
            .fail(function (data) {
                modal.find('.modal-title').text('Oooops !');
                modal.find('.modal-body').html('An error occurred, please contact the administrator');
            });
    })
});


function getUrlParam(paramName) {
    let reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
    let match = window.location.search.match(reParam);
    return ( match && match.length > 1 ) ? match[1] : null;
}

$(function () {

    $(".editorMediaFiles img").on('click', function () {
        let alt = $(this).attr('alt');
        window.opener.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), $(this).attr('src'), function () {
            let dialog = this.getDialog();
            if (dialog.getName() === 'image') {
                dialog.setValueOf('info', 'txtAlt', alt);
            }
        });
        window.close();
    });


    $("a[data-action='delete']").on('click', function () {
        if (confirm("Delete this file ?")) {
            let url = $(this).data('url');
            let uploadId = $(this).data("deleteId");
            $.get(url).done(() => $("#" + uploadId).remove());
        }
    })
});
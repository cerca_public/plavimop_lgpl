$(function () {
    let textareas = document.querySelectorAll('.editable_content textarea');
    if (textareas.length) {
        CKEDITOR.config.contentsCss = ['/build/css/app.css'];
        CKEDITOR.config.extraAllowedContent = '*(*);i;span;';
        CKEDITOR.dtd.$removeEmpty['i'] = false;
        CKEDITOR.dtd.$removeEmpty['em'] = false;
        CKEDITOR.config.filebrowserBrowseUrl = '/en/admin/editor/browse';
    }

    for (let i = 0; i < textareas.length; ++i) {
        CKEDITOR.replace(textareas[i], {
            on: {
                insertElement: function (event) {
                    let element = event.data;
                    if (element.getName() === 'img') {
                        element.addClass('img-fluid');
                    }
                }
            }
        });
    }
});

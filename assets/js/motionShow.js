$(function () {
    const divs = document.getElementsByClassName('motionShow__recognitionChart');
    for (let i = 0; i < divs.length; ++i) {
        let plotData = JSON.parse(divs[i].dataset.plotData);
        let plotTitle = JSON.parse(divs[i].dataset.plotTitle);
        let data = [];
        let chartVals = {values: [], labels: [], type: 'pie'};
        for (let [label, count] of Object.entries(plotData)) {
            chartVals.labels.push(label);
            chartVals.values.push(count);
        }
        data.push(chartVals);
        Plotly.newPlot(divs[i], data, {title: plotTitle, margin: {l: 0, r: 0, b: 0}});
    }
    window.onresize = function () {
        for (let i = 0; i < divs.length; ++i) {
            Plotly.Plots.resize(divs[i]);
        }
    };

});
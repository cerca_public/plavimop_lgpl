let attachmentCount = 0;

$(function () {

    $('body').on('click', '.add-attachment-button', function (e) {
        e.preventDefault();
        let attachmentList = $('#' + this.dataset.attachmentsDivId);
        let newWidget = attachmentList.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, '');
        attachmentCount++;
        let newDiv = $('<div>').html(newWidget);
        newDiv.appendTo(attachmentList);
    });
    $('body').on('click', '.remove-attachement-button', function () {
        $(this).parents('.form-attachment-item:first').remove();
    });
});



| Name |  Path
|----  |  ----
| [acquisition_download](actions/acquisition_download.md)        |  /acquisition/{id}/download
| [acquisition_video_download](actions/acquisition_video_download.md)        |  /acquisition-video/{id}/download
| [acquisition_export_recognitions](actions/acquisition_export_recognitions.md)        |  /acquisition/{id}/export-recognitions
| [acquisition_edit](actions/acquisition_edit.md)        |  /acquisition/{id}/edit
| [acquisition_delete](actions/acquisition_delete.md)        |  /acquisition/{id}/delete
| [acquisition_process_edit](actions/acquisition_process_edit.md)        |  /acquisition-process/{slug}/edit
| [acquisition_process_delete](actions/acquisition_process_delete.md)        |  /acquisition-process/{slug}/delete
| [admin_editor_delete](actions/admin_editor_delete.md)        |  /admin/editor/delete/{id}
| [admin_editor_browse](actions/admin_editor_browse.md)        |  /admin/editor/browse
| [article_index](actions/article_index.md)        |  /news/{page}
| [article_category_index](actions/article_category_index.md)        |  /news-category/{slug}/{page}
| [article_show](actions/article_show.md)        |  /news/{slug}
| [article_submit](actions/article_submit.md)        |  /submit-news
| [article_edit](actions/article_edit.md)        |  /news/{slug}/edit
| [article_delete](actions/article_delete.md)        |  /news/{slug}/delete
| [contact_index](actions/contact_index.md)        |  /contact
| [homepage](actions/homepage.md)        |  /
| [page_software](actions/page_software.md)        |  /software
| [page_community](actions/page_community.md)        |  /community
| [page_terms_and_conditions](actions/page_terms_and_conditions.md)        |  /terms-and-conditions
| [page_legal_notices](actions/page_legal_notices.md)        |  /legal-notices
| [motion_index](actions/motion_index.md)        |  /motions/{page}
| [motion_filter_resetting](actions/motion_filter_resetting.md)        |  /motions/clean
| [motion_show](actions/motion_show.md)        |  /motion/{id}
| [recognition_index](actions/recognition_index.md)        |  /recognition
| [recognition_result](actions/recognition_result.md)        |  /recognition/result/{id}
| [security_login](actions/security_login.md)        |  /login
| [security_logout](actions/security_logout.md)        |  /logout
| [security_register](actions/security_register.md)        |  /register
| [security_forgotten_password](actions/security_forgotten_password.md)        |  /security/forgotten-password
| [security_reset_password](actions/security_reset_password.md)        |  /security/reset-password/{resetPassword}
| [share_acquisition_index](actions/share_acquisition_index.md)        |  /share-acquisition/
| [share_acquisition_select_process](actions/share_acquisition_select_process.md)        |  /share-acquisition/select-process
| [share_acquisition_select_motion](actions/share_acquisition_select_motion.md)        |  /share-acquisition/{processSlug}
| [share_acquisition_send_acquisition](actions/share_acquisition_send_acquisition.md)        |  /share-acquisition/send-acquisition/{processSlug}/{motion_id}
| [user_edit_profile](actions/user_edit_profile.md)        |  /user/edit-profile
| [user_change_password](actions/user_change_password.md)        |  /user/change-password
| [user_send_contributor_request](actions/user_send_contributor_request.md)        |  /user/send-contributor-request
| [user_send_download_software_request](actions/user_send_download_software_request.md)        |  /user/send-download-software-request
| [user_dashboard](actions/user_dashboard.md)        |  /user/dashboard
| [admin_export_need_preview](actions/admin_export_need_preview.md)        |  /admin/export-need-preview




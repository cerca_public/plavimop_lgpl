# PLAViMoP Tests

## Launch test in dev environnement
Execute test with command line
```bash
$ ./vendor/bin/simple-phpunit
```
For generate code coverage
```bash
$ ./vendor/bin/simple-phpunit --coverage-html var/coverage
```
Open tests/coverage/index.html file on your browser to see results.
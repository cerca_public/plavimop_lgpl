# PLAViMop Commands

Vous pouvez retrouver l'ensemble des commandes créés dans le dossier [Command](../src/Command)

Liste des commandes :
- [AppCsvImport](#appcsvimport)
- [AppExportArticle](#appexportarticle)
- [AppExportEditorUpload](#appexporteditorupload)
- [AppExportPage](#appexportpage)
- [AppImportArticle](#appimportarticle)
- [AppImportDefaultSurvey](#appimportdefaultsurvey) 
- [AppImportEditorUpload](#appimporteditorupload)
- [AppImportPage](#appimportpage) 
- [AppInit](#appinit)
- [AppPreviewUpdate](#apppreviewupdate) 
- [AppResetMotions](#appresetmotions)


## AppCsvImport

Commande : `bin/console app:csv:import folder`

Argument :
 - `folder` nom du dossier à parcourir (présent dans [data/acquisitions](../data/acquisitions))

#### Command
Parcours le fichier .csv présent dans le dossier `folder` pour charger un lot d'acquisitions.
**Attention** : cette commande peut créer des nouveaux mouvements et tags

#### CSV colonnes

Liste des colonnes lues par la commande

 - Type : nom du Tag sélectionné pour le TagType "Type"
 - Health  :  nom du Tag sélectionné pour le TagType "Health"
 - Participant  :  nom du Tag sélectionné pour le TagType "Participant"
 - Age  :  nom du Tag sélectionné pour le TagType "Age"
 - Motricity  :  nom du Tag sélectionné pour le TagType "Motricity"
 - Category  :  nom du Tag sélectionné pour le TagType "Category"
 - Action  :  nom du Tag sélectionné pour le TagType "Action"
 - Object  :  nom du Tag sélectionné pour le TagType "Object"
 - Title : Nom à donner à la Motion si elle est créée par la commande
 - Comments : Champ comment de l'Acquisition
 - DateAcquisition : Date de l'acquisition au format YYYYMMDD
 - filec3d : fichier c3d correspond 
 - filemp4 : fichier mp4 correspond doit être de la forme `nom_az{az}_el{el}_Duration{duration}_MarkersSize{size}.mp4`
    - Exemple : Acquiesce_Man_1_az45_el45_Duration2630_MarkersSize2.mp4
    - `az` => azimuth (entre -180 et 180)
    - `el` => azimuth (entre -90 et 90)
    - `duration` => durée en seconde (entre 1000 et 6000)
    - `size` => taille des marqueurs (entre 1 et 10) 
 - AcquisitionProcess : identifiant de l'AcquisitionProcess (doit exister dans la base de données)
 - User : username de l'utilisateur déposant (doit exister dans la base)


## AppExportArticle

Commande : `bin/console app:export:article `


Sauvegarder les articles et leurs piéces jointes de la BDD vers le fichier [Article.yml](../data/Articles.yaml)

## AppExportEditorUpload

Commande : `bin/console app:export:editorUpload `


Sauvegarder les fichiers téléchargés depuis l'espace d'administration vers le fichier [EditorUploads.yml](../data/EditorUploads.yaml)

## AppExportPage

Commande : `bin/console app:export:page `


Sauvegarder les articles et leurs pièces jointes de la BDD vers le fichier [Page.yml](../data/Pages.yaml)


## AppImportArticle

Commande : `bin/console app:import:article `

Charger les articles présents dans le fichier  [Article.yml](../data/Articles.yaml) vers la base de données

*Note* cette commande est appelée depuis [AppInitCommand](#appinit)

## AppImportDefaultSurvey

Commande : `bin/console app:import:defaultSurvey`

Charger l'entitée [Survey](../src/Entity/Survey.php ) par défaut dans la base

*Note* cette commande est appelée depuis [AppInitCommand](#appinit)

## AppImportEditorUpload

Commande : `bin/console app:import:editorUpload`

Charger dans la base les assets déclarés dans le fichier [EditorUploads.yml](../data/EditorUploads.yaml)

*Note* cette commande est appelée depuis [AppInitCommand](#appinit)

## AppImportPage

Commande : `bin/console app:import:page `

Charger les pages présentes dans le fichier  [Page.yml](../data/Page.yaml) vers la base de données

*Note* cette commande est appelée depuis [AppInitCommand](#appinit)


## AppInit

Commande : `bin/console app:init`

Initialise l'application (à lancer lors d'une première installation)

Cette commande permet de :
 - Charger les Tag et TagType : définies dans le fichier [Tags.yml](../data/Tags.yaml)
 - Charger les User
 - Charger les Page : voir [AppImportPageCommand](#appimportpage) 
 - Charger les Article : voir [AppImportArticleCommand](#appimportarticle) 
 - Charger la Survey par défaut : voir [AppImportDefaultSurveyCommand](#appimportdefaultsurvey) 

## AppPreviewUpdate

Commande : `bin/console app:preview:update`


Parcours le dossier [data/updatePreview](../data/updatePreview) à la recherche des fichiers {id}.mp4 et met à jours 
remplace la preview de l'acquisition `id` par ce fichier.

Exemple : si un fichier 22.mp4 est présent, la preview de l'acquisition 22 sera mise à jour.

## AppResetMotions 

Commande : `bin/console app:reset:motions --force`
Options : `force` Cette option est nécessaire car commande sensible

Suppression de l'ensemble des acquisitions et motions dans la base de données
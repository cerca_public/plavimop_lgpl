# Security login

Page de connexion

## Route 

| Info | description
| ---| --- 
| Name  |  `security_login` 
| Path | `/login` 
| Controller | [SecurityController::login](../../src/Controller/SecurityController.php)
| Security | none


## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requête |
| `$userRepository` | [UserRepository](../../src/Repository/UserRepository.php) |
| `$securityMailer` | [SecurityMail](../../src/Service/SecurityMailer.php) service permettant l'envoi du mail contenant les instructions à suivre  |
| `$entityManager` | Entity Manager |


## View 

 - [security/login.html.twig](../../templates/security/login.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [ForgottenPasswordType](../../src/Form/ForgottenPasswordType.php) formulaire de récupération de mot de passe|


## Screen

![Screen](../screen/security_login.JPG)


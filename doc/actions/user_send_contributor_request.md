# User Send Contributor Request

Demande pour devenir contributeur (modale)

## Route 

| Info | description
| ---| --- 
| Name  |  `user_send_contributor_request` 
| Path | `/user/send-contributor-request` 
| Controller | [UserController::sendContributorRequest](../../src/Controller/UserController.php)
| Security | has_role('ROLE_USER') && not (has_role('ROLE_CONTRIBUTOR'))

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete | 
| `$mailer` | Service d'envoi de mail de l'application [Mailer::sendContributorRequest](../../src/Service/Mailer.php)|

## View 

 - [user/send_contributor_request.html.twig](../../templates/user/send_contributor_request.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [UserRequestContributorType](../../src/Form/UserRequestContributorType.php) formulaire de demande pour devenir contributeur |

## Screen

![Screen](../screen/user_send_contributor_request.JPG)


# Action Name

## Route 

| Info | description
| ---| --- 
| Name  |  `` 
| Path | `` 
| Controller | [Controller::action](../../src/Controller/Controller.php)
| Security | 

**Parameters**

| Parameter | Description
| --- | --- |
| `` | --- |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `` | --- |


## View 

 - [.html.twig](../../templates/)

**Parameters**

| Parameter | Description
| --- | --- |
| `` | --- |

## Screen

![Screen](../screen/)


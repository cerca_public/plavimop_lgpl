# Acquisition delete

Modal launch form user dashboard to delete acquisition

## Route 

| Info | description
| ---| --- 
| Name  |  `acquisition_delete` 
| Path | `acquisition/{id}/delete` 
| Controller | [AcquisitionController::delete](../../src/Controller/AcquisitionController.php)
| Security | is_granted('delete',acquisition) see [AcquisitionVoter](../../src/Security/Voter/AcquisitionVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | identifiant de l'acquisition à supprimer |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$acquisition` | Inject by ParamConverter from `id` route parameter
| `$acquisitionManager` | Permettra de supprimer l'acquisition
 


## View 

 - [acquisition/delete.html.twig](../../templates/acquisition/delete.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `acquisition` | [Acquisition](../../src/Entity/Acquisition.php) 
| `form` | Empty form for CSRF Protection

## Screen

![Screen](../screen/acquisition_delete.JPG)

# Share Acquisition Index

Notice d'explication soumission d'acquisition

## Route 

| Info | description
| ---| --- 
| Name  |  `share_acquisition_index` 
| Path | `/share-acquisition` 
| Controller | [ShareAcquisitionController::index](../../src/Controller/ShareAcquisitionController.php)
| Security | has_role('ROLE_CONTRIBUTOR')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `share-acquisition` issue de l'admin 


## View 

 - [shareAcquisition/index.html.twig](../../templates/shareAcquisition/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `share-acquisition` issue de l'admin 

## Screen

![Screen](../screen/share_acquisition_index.JPG)


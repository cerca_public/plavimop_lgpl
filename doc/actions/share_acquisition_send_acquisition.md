# Share Acquisition Send Acquisition

Etape de soumission d'une acquisition.

## Route 

| Info | description
| ---| --- 
| Name  |  `share_acquisition_send_acquisition` 
| Path | `/share-acquisition//send-acquisition/{processSlug}/{motion_id}` 
| Controller | [ShareAcquisitionController::shareAcquisition](../../src/Controller/ShareAcquisitionController.php)
| Security | has_role('ROLE_CONTRIBUTOR') && is_granted('select',acquisitionProcess) see [AcquisitionProcessVoter](../../src/Security/Voter/AcquisitionProcessVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `processSlug` | Slug identifiant un [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) |
| `motion_id` | id identifiant un [Motion](../../src/Entity/Motion.php) |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisitionProcess` | [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) associé au slug `{processSlug}` 
| `$motion` | [Motion](../../src/Entity/AcquisitionProcess.php) associé à `{motion_id}` 
| `$user` | L'utilisateur courant |
| `$request` | Requete | 
| `$entityManager` | Entity Manager |


## View 

 - [shareAcquisition/sendAcquisition.html.twig](../../templates/shareAcquisition/sendAcquisition.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [AcquisitionType](../../src/Form/AcquisitionType.php) formulaire de création d'acquisition |
| `motion` | [Motion](../../src/Entity/AcquisitionProcess.php) associé à `{motion_id}`  |
| `acquisitionProcess` | [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) associé au slug `{processSlug}` |

## Screen

![Screen](../screen/share_acquisition_send_acquisition.JPG)


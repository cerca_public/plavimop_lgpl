# Acquisition edit

Modal launch form user dashboard to edit acquisition

## Route 

| Info | description
| ---| --- 
| Name  |  `acquisition_edit` 
| Path | `acquisition/{id}/edit` 
| Controller | [AcquisitionController::edit](../../src/Controller/AcquisitionController.php)
| Security | is_granted('edit',acquisition) see [AcquisitionVoter](../../src/Security/Voter/AcquisitionVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | identifiant de l'acquisition à supprimer |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$acquisition` | Inject by ParamConverter from `id` route parameter
| `$acquisitionManager` | Permettra de mettre à jour l'acquisition
 


## View 

 - [acquisition/edit.html.twig](../../templates/acquisition/edit.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `acquisition` |  [Acquisition](../../src/Entity/Acquisition.php) 
| `form` | [AcquisitionType](../../src/Form/AcquisitionType.php)


## Screen

![Screen](../screen/acquisition_edit.JPG)
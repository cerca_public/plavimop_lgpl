# Page Community

Page community du site

## Route 

| Info | description
| ---| --- 
| Name  |  `page_community` 
| Path | `/community` 
| Controller | [HomeController::community](../../src/Controller/HomeController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `community` issue de l'admin 


## View 

 - [page/layout.html.twig](../../templates/page/layout.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `community` issue de l'admin 

## Screen

![Screen](../screen/page_community.JPG)


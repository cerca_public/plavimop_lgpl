# Acquisition download

Download acquistion C3D

## Route

| Info | description
| ---| --- 
| Name  |  `acquisition_video_download`
| Path | `acquisition-video/{id}/download`
| Controller | [AcquisitionController::downloadVideo](../../src/Controller/AcquisitionController.php)
| Security | None

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | identifiant de l'acquisition à télécharger |

## Action
 
**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisition` |  [Acquisition](../../src/Entity/Acquisition.php) 
 
 
## View 

 - No view
 
## Screen

 - No screen

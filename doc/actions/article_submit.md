# Article submit

Create new article

## Route 

| Info | description
| ---| --- 
| Name  |  `article_submit` 
| Path | `/submit-news` 
| Controller | [ArticleController::submit](../../src/Controller/ArticleController.php)
| Security | has_role('ROLE_CONTRIBUTOR')


## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$pageManager` | Permet de récuperer une page de l'admin
| `$articleManager` | Permettra d'enregistrer le nouvel article
 

## View 

 - [article/edit.html.twig](../../templates/article/edit.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `submit-article` issue de l'admin 
| `form` | [ArticleType](../../src/Form/ArticleType.php)


## Screen 

![Screen](../screen/article_submit.JPG)
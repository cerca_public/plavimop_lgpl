# Article index category

Liste les articles publiés d'une catégorie

## Route 

| Info | description
| ---| --- 
| Name  |  `article_category_index` &&  `article_category_index_paginated`
| Path | `/news-category/{slug}`  && `/news-category/{slug}/{page}`
| Controller | [ArticleController::action](../../src/ArticleController/Controller.php)
| Security |  None

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | Numero de page (si non spécifié vaut 1) |
| `slug` | slug de la categorie concernée |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$page` | Numero de page |
| `$category` | Inject by ParamConverter from `slug` route parameter |
| `$paginator` | Service permettant de paginer une collection |
| `$pageManager` | Service permettant de récuperer la page 'news' |
| `$articleRepository` | Repository des articles |


## View 

 - [article/index.html.twig](../../templates/article/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `articles` | Collection d'Article |
| `category` | Categorie filtrée |
| `page` | [Page](../../src/Entity/Page.php) 'news' issue de l'admin |

## Screen

![Screen](../screen/article_index.JPG)


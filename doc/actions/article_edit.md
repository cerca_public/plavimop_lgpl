# Article edit

Modal launch form user dashboard to edit article

## Route 

| Info | description
| ---| --- 
| Name  |  `article_edit` 
| Path | `news/{slug}/edit` 
| Controller | [ArticleController::edit](../../src/Controller/ArticleController.php)
| Security | is_granted('edit',article) see [ArticleVoter](../../src/Security/Voter/ArticleVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `slug` | slug de l'article à supprimer |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$article` | Inject by ParamConverter from `slug` route parameter
| `$articleManager` | Permettra de mettre à jour l'article
 


## View 

 - [article/edit.html.twig](../../templates/article/edit.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `article` | [Article](../../src/Entity/Article.php) 
| `form` | [ArticleType](../../src/Form/ArticleType.php)


## Screen 

![Screen](../screen/article_edit.JPG)
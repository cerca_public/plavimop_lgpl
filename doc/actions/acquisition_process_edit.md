# Acquisition Process edit

Modal launch form user dashboard to edit acquisition process

## Route 

| Info | description
| ---| --- 
| Name  |  `acquisition_process_edit` 
| Path | `acquisition-process/{slug}/edit` 
| Controller | [AcquisitionProcessController::edit](../../src/Controller/AcquisitionProcessController.php)
| Security | is_granted('edit',acquisitionProcess) see [AcquisitionProcessVoter](../../src/Security/Voter/AcquisitionProcessVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `slug` | AcquisitionProcess slug |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$acquisitionProcess` | Inject by ParamConverter from `slug` route parameter
| `$acquisitionProcessManager` | Permettra de mettre à jour le processus d'acquisition
 


## View 

 - [acquisition_process/edit.html.twig](../../templates/acquisition_process/edit.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `acquisitionProcess` |  [Acquisition](../../src/Entity/Acquisition.php) 
| `form` | [AcquisitionProcessType](../../src/Form/AcquisitionProcessType.php)

## Screen

![Screen](../screen/acquisition_process_edit.JPG)

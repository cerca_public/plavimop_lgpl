# Motion show 

Page détail d'un mouvement (permettant de voir les acquisitions détaillées)

## Route 

| Info | description
| ---| --- 
| Name  |  `motion_show` 
| Path | `/motion/{id}` 
| Controller | [MotionController::show](../../src/Controller/MotionController.php)
| Security | None

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | id du mouvement à afficher |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$page` | [Motion](../../src/Entity/Motion.php) d'id `{id}` | 


## View 

 - [motion/show.html.twig](../../templates/motion/show.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `motion` |  [Motion](../../src/Entity/Motion.php) d'id `{id}` | 


## Screen 

![Screen](../screen/motion_show.JPG)
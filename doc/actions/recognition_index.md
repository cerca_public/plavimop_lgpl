# Recognition index 

Effectuer un test de reconnaissance 

## Route 

| Info | description
| ---| --- 
| Name  |  `recognition_index` 
| Path | `/recognition` 
| Controller | [RecognitionController::index](../../src/Controller/RecognitionController.php)
| Security | None

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$pageManager` | Service permettant de récuperer la page 'recognition' |
| `$surveyManager` | Service permettant d'intialiser un test de reconnaissance (tirage aléatoire de mouvement)' |
 


## View 

 - [recognition/index.html.twig](../../templates/recognition/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [SurveyType](../../src/Form/SurveyType.php) Vue du formulaire de reconnaissance
| `page` | [Page](../../src/Entity/Page.php) 'recognition' issue de l'admin |


## Screen 

![Screen](../screen/recognition_index.JPG)
# Article delete

Modal launch form user dashboard to delete article

## Route 

| Info | description
| ---| --- 
| Name  |  `article_delete` 
| Path | `news/{slug}/delete` 
| Controller | [ArticleController::delete](../../src/Controller/ArticleController.php)
| Security | is_granted('delete',article) see [ArticleVoter](../../src/Security/Voter/ArticleVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `slug` | slug de l'article à supprimer |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$article` | Inject by ParamConverter from `slug` route parameter
| `$articleManager` | Permettra de supprimer l'article
 


## View 

 - [article/delete.html.twig](../../templates/article/delete.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `article` | [Article](../../src/Entity/Article.php) 
| `form` | Empty form for CSRF Protection

## Screen

![Screen](../screen/article_delete.JPG)

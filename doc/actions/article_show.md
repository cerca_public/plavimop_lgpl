# Article edit

Modal launch form user dashboard to edit article

## Route 

| Info | description
| ---| --- 
| Name  |  `article_edit` 
| Path | `news/{slug}` 
| Controller | [ArticleController::slug](../../src/Controller/ArticleController.php)
| Security | none

**Parameters**

| Parameter | Description
| --- | --- |
| `slug` | slug de l'article à afficher |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$article` | Inject by ParamConverter from `slug` route parameter
 


## View 

 - [article/edit.html.twig](../../templates/article/edit.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `article` | [Article](../../src/Entity/Article.php) 


## Screen 

![Screen](../screen/article_show.JPG)
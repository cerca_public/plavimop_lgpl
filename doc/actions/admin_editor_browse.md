# Admin editor browse

Gestionnaire de fichier pour les pages administrables (integration dans CKEditor)

## Route 

| Info | description
| ---| --- 
| Name  |  `admin_editor_browse` 
| Path | `/admin/editor/browse` 
| Controller | [EditorController::browse](../../src/Controller/Admin/EditorController.php)
| Security | has_role('ROLE_ADMIN')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | La requete entrante |
| `$uploadRepository` | Repository des uploads |
| `$entityManager` | Entity Manager permettra d'enrgistrer les nouveaux envois |


## View 

 - [admin/editor/browse.html.twig](../../templates/admin/editor/browse.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `files` | [EditorUpload](../../src/Entity/EditorUpload.php)  présents dans le gestionnaire |

## Screen

![Screen](../screen/admin_editor_browse.JPG)


# Share Acquisition Select Motion

Etape de sélection du mouvement lors du processus d'ajout d'acquisition.
Si l'utilisateur crée un mouvement existant, celui ci sera automatiquement récupéré (pas de doublon)

## Route 

| Info | description
| ---| --- 
| Name  |  `share_acquisition_select_motion` 
| Path | `/share-acquisition/{processSlug}` 
| Controller | [ShareAcquisitionController::selectMotion](../../src/Controller/ShareAcquisitionController.php)
| Security | has_role('ROLE_CONTRIBUTOR') && is_granted('select',acquisitionProcess) see [AcquisitionProcessVoter](../../src/Security/Voter/AcquisitionProcessVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `processSlug` | Slug identifiant un [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisitionProcess` | [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) associé au slug `{processSlug}` 
| `$tagTypeRepository` | [TagTypeRepository](../../src/Repository/TagTypeRepository.php) |
| `$request` | Requete | 
| `$motionManager` | [MotionManager](../../src/Manager/MotionManager.php) utilisé dans le cas d'une création de mouvement |


## View 

 - [shareAcquisition/selectMotion.html.twig](../../templates/shareAcquisition/selectMotion.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `motionSelector` | [MotionSelectorType](../../src/Form/MotionSelectorType.php) formulaire de sélection d'un mouvement existant |
| `motionCreator` | [MotionCreatorType](../../src/Form/MotionCreatorType.php) formulaire de création/récupération d'un mouvement |
| `acquisitionProcess` | [AcquisitionProcess](../../src/Entity/AcquisitionProcess.php) associé au slug `{processSlug}` |

## Screen

![Screen](../screen/share_acquisition_select_motion.JPG)


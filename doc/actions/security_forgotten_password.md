# Security forgotten password

Page de récupération de mot de passe

## Route 

| Info | description
| ---| --- 
| Name  |  `security_forgotten_password` 
| Path | `/security/forgotten-password` 
| Controller | [SecurityController::forgottenPassword](../../src/Controller/SecurityController.php)
| Security | none


## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requête |
| `$userRepository` | [UserRepository](../../src/Repository/UserRepository.php) |
| `$securityMailer` | [SecurityMail](../../src/Service/SecurityMailer.php) service permettant l'envoi du mail contenant les instructions à suivre  |
| `$entityManager` | Entity Manager |


## View 

 - [security/forgotten_password.html.twig](../../templates/security/forgotten_password.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [ForgottenPasswordType](../../src/Form/ForgottenPasswordType.php) formulaire de récupération de mot de passe|

## Mail

 - [emails/forgotten_password.html.twig](../../templates/emails/forgotten_password.html.twig)

## Screen

![Screen](../screen/security_forgotten_password.JPG)


# Admin Export Need Preview

Action permettant de générer une archive zip contenant l'ensemble des acquisitions nécessitant la mise à jour ou création d'une prévisualisation

## Route 

| Info | description
| ---| --- 
| Name  |  `admin_export_need_preview` 
| Path | `/admin/export-need-preview` 
| Controller | [AdminController::exportNeedPreview](../../src/Controller/Admin/AdminController.php)
| Security | has_role('ROLE_ADMIN')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisitionRepository` | [AcquisitionRepository::getExportNeedPreview](../../src/Repository/AcquisitionRepository.php) |
| `f$ilesystem` | Service FileSystem (voir doc symfony) |


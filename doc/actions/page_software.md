# Page Software

Page software du site

## Route 

| Info | description
| ---| --- 
| Name  |  `page_software` 
| Path | `/software` 
| Controller | [HomeController::software](../../src/Controller/HomeController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `software` issue de l'admin 


## View 

 - [page/layout.html.twig](../../templates/page/layout.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `software` issue de l'admin 

## Screen

![Screen](../screen/page_software.JPG)


# Security reset password

Ré-initialisation du mot de passe (lien depuis le mail de mot de passe oublié)

## Route 

| Info | description
| ---| --- 
| Name  |  `security_reset_password` 
| Path | `/security/reset-password/{resetPassword}` 
| Controller | [SecurityController::resetPassword](../../src/Controller/SecurityController.php)
| Security | none


**Parameters**

| Parameter | Description
| --- | --- |
| `resetPassword` | Token associé à l'utilisateur demandant une ré-initialisation du mot de passe |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requête |
| `$user` | Utilisateur associé au token `{resetPassword}` |
| `$passwordEncoder` | UserPasswordEncoderInterface service permettant d'encoder un mot de passe |
| `$entityManager` | Entity Manager |


## View 

 - [security/reset_password.html.twig](../../templates/security/reset_password.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [ResetPasswordType](../../src/Form/ResetPasswordType.php) formulaire de ré-initialisation du mot de passe |



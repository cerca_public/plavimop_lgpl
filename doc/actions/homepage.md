# Homepage

Page d'accueil du site

## Route 

| Info | description
| ---| --- 
| Name  |  `homepage` 
| Path | `/` 
| Controller | [HomeController::index](../../src/Controller/HomeController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `what-is-plavimop` issue de l'admin 


## View 

 - [page/layout.html.twig](../../templates/page/layout.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `what-is-plavimop` issue de l'admin 

## Screen

![Screen](../screen/homepage.JPG)


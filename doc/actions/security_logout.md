# Security logout

Url de déconnexion : redirige en page d'accueil

## Route 

| Info | description
| ---| --- 
| Name  |  `security_logout` 
| Path | `/logout` 
| Controller | [SecurityController::logout](../../src/Controller/SecurityController.php)
| Security | none



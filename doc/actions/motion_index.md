# Motion index 

Parcourir la base de données des acquisitions et afficher le filtre de mouvement

## Route 

| Info | description
| ---| --- 
| Name  |  `motion_index` 
| Path | `/motions/{page}` 
| Controller | [MotionController::index](../../src/Controller/MotionController.php)
| Security | None

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | Numero de page (si non spécifié vaut 1) |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$page` | Numero de la page
| `$request` | Requete |
| `$motionFilter` | Service permettant de manipuler les informations provenant du formulaire de filtre  |
| `$motionRepository` | Repository de l'entité Motion  |
| `$paginator` | Service permettant de paginer une collection   |
| `$session` | Session   |
| `$pageManager` | Service permettant de récuperer la page 'database' |
| `$parameterManager` | Service permettant de récuperer les parametres administrables |
 


## View 

 - [motion/index.html.twig](../../templates/motion/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `motionFilter` | [MotionFilter](../../src/Model/MotionFilter.php) Filtre de mouvement (contenant les tags sélectionnés)
| `motions` | Collection de mouvement contenant des acquisitions validées
| `form` | [MotionFilterType](../../src/Form/MotionFilterType.php) Vue du formulaire de filtre
| `page` | [Page](../../src/Entity/Page.php) 'database' issue de l'admin |
| `parameters` | Parametres administrables |


## Screen 

![Screen](../screen/motion_index.JPG)
# Acquisition download

Download acquistion C3D

## Route

| Info | description
| ---| --- 
| Name  |  acquisition_download 
| Path | `/acquisition/{id}/download`
| Controller | [AcquisitionController::download](../../src/Controller/AcquisitionController.php)
| Security | is_granted('download',acquisition) see [AcquisitionVoter](../../src/Security/Voter/AcquisitionVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | identifiant de l'acquisition à télécharger |

## Action
 
**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisition` |  [Acquisition](../../src/Entity/Acquisition.php) 
| `$acquisitionManager` | Permettra de retourner le c3d de l'acquisition et d'incrementer le compteur de téléchargement
 
 
## View 

 - No view
 
## Screen

 - No screen

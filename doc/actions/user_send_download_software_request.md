# User Send Contributor Request

Demande d'obtention du logiciel (modale)

## Route 

| Info | description
| ---| --- 
| Name  |  `user_send_download_software_request` 
| Path | `/user/send-download-software-request` 
| Controller | [UserController::sendDownloadSoftwareRequest](../../src/Controller/UserController.php)
| Security | has_role('ROLE_USER')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete | 
| `$mailer` | Service d'envoi de mail de l'application [Mailer::sendDownloadSoftwareRequest](../../src/Service/Mailer.php)|

## View 

 - [user/send_download_software_request.html.twig](../../templates/user/send_download_software_request.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [UserRequestSoftwareType](../../src/Form/UserRequestSoftwareType.php) formulaire de demande d'obtention du logiciel |

## Screen

![Screen](../screen/user_send_download_software_request.JPG)


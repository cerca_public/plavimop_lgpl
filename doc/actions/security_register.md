# Security register

Page d'enregistrement d'un membre

## Route 

| Info | description
| ---| --- 
| Name  |  `security_register` 
| Path | `/register` 
| Controller | [SecurityController::register](../../src/Controller/SecurityController.php)
| Security | none


## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requête |
| `$passwordEncoder` | UserPasswordEncoderInterface service permettant d'encoder un mot de passe |


## View 

 - [security/register.html.twig](../../templates/security/register.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [UserRegisterType](../../src/Form/UserRegisterType.php) formulaire d'inscription |


## Screen

![Screen](../screen/security_register.JPG)


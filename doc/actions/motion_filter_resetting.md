# Motion filter resetting 

RAZ du filtre de mouvement redirection vers la page database

## Route 

| Info | description
| ---| --- 
| Name  |  `motion_filter_resetting` 
| Path | `/motions/clean` 
| Controller | [MotionController::resetSearch](../../src/Controller/MotionController.php)
| Security | None

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$session` | Session   | 

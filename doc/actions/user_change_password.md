# User Change Password

Modification du mot de passe (modale)

## Route 

| Info | description
| ---| --- 
| Name  |  `user_change_password` 
| Path | `/user/change-password` 
| Controller | [UserController::changePassword](../../src/Controller/UserController.php)
| Security | has_role('ROLE_USER')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete | 
| `$userManager` | [UserManager](../../src/Manager/UserManager.php) service permettant de mettre à jour le mot de passe d'un utilisateur |


## View 

 - [user/change_password.html.twig](../../templates/user/change_password.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [ChangePasswordType](../../src/Form/ChangePasswordType.php) formulaire de modification du mot de passe |

## Screen

![Screen](../screen/user_change_password.JPG)


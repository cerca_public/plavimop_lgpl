# Page Terms and conditions

Page des termes et conditions

## Route 

| Info | description
| ---| --- 
| Name  |  `page_terms_and_conditions` 
| Path | `/terms-and-conditions` 
| Controller | [HomeController::termsAndCondition](../../src/Controller/HomeController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `terms-and-conditions` issue de l'admin 


## View 

 - [page/layout.html.twig](../../templates/page/layout.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `terms-and-conditions` issue de l'admin 



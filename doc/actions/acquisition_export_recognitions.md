# Acquisition export recognition

Télécharger le rapport CSV des reconnaissances d'acquisitions

## Route

| Info | description
| ---| --- 
| Name  |  acquisition_export_recognition
| Path | `acquisition/{id}/export-recognitions`
| Controller | [AcquisitionController::exportRecognitions](../../src/Controller/AcquisitionController.php)
| Security | is_granted('download_csv',acquisition) see [AcquisitionVoter](../../src/Security/Voter/AcquisitionVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | identifiant de l'acquisition liée au rapport

## Action
 
**Parameters**

| Parameter | Description
| --- | --- |
| `$acquisition` | Inject by ParamConverter from `id` route parameter
| `$serializer` | Permettra de convertir les Recognitions de l'acquisition en CSV
 
 
## View 

 - No view
 
## Screen

 - No screen

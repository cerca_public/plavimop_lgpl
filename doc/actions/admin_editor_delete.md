# Admin editor delete

Gestionnaire de fichier pour les pages administrables (integration dans CKEditor)

## Route 

| Info | description
| ---| --- 
| Name  |  `admin_editor_delete` 
| Path | `/editor/delete/{id}` 
| Controller | [EditorController::browse](../../src/Controller/Admin/EditorController.php)
| Security | has_role('ROLE_ADMIN')

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | Identifiant de [EditorUpload](../../src/Entity/EditorUpload.php) à supprimer |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$upload` | [EditorUpload](../../src/Entity/EditorUpload.php) à supprimer |
| `$entityManager` | Entity Manager permettra de supprimer l'upload |




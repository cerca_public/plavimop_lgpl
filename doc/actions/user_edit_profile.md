# User Edit Profile

Modification informations personnelles (modale)

## Route 

| Info | description
| ---| --- 
| Name  |  `user_edit_profile` 
| Path | `/user/edit-profile` 
| Controller | [UserController::editProfile](../../src/Controller/UserController.php)
| Security | has_role('ROLE_USER')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$entityManager` | Entity Manager |

## View 

 - [user/edit_profile.html.twig](../../templates/user/edit_profile.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [UserType](../../src/Form/UserType.php) formulaire de modification infos personnelles |

## Screen

![Screen](../screen/user_edit.JPG)


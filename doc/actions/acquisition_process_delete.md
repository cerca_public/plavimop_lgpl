# Acquisition Process delete

Modal launch form user dashboard to delete acquisition process

La suppression est possible uniquement si le process est lié à aucune acquisition.

## Route 

| Info | description
| ---| --- 
| Name  |  `acquisition_process_delete` 
| Path | `acquisition-process/{slug}/delete` 
| Controller | [AcquisitionProcessController::edit](../../src/Controller/AcquisitionProcessController.php)
| Security | is_granted('delete',acquisitionProcess) see [AcquisitionProcessVoter](../../src/Security/Voter/AcquisitionProcessVoter.php)

**Parameters**

| Parameter | Description
| --- | --- |
| `slug` | AcquisitionProcess slug |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$request` | Requete |
| `$acquisitionProcess` | Inject by ParamConverter from `slug` route parameter
| `$acquisitionProcessManager` | Permettra de supprimer l'acquisitionProcess
 


## View 

 - [acquisition_process/delete.html.twig](../../templates/acquisition_process/delete.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `acquisitionProcess` |  [Acquisition](../../src/Entity/Acquisition.php) 
| `form` | Empty form for CSRF Protection

## Screen

![Screen](../screen/acquisition_process_delete.JPG)

OR

![Screen](../screen/acquisition_process_delete_2.JPG)

# Recognition result 

Afficher les resultats d'un test de reconnaissance 

## Route 

| Info | description
| ---| --- 
| Name  |  `recognition_result` 
| Path | `/recognition/result/{id}` 
| Controller | [RecognitionController::result](../../src/Controller/RecognitionController.php)
| Security | None

**Parameters**

| Parameter | Description
| --- | --- |
| `id` | id du test à afficher |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$respondent` | [Respondent](../../src/Entity/Respondent.php) d'id `{id}` |
 


## View 

 - [recognition/index.html.twig](../../templates/recognition/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `respondent` | [Respondent](../../src/Entity/Respondent.php) représentant un résultat de test de reconnaissance |


## Screen 

![Screen](../screen/recognition_result.JPG)
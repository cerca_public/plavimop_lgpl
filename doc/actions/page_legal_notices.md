# Page Legal Notices

Page des mentions légales

## Route 

| Info | description
| ---| --- 
| Name  |  `page_legal_notices` 
| Path | `/legal-notices` 
| Controller | [HomeController::legalNotices](../../src/Controller/HomeController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Permet de récuperer la page `legal-notices` issue de l'admin 


## View 

 - [page/layout.html.twig](../../templates/page/layout.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | [Page](../../src/Entity/Page.php) `legal-notices` issue de l'admin 



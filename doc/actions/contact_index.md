# Contact index

Page contenant le formulaire de contat

## Route 

| Info | description
| ---| --- 
| Name  |  `contact_index` 
| Path | `/contact` 
| Controller | [ContactController::contact](../../src/Controller/ContactController.php)
| Security | none

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$pageManager` | Service permettant de récuperer la page 'contact' |
| `$mailer` | Service d'envoi de mail de l'application [Mailer::contact](../../src/Service/Mailer.php)|


## View 

 - [home/contact.html.twig](../../templates/home/contact.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `form` | [ContactType](../../src/Form/ContactType.php) Vue du formulaire de contact
| `page` | [Page](../../src/Entity/Page.php) `contact` issue de l'admin 

## Mail

 - [email/contact.html.twig](../../templates/emails/contact.html.twig)


## Screen

![Screen](../screen/contact_index.JPG)


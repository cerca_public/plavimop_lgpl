# Share Acquisition Select Process

Etape de sélection/création du processus d'acquisition.

## Route 

| Info | description
| ---| --- 
| Name  |  `share_acquisition_select_process` 
| Path | `/share-acquisition/select-process` 
| Controller | [ShareAcquisitionController::selectProcess](../../src/Controller/ShareAcquisitionController.php)
| Security | has_role('ROLE_CONTRIBUTOR')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$entityManager` | Entity Manager |
| `$request` | Requete | 


## View 

 - [shareAcquisition/selectProcess.html.twig](../../templates/shareAcquisition/selectProcess.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `processSelector` | [AcquisitionProcessSelectorType](../../src/Form/AcquisitionProcessSelectorType.php) formulaire de sélection d'un processus d'acquisition |
| `processCreator` | [AcquisitionProcessType](../../src/Form/AcquisitionProcessType.php) formulaire de création/récupération d'un processus d'acquisition |

## Screen

![Screen](../screen/share_acquisition_select_process.JPG)


# Article index

Liste les articles publiés.

## Route 

| Info | description
| ---| --- 
| Name  |  `article_index` &&  `article_index_paginated`
| Path | `/news`  && `/news/{page}`
| Controller | [ArticleController::action](../../src/ArticleController/Controller.php)
| Security |  None

**Parameters**

| Parameter | Description
| --- | --- |
| `page` | Numero de page (si non spécifié vaut 1) |

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$page` | Numero de la page courante |
| `$paginator` | Service permettant de paginer une collection |
| `$pageManager` | Service permettant de récuperer la page 'news' |
| `$articleRepository` | Repository des articles |
| `$articleCategoryRepository` | Respository des catégorie d'article |


## View 

 - [article/index.html.twig](../../templates/article/index.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `articles` | Collection d'Article |
| `categories` | Toutes les catégories d'article (utile au filtre par catégorie |
| `page` | [Page](../../src/Entity/Page.php) 'news' issue de l'admin |

## Screen

![Screen](../screen/article_index.JPG)


# User Dashboard

Tableau de bord utilisateur

## Route 

| Info | description
| ---| --- 
| Name  |  `user_dashboard` 
| Path | `/user/dashboard` 
| Controller | [UserController::dashboard](../../src/Controller/UserController.php)
| Security | has_role('ROLE_USER')

## Action 

**Parameters**

| Parameter | Description
| --- | --- |
| `$userRepository` | [UserRepository::getDashboardInfo](../../src/Repository/UserRepository.php) |

## View 

 - [user/user/dashboard.html.twig](../../templates/user/dashboard.html.twig) (layout)
   - [user/dashboard/_card_acquisition.html.twig](../../templates/user/dashboard/_card_acquisition.html.twig)
   - [user/dashboard/_card_acquisition_process.html.twig](../../templates/user/dashboard/_card_acquisition_process.html.twig)
   - [user/dashboard/_card_article.html.twig](../../templates/user/dashboard/_card_article.html.twig)
   - [user/dashboard/_card_profile_management.html.twig](../../templates/user/dashboard/_card_profile_management.html.twig)

**Parameters**

| Parameter | Description
| --- | --- |
| `user` | [User](../../src/Entity/User.php) Utilisateur courant |

## Screen

![Screen](../screen/user_dashboard.JPG)


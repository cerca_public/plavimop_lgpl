# PLAViMoP Development

## PHP

 - symfony 4.0
 - VichUploaderBundle
 - EasyAdmin
 

## JS/CSS
For generate assets in public/build folder, you must install yarn on your development desktop.

[Yarn installation link](https://yarnpkg.com/en/docs/install)

Install dev dependencies :
```bash
$ yarn install
```

Run following command to build assets files
For development
```bash
$ yarn run encore dev 
```
Before commit/push for production
```bash
$ yarn run encore production 
```

For more information please read the [symfony webpack documentation](https://symfony.com/doc/current/frontend.html)

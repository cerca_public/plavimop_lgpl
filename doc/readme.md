# PLAViMoP Project

Point Light Action Visualization and Modification Platorm

## Minimum requirements

- php 7.2 version
- php_fileinfo extension
- php_zip extension

## Pre-installation : preprod server
Get fresh data from preprod (clear unused upload's files before executing commands http://localhost:8000/admin/editor/browse )
```bash
$ git pull origin master
$ composer install
$ php bin/console doctrine:migration:migrate
$ php bin/console app:export:page
$ php bin/console app:article:page 
$ git add  data/Pages.yaml  data/Articles.yaml data/EditorUploads.yaml public/uploads/editor/files/*  public/uploads/article/*
$ git commit -m "Content : Pages and Articles update"
$ git push origin master

## Pour terminer
$ npm install
```

## Installation : prod

```bash
$ git clone http://gitlab+deploy-token-863:pKdvpNa8cKLsb84GjMQm@gitlab.com/SebastienXD/plavimop.git
$ cd plavimop
$ composer install
````
Configure your database in `.env` project root file ([MAILER_URL](https://symfony.com/doc/current/email.html), PLAVIMOP_MAIL_CONTACT, [DATABASE_URL](https://symfony.com/doc/current/doctrine.html#configuring-the-database))

```bash
$ php bin/console doctrine:database:create
$ phpbin/console doctrine:migration:migrate
$ php bin/console app:init
```

Import pprime acquisitions put in `data/acquisitions/acq_2016` directories
```bash
$ php bin/console app:csv:import acq_2016
```

## Usage
There's no need to configure anything to run the application. Just execute this
command to run the built-in web server and access the application in your
browser at <http://localhost:8000>:

```bash
$ php bin/console server:run
```

## Read more :

  - About PLAViMoP [Commands](commands.md)
  - About PLAViMoP [Entities](entities.md)
  - About PLAViMoP [Tests](tests.md)
  - About PLAViMoP [Development](developments.md)
  - About PLAViMoP [Actions](actions.md)



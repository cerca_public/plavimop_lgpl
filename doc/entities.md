# PLAViMoP Entities

## Diagramme

![EntityRelation](schema/EntityRelation.png)


## Mise à jour du schema d'entité
Plavimop use [doctrine migrations bundle](https://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html) to defined database structure along development.

Creer un fichier de migration

```bash
$ bin/console doctrine:migrations:diff
```
Mettre à jour la base de données

```bash
$ bin/console doctrine:migrations:migrate
```
Pour mettre à jour le [diagramme](#diagramme) présent sur cette page

```bash
$ bin/console yuml:mappings -f doc/schema/EntityRelation.png
```

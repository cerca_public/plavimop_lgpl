let Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .autoProvideVariables()
    .enableSassLoader()
    .enableVersioning(false)
    .createSharedEntry('js/common', ['jquery'])
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/motion', './assets/js/motion.js')
    .addEntry('js/recognitionTest', './assets/js/recognitionTest.js')
    .addEntry('js/admin', './assets/js/admin.js')
    .addEntry('js/editor', './assets/js/editor.js')
    .addEntry('js/addAttachment', './assets/js/addAttachment.js')
    .addEntry('js/trumbowyg', './assets/js/trumbowyg.js')
    .addEntry('js/motionShow', './assets/js/motionShow.js')
    .addStyleEntry('css/app', ['./assets/scss/app.scss'])
    .addStyleEntry('css/admin', ['./assets/scss/admin.scss'])
    .addLoader({
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['env']
            }
        }
    })


;

module.exports = Encore.getWebpackConfig();

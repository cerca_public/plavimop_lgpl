**PLAViMoP Project**

Point Light Action Visualization and Modification Platform

_**Université de Poitiers - CNRS**_

_Minimum requirements_

php 7.2 version
php_fileinfo extension
php_zip extension


_Get fresh data from preprod_ 

(clear unused upload's files before executing commands http://localhost:8000/admin/editor/browse )

$ git pull origin master
$ composer install
$ php bin/console doctrine:migration:migrate
$ git push origin master

_The end_

$ npm install


_Usage_

There's no need to configure anything to run the application. Just execute this
command to run the built-in web server and access the application in your
browser at http://localhost:8000:
$ php bin/console server:run


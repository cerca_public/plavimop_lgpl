<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 27/02/2018
 * Time: 23:16
 */

namespace App\Tests\Controller\Admin;

namespace App\Tests;

use App\Entity\Acquisition;
use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\Motion;
use App\Entity\Page;
use App\Entity\Survey;
use App\Entity\Tag;
use App\Entity\TagType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BackendTest extends WebTestCase
{

    /**
     * @dataProvider queryParametersProvider
     */
    public function testBackendPagesLoadCorrectly($queryParameters)
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => 'admin',
            )
        );
        $client->request('GET', '/admin/?'.http_build_query($queryParameters));
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function queryParametersProvider()
    {
        $kernel = self::bootKernel();
        $entities = [
            'User' => User::class,
            'TagType' => TagType::class,
            'Tag' => Tag::class,
            'Motion' => Motion::class,
            'ValidAcquisition' => Acquisition::class,
            'NeedReviewAcquisition' => Acquisition::class,
            'Page' => Page::class,
            'Article' => Article::class,
            'ArticleCategory' => ArticleCategory::class,
            'Survey' => Survey::class,
        ];
        $disabledNew = [Acquisition::class];

        foreach ($entities as $class => $fqn) {

            $em = $kernel->getContainer()
                ->get('doctrine')
                ->getManager();
            $entity = $em
                ->getRepository($fqn)
                ->findOneBy([]);
            $em->close();
            $em = null;

            yield  [['action' => 'list', 'entity' => $class]];
            if (!in_array($fqn, $disabledNew)) {
                yield  [['action' => 'new', 'entity' => $class]];
            }

            if ($entity) {
                yield  [['action' => 'show', 'entity' => $class, 'id' => $entity->getId()]];
                yield  [['action' => 'edit', 'entity' => $class, 'id' => $entity->getId()]];
            }
        }
        $em = null;
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

    }
}
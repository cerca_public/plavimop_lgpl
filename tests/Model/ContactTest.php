<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 22/01/2018
 * Time: 00:19
 */

namespace App\Tests\Model;

use App\Model\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    public function testCreateContact()
    {
        $contact = new Contact();
        $contact->setTitle('Title');
        $contact->setSubject('Subject');
        $contact->setContent('Content');
        $contact->setAttachments([]);
        $contact->setEmail('Email');

        $this->assertSame('Title', $contact->getTitle());
        $this->assertSame('Subject', $contact->getSubject());
        $this->assertSame('Content', $contact->getContent());
        $this->assertSame('Email', $contact->getEmail());
        $this->assertSame([], $contact->getAttachments());
    }
}

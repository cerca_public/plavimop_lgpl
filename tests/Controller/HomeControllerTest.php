<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 13/01/2018
 * Time: 02:06
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testUrls($url, $statusCode)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        $this->assertSame($statusCode, $client->getResponse()->getStatusCode());
    }

    public function urlProvider()
    {
        yield ['/', Response::HTTP_FOUND];
        yield ['/en/', Response::HTTP_OK];
        yield ['/en/software', Response::HTTP_OK];
        yield ['/en/terms-and-conditions', Response::HTTP_OK];
        yield ['/en/community', Response::HTTP_OK];
    }
}
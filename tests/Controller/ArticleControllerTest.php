<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 22/01/2018
 * Time: 01:33
 */

namespace App\Tests\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/en/news');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }


    public function testShow()
    {
        $client = static::createClient();
        /** @var Article $article */
        $article = $client->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Article::class)
            ->findOneBy([]);
        if ($article) {
            $client->request(Request::METHOD_GET, '/en/new/'.$article->getSlug());
            $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        }
    }
}

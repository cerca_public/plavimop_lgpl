<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 23/01/2018
 * Time: 01:09
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecognitionControllerTest extends WebTestCase
{


    public function testIndex()
    {
        $client = $this->createClient();
        $client->request(Request::METHOD_GET, '/en/recognition');

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}

<?php

namespace App\Tests\Controller;

use App\Entity\Motion;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Field\ChoiceFormField;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MotionControllerTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     * @param $url  string Url to test
     * @param $method string
     * @param $statusCode integer Status code excepted
     */
    public function testUrls($url, $method, $statusCode)
    {
        $client = static::createClient();
        $client->request($method, $url);
        $this->assertSame($statusCode, $client->getResponse()->getStatusCode());
    }

    public function urlProvider()
    {
        yield ['/en/motions', Request::METHOD_GET, Response::HTTP_OK];
        yield ['/en/motions/1', Request::METHOD_GET, Response::HTTP_OK];
        yield ['/en/motions/2', Request::METHOD_GET, Response::HTTP_OK];
        yield ['/en/motion/0', Request::METHOD_GET, Response::HTTP_NOT_FOUND];
        yield ['/en/motions/clean', Request::METHOD_GET, Response::HTTP_FOUND];
    }

    public function testIndexWithFormSubmit()
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/en/motions');

        $searchForm = $crawler->selectButton('Search')->form();


        /** @var ChoiceFormField $select */
        $select = $searchForm['motion_filter[selectedTags][types][6][tagsSelected]'];
        $select->select('0');

        $client->submit($searchForm);

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

    }

    public function testShow()
    {
        $client = static::createClient();

        /** @var Motion $motion */
        $motion = $client->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Motion::class)
            ->findOneBy([], ['id' => 'asc']);

        if ($motion) {
            $client->request(Request::METHOD_GET, '/en/motion/'.$motion->getId());
            $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        }

    }
}

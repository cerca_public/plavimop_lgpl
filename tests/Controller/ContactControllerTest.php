<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 14/01/2018
 * Time: 02:03
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;


class ContactControllerTest extends WebTestCase
{

    /**
     * @dataProvider contactFormData
     * @param $formSubmitResponse
     * @param $formData
     */
    public function testContact($formSubmitResponse, $formData)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', "/en/contact");

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $contactForm = $crawler->selectButton('Send')->form();

        $client->submit($contactForm, $formData);

        $this->assertSame($formSubmitResponse, $client->getResponse()->getStatusCode());
    }


    public function contactFormData()
    {
        yield [
            Response::HTTP_FOUND,
            [
                'contact[email]' => 'email@plavimop.com',
                'contact[title]' => 'Test title',
                'contact[content]' => 'Test content'
            ]
        ];
        yield [
            Response::HTTP_OK,
            [
                'contact[email]' => '',
                'contact[title]' => '',
                'contact[content]' => ''
            ]
        ];
        yield [
            Response::HTTP_OK,
            [
                'contact[email]' => 'huihiuh',
                'contact[title]' => '',
                'contact[content]' => 'Test content'
            ]
        ];

    }
}

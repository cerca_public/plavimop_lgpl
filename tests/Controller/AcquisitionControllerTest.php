<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 22/01/2018
 * Time: 01:24
 */

namespace App\Tests\Controller;

use App\Entity\Acquisition;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AcquisitionControllerTest extends WebTestCase
{

    public function testDownload()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => 'admin',
            )
        );

        /** @var Acquisition $acquisition */
        $acquisition = $client->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Acquisition::class)
            ->findBy([], null, 1)[0];

        $client->request(Request::METHOD_GET, '/en/acquisition/'.$acquisition->getId().'/download');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testDownloadVideo()
    {
        $client = static::createClient();

        /** @var Acquisition $acquisition */
        $acquisition = $client->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Acquisition::class)
            ->findBy([], null, 1)[0];

        $client->request(Request::METHOD_GET, '/en/acquisition-video/'.$acquisition->getId().'/download');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}

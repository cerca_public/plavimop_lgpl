<?php
/**
 * Created by PhpStorm.
 * User: Sébastien
 * Date: 22/01/2018
 * Time: 23:22
 */

namespace App\Tests\Controller\Admin;

use App\Entity\EditorUpload;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EditorControllerTest extends WebTestCase
{


    public function testBrowse()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => 'admin',
            )
        );

        $client->request(Request::METHOD_GET, '/en/admin/editor/browse');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $client = static::createClient(
            array(),
            array(
                'PHP_AUTH_USER' => 'admin',
                'PHP_AUTH_PW' => 'admin',
            )
        );

        $crawler = $client->request(Request::METHOD_GET, '/en/admin/editor/browse');
        $editorUploadForm = $crawler->selectButton('Envoyer')->form();
        $editorUploadForm['fileFile[file]']->upload(
            $client->getContainer()->getParameter('app.dir.tests').'/Fixtures/files/logo.png'
        );


        $client->submit($editorUploadForm);

        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());

        $uploadRepository = $client->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(EditorUpload::class);


        /** @var EditorUpload $upload */
        $upload = $uploadRepository->findBy([], ['id' => 'DESC'], 1)[0];


        $this->assertSame('logo.png', $upload->getFile()->getOriginalName());

        $client->request(Request::METHOD_GET, '/en/admin/editor/delete/'.$upload->getId());
        $this->assertSame(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        $this->assertNull($uploadRepository->find($upload->getId()));


    }


}
